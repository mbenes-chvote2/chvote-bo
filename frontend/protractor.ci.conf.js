/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

global.disableScreenshots = false;
let config = require("./protractor.conf").config;

config.params = {
  mockServer: {
    hostname: "docker",
    port: 80
  }
};


let filterConfig = (suites, keyFilter) => {
  let filteredSuites = {};
  Object.keys(suites)
    .filter(keyFilter)
    .forEach(k => filteredSuites[k] = suites[k]);
  return filteredSuites;
};

// Concurrency don't work on docker
config.suites = filterConfig(config.suites, spec => spec.indexOf("Concurrency") === -1);

exports.config = config;
