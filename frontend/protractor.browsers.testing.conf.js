/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

// Protractor configuration file for crossbrowsertesting.com

exports.config = {
  allScriptsTimeout: 24000,
  suites: {
    'Home': './e2e/01-home/*.e2e-spec.ts',
  },
  exclude: ['**/*.po.js'], // exclude page objects
  seleniumAddress: 'http://CROSSBROWSERTESTING_USERNAME:CROSSBROWSERTESTING_PASSWORD@hub.crossbrowsertesting.com:80/wd/hub',
  // webDriverProxy: 'http://192.168.137.2:8888', //only for use in a local tunnel
  capabilities: {
    // Cloud capabilities
    name: 'Crossbrowser Testing tests',
    username :  "CROSSBROWSERTESTING_USERNAME",
    password : "CROSSBROWSERTESTING_PASSWORD",
    record_video: 'true',
    record_network: 'true',
    shardTestFiles: true,
    maxInstances: 1,

    // Device capabilities
    platform : 'Windows 7',    // Gets latest version by default
    browserName: 'chrome'      // To specify version, add version: 'desired version'

  },

  baseUrl: 'BASE_URL',   //locally use : http://local:4200/

  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 300000,
    print: function () {
    }
  },


};
