/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { DatePipe } from '@angular/common';
import { base64DataToBlob } from '../../util/file-utils';

@Injectable()
export class FileService {
  // special character added at the top of the CSV file as a workaround to bug in Excel not recognizing UTF-8 characters
  readonly EXCEL_BOM: string = "\uFEFF";

  constructor(private translateService: TranslateService,
              private datePipe: DatePipe) {
  }


  /**
   * @param {any[]} objects
   * @param {String} headerTranslationPath
   */
  generateCsvFromFlatObject(objects: any[], headerTranslationPath: string) {
    if (!objects || objects.length == 0) {
      return "";
    }
    let columns = objects.reduce((prevKeys, object) => {
      let keys = [...prevKeys];
      Object.keys(object).forEach(k => (keys.indexOf(k) == -1  ) && keys.push(k));
      return keys;
    }, []);

    let header = columns.map(column => this.translateService.instant(headerTranslationPath + "." + column)).join(";");
    let content = objects.map(obj => columns.map(column => this.formatForCsv(obj[column])).join(";")).join("\n");
    return this.EXCEL_BOM + header + "\n" + content;

  }


  private formatForCsv(fieldValue: any) {
    if (typeof fieldValue === 'string') {
      return '"' + fieldValue.replace(/\s/g, " ").replace('"', '""') + '"';
    } else if (fieldValue instanceof Date) {
      return this.datePipe.transform(fieldValue, 'd.MM.y')
    } else if (fieldValue instanceof Array) {
      return fieldValue.join(', ');
    } else {
      return fieldValue;
    }
  }

  downloadFile(fileName: string, content: string, isBase64 = false) {

    let blob = isBase64 ? base64DataToBlob(content, "octet/stream") : new Blob([content], {type: "octet/stream"});
    this.downloadBlob(fileName, blob);
  }

  downloadBlob(fileName: string, blob: Blob) {
    (<any>window).lastDownloadedContent = blob;
    (<any>window).lastDownloadedFileName = fileName;
    let url = window.URL.createObjectURL(blob);
    let a = document.createElement("a");
    document.body.appendChild(a);
    a.href = url;
    a.download = fileName;
    a.click();
    setTimeout(function () {
      window.URL.revokeObjectURL(url);
      document.body.removeChild(a);
    }, 0);
  }


}
