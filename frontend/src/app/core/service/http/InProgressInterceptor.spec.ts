/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { BACKGROUND_PROCESSING, InProgressInterceptor } from './InProgressInterceptor';
import { HttpEvent, HttpHandler, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { Subject } from 'rxjs/Subject';


describe('InProgressInterceptor', () => {

  let interceptor: MockedInProgressInterceptor;
  let next: HttpHandler;
  let responseReceived = new Subject<HttpEvent<any>>();

  beforeEach(() => {
    interceptor = new MockedInProgressInterceptor();
    next = <any>{handle: () => responseReceived};
  });

  it("should display a apinner if it's not a background process", () => {

    responseReceived.next(new HttpResponse({body: "test"}));
    let req = new HttpRequest("GET", "url", null);
    interceptor.intercept(req, next).subscribe(() => {
    });
    expect(interceptor._showCoverSpy).toHaveBeenCalled();
    expect(interceptor._hideCoverSpy).not.toHaveBeenCalled();
    responseReceived.next(<HttpEvent<any>>{});
    responseReceived.complete(); // For finally ...
    expect(interceptor._hideCoverSpy).toHaveBeenCalled();

  });

  it("shouldn't display a apinner if it's a background process", () => {
    let req = new HttpRequest("GET", "url", null, {headers: new HttpHeaders().append(BACKGROUND_PROCESSING, "true")});
    interceptor.intercept(req, next).subscribe(() => {
    });

    expect(interceptor._showCoverSpy).not.toHaveBeenCalled();
    responseReceived.next(null);
    responseReceived.complete(); // For finally ...
    expect(interceptor._hideCoverSpy).not.toHaveBeenCalled();

  })
});

class MockedInProgressInterceptor extends InProgressInterceptor {
  _showCoverSpy;
  _hideCoverSpy;


  constructor() {
    super(null, null, null);
    this._showCoverSpy = jasmine.createSpy("showCover");
    this.showCover = this._showCoverSpy;
    this._hideCoverSpy = jasmine.createSpy("hideCover");
    this.hideCover = this._hideCoverSpy;
  }


}
