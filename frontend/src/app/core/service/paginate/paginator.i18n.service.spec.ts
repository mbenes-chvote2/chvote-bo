/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { LangChangeEvent, TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs/Subject';
import { PaginatorI18NService } from './paginator.i18n.service';
import Spy = jasmine.Spy;

describe('PaginatorI18NService', () => {
  let translateService: TranslateService = <any>{
    currentLang: "fr",
    onLangChange: new Subject<LangChangeEvent>(),
    instant: jasmine.createSpy("instant")
  };
  (<Spy>translateService.instant).and.callFake(key => translateService.currentLang + "." + key);


  let service = new PaginatorI18NService(translateService);


  it('should translate accordingly to the user language', () => {
    expect(service.itemsPerPageLabel).toEqual('fr.global.table.paginator.itemsPerPageLabel');
    expect(service.getRangeLabel(0, 10, 200)).toEqual('1 - 10 fr.global.table.paginator.of 200');
    expect(service.nextPageLabel).toEqual('fr.global.table.paginator.nextPageLabel');
    expect(service.previousPageLabel).toEqual('fr.global.table.paginator.previousPageLabel');

    translateService.currentLang = "de";
    let hasBeenNotified = false;
    service.changes.subscribe(() => hasBeenNotified = true);
    (<Subject<LangChangeEvent>>translateService.onLangChange).next();

    expect(service.itemsPerPageLabel).toEqual('de.global.table.paginator.itemsPerPageLabel');
    expect(service.getRangeLabel(0, 10, 200)).toEqual('1 - 10 de.global.table.paginator.of 200');
    expect(service.nextPageLabel).toEqual('de.global.table.paginator.nextPageLabel');
    expect(service.previousPageLabel).toEqual('de.global.table.paginator.previousPageLabel');
    expect(hasBeenNotified).toBeTruthy();

  });


});
