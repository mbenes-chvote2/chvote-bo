/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { TranslateService } from "@ngx-translate/core";
import { Observable } from "rxjs/Observable";
import { MatDialog } from "@angular/material";
import { BaseUrlService } from '../service/base-url/base-url.service';

export class MockObjectFactory {

  static dateExpression: RegExp = /^(\d{2})\.(\d{2})\.(\d{4}) (\d{2}):(\d{2}):(\d{2})$/;

  static dateTime(formattedDateTime: string): Date {
    let dateField = MockObjectFactory.dateExpression.exec(formattedDateTime);
    if (dateField) {
      let day = dateField[1];
      let month = dateField[2];
      let year = dateField[3];
      let hours = dateField[4];
      let minutes = dateField[5];
      let seconds = dateField[6];

      return new Date(
        parseInt(year), parseInt(month) - 1, parseInt(day),
        parseInt(hours), parseInt(minutes), parseInt(seconds)
      );
    }

  }

  translateServiceMock(): TranslateService {
    return {
      get(key: string | Array<string>, interpolateParams?: Object): Observable<string | any> {
        return Observable.create(function mockTranslate(observer) {
          observer.next(key);
          observer.complete();
        });
      }
    } as TranslateService;
  }

  dialogMock(): MatDialog {
    return jasmine.createSpyObj("dialog", ["open"]) as MatDialog;
  }

  baseUrlServiceMock(): BaseUrlService {
    const baseUrlServiceMock: any = jasmine.createSpyObj('baseUrlService', ["getFrontendBaseUrl", "getBackendBaseUrl"]);
    baseUrlServiceMock.getFrontendBaseUrl.and.returnValue("/");
    baseUrlServiceMock.getBackendBaseUrl.and.returnValue("/");
    return baseUrlServiceMock;
  }

}
