/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { AfterViewChecked, Component, OnInit } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { AuthenticationService } from './core/authentication/authentication.service';
import { Router } from '@angular/router';
import { AuthorizationService } from './core/service/http/authorization.service';
import * as moment from "moment";
import { Moment } from "moment";
import { DateAdapter } from '@angular/material';
import { DatetimeAdapter } from '@mat-datetimepicker/core';
import { VersionService } from './core/service/version.service';
import { Version } from './core/model/version';
import { User } from './core/model/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewChecked {


  version: Version;
  user: User;

  constructor(private translate: TranslateService,
              private momentDateAdapter: DateAdapter<Moment>,
              private momentDatetimeAdapter: DatetimeAdapter<Moment>,
              private router: Router,
              private versionService: VersionService,
              private authorisationService: AuthorizationService,
              public authenticationService: AuthenticationService) {

    moment().locale('fr-ch');
    // this language will be used as a fallback when a translation isn't found in the current language
    translate.setDefaultLang('fr');

    // the lang to use, if the lang isn't available, it will use the current loader to get them
    translate.use('fr');
  }

  ngOnInit(): void {
    this.authorisationService.userChange
      .subscribe(
        user => {
          this.user = user;
          if (!this.version && user) {
            this.versionService.get().subscribe(version => this.version = version);
          }
        }
      );
  }

  ngAfterViewChecked(): void {
    [].slice.call(document.querySelectorAll("mat-datetimepicker-toggle button"))
      .forEach(button => button.setAttribute("tabindex", -1))
  }


  logout() {
    this.authenticationService.logout();
    this.router.navigate(["login"]);
  }

  switchLang(lang: string): void {
    this.translate.use(lang);
    this.momentDateAdapter.setLocale(lang);
    this.momentDatetimeAdapter.setLocale(lang);
  }

}
