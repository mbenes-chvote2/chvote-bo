/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, OnInit } from '@angular/core';
import { AuthorizationService } from '../../../core/service/http/authorization.service';
import { ElectoralAuthorityKeyService } from '../electoral-authority-key.service';
import { ElectoralAuthorityKey } from '../electoral-authority-key';

@Component({
  selector: 'electoral-authority-key-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class ElectoralAuthorityKeyCardComponent implements OnInit {
  keys: ElectoralAuthorityKey[] = [];

  constructor(private authService: AuthorizationService, private service: ElectoralAuthorityKeyService) {

  }

  ngOnInit() {
    this.service.findAll().subscribe(keys => this.keys = keys);
  }

  get completed() {
    return this.service.completed;
  }

  get readOnly() {
    return !this.authService.hasAtLeastOneRole(["ADD_ELECTORAL_AUTHORITY_KEY"]);
  }


}
