/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'edit-with-side-bar',
  templateUrl: './edit-with-side-bar.component.html',
  styleUrls: ['./edit-with-side-bar.component.scss']
})
export class EditWithSideBarComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}

@Component({
  selector: 'edit-content',
  template: '<ng-content></ng-content>',
})
export class EditContentComponent {
}

@Component({
  selector: 'side-bar-item',
  template: `
    <mat-list-item>
      <h4 matLine [class.task-error]="inError">{{title}}<span *ngIf="!locked && required" class="required"> *</span></h4>
      <mat-icon class="task-complete" *ngIf="!locked && !inError && complete">check_circle</mat-icon>
      <mat-icon class="task-locked" *ngIf="locked">lock</mat-icon>
      <mat-icon class="task-error" *ngIf="inError">warning</mat-icon>
    </mat-list-item>
  `
})
export class SideBarItemComponent {
  @Input() title: string;
  @Input() required: boolean;
  @Input() complete: boolean;
  @Input() locked: boolean;
  @Input() inError: boolean;
}


@Component({
  selector: 'side-bar-back-button',
  template: `
    <mat-list-item>
      <h4 matLine>
        {{title}}
      </h4>
      <mat-icon>backspace</mat-icon>
    </mat-list-item>
  `
})
export class SideBarBackButtonComponent {
  @Input() title: string;
}
