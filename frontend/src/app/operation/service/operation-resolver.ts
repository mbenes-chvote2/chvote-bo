/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Operation } from '../model/operation';
import { OperationService } from './operation.service';
import { OperationManagementService } from './operation-managment.service';
import { flatMap, mapTo, take } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';


@Injectable()
export class OperationResolver implements Resolve<Operation> {

  constructor(private operationService: OperationService,
              private operationManagementService: OperationManagementService,
              private router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Operation> {
    let id = +route.paramMap.get('id');

    return this.operationService.findOne(id).pipe(
      take(1),
      flatMap(operation => {
          if (operation) {
            return this.operationManagementService.updateOperation(operation).pipe(mapTo(operation));
          } else { // id not found
            this.router.navigate(['/']);
            return of(null);
          }
        }
      ));
  }
}
