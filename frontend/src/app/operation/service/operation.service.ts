/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Injectable } from "@angular/core";
import { Operation } from "../model/operation";
import { Observable } from "rxjs/Observable";
import { HttpClient } from '@angular/common/http';

import { HttpParameters } from '../../core/service/http/http.parameters.service';
import { BaseConfiguration } from '../parameters/base/base-configuration';
import { MilestoneConfiguration } from '../parameters/milestone/milestone-configuration';

@Injectable()
export class OperationService {

  private serviceUrl: string;

  constructor(private http: HttpClient, private httpParameters: HttpParameters) {
    this.serviceUrl = this.httpParameters.apiBaseURL + '/operation';
  }

  findAll(): Observable<Operation[]> {
    return this.http.get<Operation[]>(`${this.serviceUrl}/all`);
  }

  findOne(id: number): Observable<Operation> {
    return this.http.get<Operation>(`${this.serviceUrl}/${id}`);
  }

  create(baseConfig: BaseConfiguration): Observable<number> {
    return this.http.post<number>(`${this.serviceUrl}`, baseConfig);
  }

  updateBaseConfiguration(operationId: number, baseConfig: BaseConfiguration): Observable<Operation> {
    return this.http.put<Operation>(`${this.serviceUrl}/${operationId}/base`, baseConfig);
  }

  updateMilestones(operationId: number, milestoneConfig: MilestoneConfiguration): Observable<Operation> {
    return this.http.put<Operation>(`${this.serviceUrl}/${operationId}/milestones`, milestoneConfig);
  }

  invalidateTestSite(id: number) {
    return this.http.get(`${this.serviceUrl}/${id}/invalidate-test-site`);
  }

  validateTestSite(id: number) {
    return this.http.get(`${this.serviceUrl}/${id}/validate-test-site`);
  }

  validateVotingMaterial(id: number) {
    return this.http.get(`${this.serviceUrl}/${id}/validate-voting-material`);
  }

  closeVotingPeriod(id: number) {
    return this.http.get(`${this.serviceUrl}/${id}/close-voting-period`);
  }

  targetReal(id: number) {
    return this.http.get(`${this.serviceUrl}/${id}/target-real`);
  }

  targetSimulation(id: number, simulationName) {
    return this.http.put(`${this.serviceUrl}/${id}/target-simulation`, simulationName);
  }

  modifyConfiguration(id: number) {
    return this.http.get(`${this.serviceUrl}/${id}/modify`);
  }

  updateVotingCardTitle(operation: Operation, title) {
    return this.http.post(`${this.serviceUrl}/${operation.id}/voting-card-title`, {value: title});
  }

  updatePrinterTemplate(operation: Operation, printerTemplate: string) {
    return this.http.post(`${this.serviceUrl}/${operation.id}/printer-template`, {value: printerTemplate});
  }
}
