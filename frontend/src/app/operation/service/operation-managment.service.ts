/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Injectable } from '@angular/core';
import { OperationStatusService } from './operation.status.service';
import { Operation } from '../model/operation';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { OperationStatus } from '../model/operation-status';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { take } from 'rxjs/operators';

@Injectable()
export class OperationManagementService {
  operation = new BehaviorSubject<Operation>(null);
  status = new BehaviorSubject<OperationStatus>(null);

  public constructor(private operationStatusService: OperationStatusService) {
  }

  /**
   * @param {Operation} operation
   * @returns {Observable<any>} observable returned guarantees that state and operation match
   */
  public updateOperation(operation: Operation): Observable<any> {
    this.operation.next(operation);
    return this.shouldUpdateStatus();
  }

  public updateStatus(status: OperationStatus) {
    this.status.next(status);
  }

  public shouldUpdateStatus(forPolling = false): Observable<OperationStatus> {
    let subject = new Subject<OperationStatus>();
    this.operationStatusService.getStatus(this.operation.getValue().id, forPolling)
      .subscribe(status => {
        this.status.next(status);
        subject.next(status);
      });
    return subject.pipe(take(1));
  }

}
