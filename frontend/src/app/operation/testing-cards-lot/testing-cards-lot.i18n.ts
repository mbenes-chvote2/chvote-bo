/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

export const i18n_testing_cards_lot = {
  fr: {
    "card": {
      "for-voting-material": {
        "title": "Matériel de test",
        "subtitle": "Définir le matériel de test"
      },
      "for-configuration": {
        "title": "Électeurs de test",
        "subtitle": "Définir les cartes de test"
      },
      "lot-count": "Nombre de lot de carte",
      "testing-material": "Matériel de test",
      "testing-material-status": {
        "available": "disponible",
        "unavailable": "indisponible"
      }
    },
    "title": "Lot de cartes de test et de contrôle",
    "subtitle": {
      "list": "Liste des lots de cartes de test et de contrôle",
      "create": "Définir un lot de cartes de test et de contrôle",
      "edit": "Édition d'un lot de cartes de test et de contrôle"
    },
    "no-registry": "Les domaines d'influence ou les référentiels de l'opération n'ont pas été définis, vous ne pouvez pas modifier les lots de cartes de test",
    "actions": {
      "create": "Définir un lot de cartes",
      "delete": "Supprimer un lot de cartes"
    },
    "dialogs": {
      "delete": "Voulez-vous vraiment supprimer le lot de cartes ?",
      "save-success": "Le lot de cartes a été correctement enregistré.",
      "update-success": "Le lot de cartes a été correctement mis à jour."
    },
    "grid": {
      "header": {
        "lot-name": "Nom du lot",
        "card-type": "Type de carte",
        "count": "Nb cartes à générer",
        "voter": "Électeur",
        "language": "Langue",
        "should-print": "Impression",
        "actions": "Actions"
      }
    },
    "form": {
      "voter-sub-section-title": "Saisir un électeur virtuel pour le lot",
      "values": {
        "technical-printer": "Imprimeur virtuel",
        "all-printers": "Tous les imprimeurs",
        "printer-config-not-defined": "Non défini",
        "should-print": {
          "true": "Imprimer",
          "false": "Ne pas imprimer"
        }
      },
      "placeholder": {
        "lot-name": "Nom du lot",
        "card-type": "Sélectionner le type de carte",
        "card-type-fixed": "Type de carte",
        "should-print": "Choix d'impression",
        "count": "Nombre de cartes à générer",
        "printer": "Imprimeur",
        "signature": "Civilité",
        "first-name": "Prénom",
        "last-name": "Nom",
        "birthday": "Date de naissance",
        "address1": "Complément d'adresse",
        "street": "Rue",
        "postal-code": "Code postal",
        "city": "Ville",
        "country": "Pays",
        "doi": "Domaines d'influence",
        "language": "Langue de communication"
      }
    }
  },
  de: {
    "card": {
      "for-voting-material": {
        "title": "DE - Matériel de test",
        "subtitle": "DE - Définir le matériel de test"
      },
      "for-configuration": {
        "title": "DE - Électeurs de test",
        "subtitle": "DE - Définir les cartes de test"
      },
      "lot-count": "DE - Nombre de lot de carte",
      "testing-material": "DE - Matériel de test",
      "testing-material-status": {
        "available": "DE - disponible",
        "unavailable": "DE - indisponible"
      }
    },
    "title": "DE - Lot de cartes de test et de contrôle",
    "subtitle": {
      "list": "DE - Liste des lots de cartes de test et de contrôle",
      "create": "DE - Définir un lot de cartes de test et de contrôle",
      "edit": "DE - Édition d'un lot de cartes de test et de contrôle"
    },
    "actions": {
      "create": "DE - Définir un lot de cartes",
      "delete": "DE - Supprimer un lot de cartes"
    },
    "dialogs": {
      "delete": "DE - Voulez-vous vraiment supprimer lot de cartes ?",
      "save-success": "DE - Le lot de cartes a été correctement enregistré.",
      "update-success": "DE - Le lot de cartes a été correctement mis à jour."
    },
    "grid": {
      "header": {
        "lot-name": "DE - Nom du lot",
        "card-type": "DE - Type de carte",
        "count": "DE - Nb cartes à générer",
        "voter": "DE - Électeur",
        "language": "DE - Langue",
        "should-print": "DE - Impression",
        "actions": "DE - Actions"
      }
    },
    "form": {
      "voter-sub-section-title": "DE - Saisir un électeur virtuel pour le lot",
      "values": {
        "technical-printer": "DE - Imprimeur virtuel",
        "printer-config-not-defined": "DE - Non défini",
        "should-print": {
          "true": "DE - Imprimer",
          "false": "DE - Ne pas imprimer"
        }
      },
      "placeholder": {
        "lot-name": "DE - Nom du lot",
        "card-type": "DE - Sélectionner le type de carte",
        "card-type-fixed": "DE - Type de carte",
        "should-print": "DE - Choix d'impression",
        "count": "DE - Nombre de cartes à générer",
        "printer": "DE - Imprimeur",
        "signature": "DE - Civilité",
        "first-name": "DE - Prénom",
        "last-name": "DE - Nom",
        "birthday": "DE - Date de naissance",
        "address1": "DE - Complément d'adresse",
        "street": "DE - Rue",
        "postal-code": "DE - Code postal",
        "city": "DE - Ville",
        "country": "DE - Pays",
        "doi": "DE - Domaines d'influence",
        "language": "DE - Langue de communication"
      }
    }
  }
};
