/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

export const i18n_selected_electoral_authority_key = {
  fr: {
    "card": {
      "title": "Clé de l'autorité électorale",
      "subtitle": "Clé publique utilisée pour le chiffrement de l'urne électronique"
    },
    "sidebar": {
      "title": "Clé Autorité Électorale"
    },
    "edit": {
      "title": "Clé de l'autorité électorale",
      "subtitle": "Définir la clé publique utilisée pour le chiffrement de l'urne électronique",
      "electoral-authority-keys-required": "Au moins une clé de l'autorité électorale doit être configurée dans le module administration",
      "form": {
        "fields": {
          "keyId": {
            "placeholder": "Sélectionner une clé"
          }
        }
      }
    },
    "key": {
      "info-title": "Clé sélectionnée",
      "hash": "Empreinte",
      "import-date": "Importée le",
      "label": "Intitulé de la clé",
      "login": "Importée par",
    }
  },
  de: {
    "card": {
      "title": "DE - Clé de l'autorité électorale",
      "subtitle": "DE - Clé publique utilisée pour le chiffrement de l'urne électronique"
    },
    "sidebar": {
      "title": "DE - Clé Autorité Électorale"
    },
    "edit": {
      "title": "DE - Clé de l'autorité électorale",
      "subtitle": "DE - Définir la clé publique utilisée pour le chiffrement de l'urne électronique",
      "electoral-authority-keys-required": "DE - Au moins une clé de l'autorité électorale doit être configurée dans le module administration",
      "form": {
        "fields": {
          "keyId": {
            "placeholder": "DE - Sélectionner une clé"
          }
        }
      }
    },
    "key": {
      "info-title": "DE - Clé sélectionnée",
      "hash": "DE - Empreinte",
      "import-date": "DE - Importée le",
      "label": "DE - Intitulé de la clé",
      "login": "DE - Importée par",
    }
  }
};
