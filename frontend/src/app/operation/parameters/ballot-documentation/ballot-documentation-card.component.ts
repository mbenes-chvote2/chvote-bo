/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs/Subscription';
import { ReadOnlyService } from '../../service/read-only.service';
import { BallotDocumentation } from './model/ballot-documentation';
import { Operation } from '../../model/operation';
import { BallotDocumentationService } from './service/ballot-documentation.service';
import { OperationManagementService } from '../../service/operation-managment.service';

@Component({
  selector: 'ballot-documentation-card',
  templateUrl: './ballot-documentation-card.component.html'
})
export class BallotDocumentationCardComponent implements OnInit, OnDestroy {
  private _readOnly: boolean;
  private _subscriptions: Subscription[] = [];
  @Input() operation: Operation;
  private ballotDocumentations: BallotDocumentation[] = [];
  completed = false;


  constructor(private ballotDocumentationService: BallotDocumentationService,
              private readOnlyService: ReadOnlyService,
              private translateService: TranslateService,
              private operationManagementService: OperationManagementService) {

  }


  ngOnDestroy(): void {
    this._subscriptions.forEach(s => s.unsubscribe());
  }

  get readOnly() {
    return this._readOnly;
  }

  ngOnInit() {
    this.ballotDocumentationService.list(this.operation.id)
      .subscribe(ballotDocumentations => this.ballotDocumentations = ballotDocumentations);

    this._subscriptions.push(
      this.operationManagementService.status.subscribe(
        status => this.completed = status.configurationStatus.completedSections["ballot-document"]),
      this.readOnlyService.isBallotDocumentationInReadOnly()
        .subscribe(
          readOnly => {
            this._readOnly = readOnly;
          }
        ),
    );
  }

  get getBallotDocumentationsLength() {
    return this.ballotDocumentations.length;
  }

  get partiallyComplete() {
    return this.getBallotDocumentationsLength > 0
  }

  get ballotDocumentationCount() {
    if (this.getBallotDocumentationsLength === 0) {
      return this.translateService.instant('parameters.ballot-documentation.card.no-documentation');
    }
    if (this.getBallotDocumentationsLength === 1) {
      return this.translateService.instant('parameters.ballot-documentation.card.1-documentation');
    }
    return this.translateService.instant('parameters.ballot-documentation.card.n-documentation',
      {count: this.getBallotDocumentationsLength});
  }
}
