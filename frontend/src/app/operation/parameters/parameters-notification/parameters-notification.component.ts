/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, Input } from '@angular/core';
import { Operation } from '../../model/operation';
import {
  COMPLETE, DEPLOYMENT_REQUESTED, IN_ERROR, IN_VALIDATION, OperationStatus, VALIDATED
} from '../../model/operation-status';
import { OperationManagementService } from '../../service/operation-managment.service';
import { MatSnackBar } from '@angular/material';
import { UploadService } from '../../../core/service/upload/upload.service';
import { TranslateService } from '@ngx-translate/core';
import { OperationService } from '../../service/operation.service';
import { TdDialogService } from '@covalent/core';
import { AuthorizationService } from '../../../core/service/http/authorization.service';

@Component({
  selector: 'parameters-notification',
  templateUrl: './parameters-notification.component.html',
  styleUrls: ['./parameters-notification.component.scss']
})
export class ParametersNotificationComponent {

  uploadInProgress = false;
  @Input() operation: Operation;
  @Input() operationStatus: OperationStatus;

  constructor(private operationService: OperationService,
              private exportService: UploadService,
              private dialogService: TdDialogService,
              private snackBar: MatSnackBar,
              private translateService: TranslateService,
              private operationManagementService: OperationManagementService,
              private authorizationService: AuthorizationService) {
  }

  get userBelongsToOperationManagementEntity() {
    return this.operation.managementEntity == this.authorizationService.userSnapshot.managementEntity;
  }

  get pactLink() {
    return this.operationStatus.configurationStatus.pactUrl;
  }

  isTestSiteReadyForDeployment() {
    return this.authorizationService.hasAtLeastOneRole(['ROLE_DEPLOY_TEST_SITE']) &&
           this.operationStatus.configurationCompleteAndConsistent &&
           (this.operationStatus.configurationStatus.state == COMPLETE ||
            this.operationStatus.configurationStatus.state == IN_ERROR);
  }

  isTestSiteInValidation() {
    return this.authorizationService.hasAtLeastOneRole(['INVALIDATE_TEST_SITE', 'VALIDATE_TEST_SITE']) &&
           this.operationStatus.configurationStatus.state == IN_VALIDATION;
  }

  isRequestDeploymentShown() {
    return this.operationStatus.configurationStatus.state == VALIDATED;
  }

  isValidateDeploymentShown() {
    return this.operationStatus.configurationStatus.state === DEPLOYMENT_REQUESTED
  }

  uploadParameters() {
    this.uploadInProgress = true;
    this.exportService.exportOperationConfiguration(this.operation.id).subscribe(() => {
      this.operationManagementService.shouldUpdateStatus();
      this.snackBar.open(
        this.translateService.instant('parameters.notification.upload.success'), '', {
          duration: 5000
        });
      this.uploadInProgress = false;
    }, () => {
      this.uploadInProgress = false;
    });
  }

  validateParameters() {
    this.operationService.validateTestSite(this.operation.id)
      .subscribe(() => {
        this.snackBar.open(
          this.translateService.instant('parameters.notification.validation.validate.success'), '',
          {duration: 5000}
        );
        this.operationManagementService.shouldUpdateStatus();
      });
  }


  invalidateParameters() {
    this.dialogService.openConfirm({
      message: this.translateService.instant('parameters.notification.validation.invalidate.dialog.message'),
      title: this.translateService.instant('parameters.notification.validation.invalidate.dialog.title'),
      cancelButton: this.translateService.instant('global.actions.cancel').toUpperCase(),
      acceptButton: this.translateService.instant('global.actions.validate').toUpperCase(),
    }).afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.operationService.invalidateTestSite(this.operation.id)
          .subscribe(() => {
            this.snackBar.open(
              this.translateService.instant('parameters.notification.validation.invalidate.success'), '',
              {duration: 5000}
            );
            this.operationManagementService.shouldUpdateStatus();
          });
      }
    });
  }

}
