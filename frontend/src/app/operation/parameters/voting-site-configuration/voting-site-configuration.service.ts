/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { HttpParameters } from '../../../core/service/http/http.parameters.service';
import { VotingSiteConfiguration } from './voting-site-configuration';


@Injectable()
export class VotingSiteConfigurationService {
  private serviceUrl;

  constructor(private http: HttpClient, private params: HttpParameters) {
    this.serviceUrl = (operation) => `${this.params.apiBaseURL}/operation/${operation}/voting-site-config`;
  }


  public saveOrUpdate(operationId: number, config: VotingSiteConfiguration): Observable<any> {
    return this.http.post<any>(this.serviceUrl(operationId), config);
  }

  public findForOperation(operationId: number): Observable<VotingSiteConfiguration> {
    return this.http.get<VotingSiteConfiguration>(this.serviceUrl(operationId));
  }

}
