/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { OperationManagementService } from '../../../service/operation-managment.service';
import { VotingSiteConfigurationService } from '../voting-site-configuration.service';
import { ReadOnlyService } from '../../../service/read-only.service';
import { Subscription } from 'rxjs/Subscription';
import { VotingSiteConfiguration } from '../voting-site-configuration';
import { Operation } from '../../../model/operation';

@Component({
  selector: 'voting-site-configuration-card',
  templateUrl: './voting-site-configuration-card.component.html',
  styleUrls: ['./voting-site-configuration-card.component.scss']
})
export class VotingSiteConfigurationCardComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  readOnly: boolean;
  config: VotingSiteConfiguration;
  completed = false;

  @Input()
  operation: Operation;

  constructor(private service: VotingSiteConfigurationService,
              private readOnlyService: ReadOnlyService,
              private operationManagementService: OperationManagementService) {
  }


  ngOnInit(): void {
    this.subscriptions.push(
      this.readOnlyService.isVotingSiteConfigurationInReadonly().subscribe(readOnly => this.readOnly = readOnly),
      this.operationManagementService.status.subscribe(
        status => this.completed = status.configurationStatus.completedSections["voting-site-configuration"]),
      this.service.findForOperation(this.operation.id).subscribe(config => this.config = config)
    )
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }


}
