/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { DomainInfluence } from '../model/domain-influence';
import { Component, Input, OnChanges, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { sortOnProperty } from '../../../../core/util/table-utils';
import { map } from 'rxjs/operators';
import { merge } from 'rxjs/observable/merge';

@Component({
  selector: 'domain-influence-grid',
  templateUrl: './domain-influence-grid.component.html'
})
export class DomainInfluenceGridComponent implements OnInit, OnChanges {

  @Input() domains: DomainInfluence[];
  detailDataSource: DomainInfluenceDataSource;
  detailDisplayedColumns = ['type', 'id', 'name', 'shortName'];
  @ViewChild(MatSort) sort: MatSort;
  private domainsSubject: BehaviorSubject<DomainInfluence[]> = new BehaviorSubject<DomainInfluence[]>([]);

  constructor() {
  }

  ngOnInit() {
    this.detailDataSource = new DomainInfluenceDataSource(this.domainsSubject, this.sort);
  }

  ngOnChanges(): void {
    this.domainsSubject.next(this.domains);
  }
}

class DomainInfluenceDataSource extends DataSource<DomainInfluence> {

  constructor(private domains: BehaviorSubject<DomainInfluence[]>, private sort: MatSort) {
    super();
  }

  connect(): Observable<DomainInfluence[]> {
    return merge(this.domains, this.sort.sortChange).pipe(
      map(() => sortOnProperty(this.domains.getValue(), this.sort.active, this.sort.direction))
    );
  }

  disconnect() {
  }
}

