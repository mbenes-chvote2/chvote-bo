/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { DomainInfluenceFile } from './model/domain-influence-file';
import * as _ from "underscore";
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs/Subscription';
import { OperationManagementService } from '../../service/operation-managment.service';
import { ReadOnlyService } from '../../service/read-only.service';

@Component({
  selector: 'domain-influence-card',
  templateUrl: './domain-influence-card.component.html'
})
export class DomainInfluenceCardComponent implements OnInit, OnDestroy {
  private _subscriptions: Subscription[] = [];
  private _readOnly = false;

  @Input() file: DomainInfluenceFile;
  domainCountByType;
  completed = false;
  inError = false;

  constructor(private readOnlyService: ReadOnlyService,
              private translateService: TranslateService,
              private operationManagementService: OperationManagementService) {
  }

  ngOnDestroy(): void {
    this._subscriptions.forEach(s => s.unsubscribe());
  }

  get readOnly() {
    return this._readOnly;
  }

  ngOnInit() {
    this._subscriptions.push(
      this.operationManagementService.status.subscribe(status => {
        this.completed = status.configurationStatus.completedSections["domain-of-influence"];
        this.inError = status.configurationStatus.sectionsInError["domain-of-influence"];
      }),
      this.readOnlyService.isDoiInReadOnly()
        .subscribe(
          readOnly => {
            this._readOnly = readOnly;
          }
        ),
    );

  }

  ngOnChanges(): void {
    this.domainCountByType = _.countBy(this.file.details, detail => {
      return detail.type;
    });
  }

  get domainTypes(): string[] {
    return Object.keys(this.domainCountByType);
  }

  domainCount(type: string): string {
    let count: number = this.domainCountByType[type];
    if (count === 1) {
      return this.translateService.instant('parameters.domain-influence.card.1-domain');
    }
    return this.translateService.instant('parameters.domain-influence.card.n-domain', {count: count});
  }
}
