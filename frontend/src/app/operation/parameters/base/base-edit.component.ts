/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Operation } from '../../model/operation';
import { OperationService } from '../../service/operation.service';
import { MatSnackBar } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { OperationParametersModel } from '../model/operation-parameters.model';
import { BaseSaveOrEditComponent } from './base-save-or-edit-component';

import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { ReadOnlyService } from 'app/operation/service/read-only.service';
import { enableFormComponent } from '../../../core/util/form-utils';
import { BaseConfiguration } from './base-configuration';

@Component({
  templateUrl: './base-edit.component.html',
  styleUrls: ['../parameter-edit.scss']
})
export class BaseEditComponent extends BaseSaveOrEditComponent implements OnInit, OnDestroy, AfterViewInit {

  private _subscriptions: Subscription[] = [];
  private _readOnly = false;
  _operationDateFixed;


  constructor(formBuilder: FormBuilder,
              private operationService: OperationService,
              private parametersModel: OperationParametersModel,
              private translateService: TranslateService,
              private router: Router,
              private readOnlyService: ReadOnlyService,
              private snackBar: MatSnackBar) {
    super(formBuilder);
    this.operation = new Operation();
    this.resetForm();
  }


  ngOnDestroy(): void {
    this._subscriptions.forEach(s => s.unsubscribe());
  }

  get readOnly() {
    return this._readOnly;
  }

  ngOnInit() {
    this._subscriptions.push(
      this.readOnlyService.isBaseParameterInReadOnly()
        .subscribe(
          readOnly => {
            this._readOnly = readOnly;
            this.updateFormEnable();
          }
        ),

      this.parametersModel.currentOperation.subscribe(operation => {
          this.operation = operation;
          this.resetForm();
        }
      ),
      this.parametersModel.isOperationDateEditable.subscribe(editable => {
        this._operationDateFixed = !editable;
        this.updateFormEnable();
      })
    );

  }


  updateFormEnable() {
    enableFormComponent(this.baseParametersForm.controls.longLabel, !this.readOnly);
    enableFormComponent(this.baseParametersForm.controls.shortLabel, !this.readOnly);
    enableFormComponent(this.baseParametersForm.controls.date, !this.readOnly && !this._operationDateFixed);
  }

  doSave(baseConfig: BaseConfiguration) {
    this.operationService.updateBaseConfiguration(this.operation.id, baseConfig).subscribe(operation =>
    {
      this.parametersModel.updateOperation(operation);
      this.router.navigate(['/operations', operation.id, 'parameters']);
      this.snackBar.open(
        this.translateService.instant('parameters.base.edit.form.save-success',
          {operation: operation.longLabel}),
        '',
        {
          duration: 5000
        });
    });

  }

  public cancel(): void {
  }

  get createMode(): boolean {
    return false;
  }
}
