/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, Input, OnInit } from '@angular/core';
import { HighlightedQuestionService } from '../services/highlighted-question.service';
import { Operation } from '../../../../model/operation';
import { HighlightedQuestion } from '../model/highlighted-question';
import { TranslateService } from '@ngx-translate/core';
import { TdDialogService } from '@covalent/core';

@Component({
  selector: 'highlighted-question-list',
  templateUrl: './highlighted-question-list.component.html',
  styleUrls: ['./highlighted-question-list.component.scss']
})
export class HighlightedQuestionListComponent implements OnInit {
  ready = false;
  @Input() operation: Operation;
  @Input() readOnly = true;
  questions: HighlightedQuestion[];
  editedQuestion: HighlightedQuestion;

  constructor(private highlightedQuestionService: HighlightedQuestionService,
              private translateService: TranslateService,
              private dialogService: TdDialogService) {
  }

  get inEdition() {
    return !!this.editedQuestion;
  }


  ngOnInit() {
    this.refresh();
  }

  deleteQuestion(questionId: number, index: number) {
    this.dialogService.openConfirm({
      message: this.translateService.instant('parameters.highlighted-question.delete-question-dialog.message',
        {index: index}),
      disableClose: true,
      cancelButton: this.translateService.instant('global.actions.no'),
      acceptButton: this.translateService.instant('global.actions.yes'),
      width: "55rem",
    }).afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.highlightedQuestionService.deleteQuestion(this.operation.id, questionId)
          .subscribe(() => this.questions = this.questions.filter(q => q.id !== questionId));
      }
    });
  }


  refresh() {
    this.ready = false;
    this.editedQuestion = null;
    this.highlightedQuestionService.list(this.operation.id)
      .subscribe(questions => {
        this.questions = questions;
        this.ready = true;
      });
  }


  addQuestion() {
    this.editedQuestion = {
      localizedQuestions: []
    };
  }

}
