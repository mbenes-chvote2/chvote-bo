/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, OnDestroy, OnInit } from '@angular/core';
import { Operation } from '../model/operation';
import { OperationParametersModel } from './model/operation-parameters.model';
import { DomainInfluenceFile } from './domain-influence/model/domain-influence-file';
import { RepositoryFile } from './repository/model/repository-file';
import { OperationFile } from '../../core/model/operation-file';
import { OperationStatus } from '../model/operation-status';
import { Subscription } from 'rxjs/Subscription';
import { OperationManagementService } from '../service/operation-managment.service';
import * as _ from "underscore";
import { ElectionPagePropertiesService } from './election-page-properties/services/election-page-properties.service';

@Component({
  selector: 'parameters',
  templateUrl: './parameters.component.html',
  styleUrls: ['./parameters.component.scss']
})
export class ParametersComponent implements OnInit, OnDestroy {
  operation: Operation;
  operationStatus: OperationStatus;
  domainInfluenceFile: DomainInfluenceFile;
  repositoryFiles: RepositoryFile[];
  documentFiles: OperationFile[];
  hasElection: boolean;
  hasElectionPageModel: boolean;
  private subscriptions: Subscription[] = [];

  constructor(private parametersModel: OperationParametersModel,
              private operationManagementService: OperationManagementService,
              private electionPagePropertiesService: ElectionPagePropertiesService) {
  }

  ngOnInit() {
    this.subscriptions.push(
      this.parametersModel.currentOperation.subscribe(operation => this.operation = operation),
      this.parametersModel.currentDomainInfluenceFile.subscribe(file => this.domainInfluenceFile = file),
      this.parametersModel.currentRepositoryFiles.subscribe(files => {
        this.repositoryFiles = files;
        this.hasElection = _.find(files, function (file) {
          return file.type === 'ELECTION_REPOSITORY';
        }) !== undefined;
        this.electionPagePropertiesService.getModelsForOperation(this.operation.id)
          .subscribe(models => this.hasElectionPageModel = models.length > 0);
      }),
      this.parametersModel.currentDocumentFiles.subscribe(files => this.documentFiles = files),
      this.operationManagementService.status.subscribe(operationStatus => this.operationStatus = operationStatus)
    )
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

}
