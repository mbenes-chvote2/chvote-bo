/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  enableFormComponents, focusOnError, isFormModifiedAndNotSaved, REGEX_ALPHA_NUMERICAL_WITH_ACCENT, validateRegex
} from '../../../core/util/form-utils';

import { ReadOnlyService } from '../../service/read-only.service';
import { Subscription } from 'rxjs/Subscription';
import { ElectionPagePropertiesService } from './services/election-page-properties.service';
import { OperationManagementService } from '../../service/operation-managment.service';
import { Operation } from '../../model/operation';
import { Model } from './model/election-page-properties';
import { ConfirmBeforeQuit } from '../../../core/confirm-before-quit';
import { DictionaryService } from '../../../core/service/dictionary.service';

@Component({
  selector: 'election-page-properties-model-edit',
  templateUrl: './election-page-properties-model-edit.component.html',
  styleUrls: ['./election-page-properties-model-edit.component.scss']
})
export class ElectionPagePropertiesModelEditComponent implements OnInit, OnDestroy, ConfirmBeforeQuit {
  form: FormGroup;

  mode = "create";
  private saved = false;
  private subscriptions: Subscription[] = [];
  readOnly = false;
  operation: Operation;
  possibleCandidateInformationDisplayModels: string[] = [];
  possibleColumnsForVerificationsCodes: string[];

  displayedColumnsOnVerificationTable: string[];
  columnsOrderOnVerificationTable: string[];


  constructor(fb: FormBuilder, private route: ActivatedRoute, private router: Router,
              private readOnlyService: ReadOnlyService,
              private service: ElectionPagePropertiesService,
              private operationManagementService: OperationManagementService,
              private dictionaryService: DictionaryService) {

    this.form = fb.group({
      id: 0,
      modelName: [null, [
        Validators.required,
        Validators.maxLength(255),
        validateRegex(REGEX_ALPHA_NUMERICAL_WITH_ACCENT, "onlyAlphaNumerical")
      ]],
      displayCandidateSearchForm: [false],
      allowChangeOfElectoralList: [false],
      displayEmptyPosition: [false],
      displayCandidatePositionOnACompactBallotPaper: [false],
      displayCandidatePositionOnAModifiedBallotPaper: [false],
      displaySuffrageCount: [false],
      displayListVerificationCode: [false],
      candidateInformationDisplayModel: [null, [Validators.required]],
      allowOpenCandidature: [false],
      allowMultipleMandates: [false],
      displayVoidOnEmptyBallotPaper: [false]
    });


  }

  isFormModified(): boolean {
    return isFormModifiedAndNotSaved(this.form, this.saved);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }


  ngOnInit() {
    this.subscriptions.push(
      this.readOnlyService.isElectionPagePropertiesInReadOnly().subscribe(readOnly => this.updateForReadonly(readOnly)),
      this.operationManagementService.operation.subscribe(operation => {
        this.operation = operation;

        this.dictionaryService.getCandidateInformationDisplayModels(operation.id)
          .subscribe(candidateDisplayModels => this.possibleCandidateInformationDisplayModels = candidateDisplayModels);

        this.dictionaryService.getColumnsForVerificationsCodes(operation.id)
          .subscribe(
            columnsForVerificationsCodes => {
              this.possibleColumnsForVerificationsCodes = columnsForVerificationsCodes;
              if (!this.displayedColumnsOnVerificationTable) {
                this.displayedColumnsOnVerificationTable = columnsForVerificationsCodes;
              }
            });


      }),
      this.route.data.subscribe(data => this.updateFormWithModel(data.model))
    );
  }


  private updateFormWithModel(model: Model) {
    this.form.reset(model);
    if (model) {
      this.displayedColumnsOnVerificationTable = model.displayedColumnsOnVerificationTable;
      this.columnsOrderOnVerificationTable = model.columnsOrderOnVerificationTable;
    } else {
      this.columnsOrderOnVerificationTable = [];
    }
  }


  private updateForReadonly(readOnly: boolean) {
    this.readOnly = readOnly;
    enableFormComponents(this.form.controls, !readOnly);
  }


  save(): void {
    Object.keys(this.form.controls).forEach(k => this.form.controls[k].markAsTouched());
    this.form.updateValueAndValidity();
    if (this.form.valid) {
      this.service.saveOrUpdateModel(this.operation.id,
        Object.assign({}, this.form.getRawValue(), {
          columnsOrderOnVerificationTable: this.columnsOrderOnVerificationTable,
          displayedColumnsOnVerificationTable: this.displayedColumnsOnVerificationTable
        })).subscribe(() => {
        this.saved = true;
        this.router.navigate(['../..'], {relativeTo: this.route});
        this.operationManagementService.shouldUpdateStatus(false);
      });
    } else {
      focusOnError(this.form);
    }
  }

}
