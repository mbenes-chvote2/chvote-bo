/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { Model } from '../model/election-page-properties';
import { ElectionPagePropertiesService } from '../services/election-page-properties.service';
import { Operation } from '../../../model/operation';


export interface ModelSelectorData {
  operation: Operation;
  possibleModels: Model[];
  possibleBallots: string[];
}

@Component({
  selector: "election-page-bulk-update",
  templateUrl: './model-selector.component.html',
  styleUrls: ['./model-selector.component.scss']
})
export class ModelSelectorComponent {
  selectedModelId: number;
  revokeError: string = null;
  dataSource = new MatTableDataSource();
  selection = new SelectionModel<any>(true, []);


  constructor(@Inject(MAT_DIALOG_DATA) public data: ModelSelectorData,
              private service: ElectionPagePropertiesService,
              private dialogRef: MatDialogRef<ModelSelectorComponent>) {
    this.search("");
  }

  get displayedColumns() {
    return ["select", "ballot"];
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected == numRows;
  }


  search(q) {
    this.dataSource.data =
      this.data.possibleBallots
        .filter(ballot => ballot.toLocaleLowerCase().indexOf(q.toLocaleLowerCase()) >= 0)
        .map(ballot => {
          return {ballot}
        });
    this.selection.clear();
  }


  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  get selectedValues() {
    return this.data.possibleBallots
  }


  validate() {
    let selectedBallots: string[] = this.selection.selected.map(v => v.ballot);
    this.dialogRef.close();
    this.service.associateModelToBallots(this.data.operation.id, this.selectedModelId, selectedBallots).subscribe();
  }


}
