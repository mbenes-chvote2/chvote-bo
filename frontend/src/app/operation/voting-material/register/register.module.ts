/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { NgModule } from '@angular/core';

import { SharedModule } from '../../../shared/shared.module';
import { RegisterService } from './services/register.service';
import { RegisterListComponent } from './register-list.component';
import { RegisterUploadComponent } from './register-upload/register-upload.component';
import { RegisterCardComponent } from './register-card/register-card.component';

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    RegisterListComponent,
    RegisterUploadComponent,
    RegisterCardComponent
  ],
  providers: [
    RegisterService
  ],
  exports: [
    RegisterCardComponent,
    RegisterListComponent,
    RegisterUploadComponent,
  ]
})
export class RegisterModule {
}


