/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, Input, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { MatSort } from "@angular/material";
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { ActivatedRoute, Router } from "@angular/router";
import { DataSource } from "@angular/cdk/collections";
import { Observable } from "rxjs/Observable";
import { TdDialogService } from "@covalent/core";
import { TranslateService } from "@ngx-translate/core";
import { Operation } from "../../model/operation";
import { sortOnProperty } from "../../../core/util/table-utils";
import { ImportedRegisterFile } from "./model/imported-register-file";
import { RegisterService } from "./services/register.service";
import { OperationManagementService } from '../../service/operation-managment.service';
import { map, merge } from 'rxjs/operators';
import { AuthorizationService } from '../../../core/service/http/authorization.service';
import { ReadOnlyService } from '../../service/read-only.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'register-list',
  templateUrl: './register-list.component.html',
  styleUrls: ['./register-list.component.scss']
})
export class RegisterListComponent implements OnInit, OnDestroy {
  dataSource: ImportedRegisterFileDataSource;
  @Input()
  operation: Operation;
  importedRegisterFiles = new BehaviorSubject<ImportedRegisterFile[]>([]);
  @ViewChild(MatSort) sort: MatSort;
  private _subscriptions: Subscription[] = [];

  constructor(private route: ActivatedRoute,
              private dialogService: TdDialogService,
              private translateService: TranslateService,
              private router: Router,
              private operationManagementService: OperationManagementService,
              private authorizationService: AuthorizationService,
              private readOnlyService: ReadOnlyService,
              private registerService: RegisterService) {
  }

  private _readOnly = false;

  get readOnly(): boolean {
    return this._readOnly;
  }

  get displayedColumns() {

    return ["managementEntity", "fileName", "uploadDetails", "statistics"]
      .concat(this.readOnly || !this.authorizationService.hasAtLeastOneRole(['DELETE_REGISTER_FILE']) ? [] : ["delete"])
      .concat(this.authorizationService.hasAtLeastOneRole(['GET_REGISTER_REPORT']) ? ["detailedReport"] : []);
  }

  get registerCount(): number {
    let registers = this.importedRegisterFiles.getValue();
    return registers ? registers.length : 0;
  }

  ngOnDestroy(): void {
    this._subscriptions.forEach(s => s.unsubscribe());
  }

  ngOnInit() {
    this.registerService.getReports(this.operation.id).subscribe(reports => this.importedRegisterFiles.next(reports));
    this.dataSource = new ImportedRegisterFileDataSource(this.importedRegisterFiles, this.sort);
    this._subscriptions.push(
      this.readOnlyService.isRegisterInReadOnly().subscribe(readOnly => this._readOnly = readOnly));

  }

  getReport(messageUniqueId) {
    this.registerService.downloadReport(messageUniqueId, this.operation.id);
  }

  getConsolidatedReport() {
    this.registerService.downloadConsolidatedReport(this.operation.id);
  }

  remove(messageUniqueId, fileName) {
    this.dialogService.openConfirm({
      message: this.translateService.instant('voting-material.register.delete-dialog.message', {fileName: fileName}),
      disableClose: true,
      cancelButton: this.translateService.instant('global.actions.no'),
      acceptButton: this.translateService.instant('global.actions.yes'),
    }).afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.registerService.remove(this.operation.id, messageUniqueId)
          .subscribe(() => {
            this.importedRegisterFiles.next(
              this.importedRegisterFiles.getValue().filter((rf) => rf.messageUniqueId != messageUniqueId));
          });
        this.operationManagementService.shouldUpdateStatus();
      }
    });
  }

  uploadRegister() {
    this.router.navigate(['upload'], {relativeTo: this.route});
  }

  userManagementEntityCanDelete(element: ImportedRegisterFile) {
    const userManagementEntity = this.authorizationService.userSnapshot.managementEntity;
    return element.managementEntity === userManagementEntity ||
           this.operation.managementEntity === userManagementEntity;
  }


}

class ImportedRegisterFileDataSource extends DataSource<any> {

  constructor(private data: BehaviorSubject<ImportedRegisterFile[]>, private sort: MatSort) {
    super();
  }

  connect(): Observable<any[]> {
    return this.data.pipe(
      merge(this.sort.sortChange),
      map(() => sortOnProperty(this.data.getValue(), this.sort.active, this.sort.direction))
    );
  }

  disconnect() {
  }
}
