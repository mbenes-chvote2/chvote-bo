/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { PrinterTemplateService } from '../services/printer-template.service';
import { PrinterTemplate } from '../model/PrinterTemplate';
import { Operation } from '../../../model/operation';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { OperationService } from '../../../service/operation.service';
import { ActivatedRoute, Router } from '@angular/router';
import { OperationManagementService } from '../../../service/operation-managment.service';
import { AuthorizationService } from '../../../../core/service/http/authorization.service';
import { ConfirmBeforeQuit } from '../../../../core/confirm-before-quit';
import { Subscription } from 'rxjs/Subscription';
import { ReadOnlyService } from '../../../service/read-only.service';
import { enableFormComponent, isFormModifiedAndNotSaved } from '../../../../core/util/form-utils';

@Component({
  selector: 'printer-edit',
  templateUrl: './printer-edit.component.html'
})
export class PrinterEditComponent implements OnInit, OnDestroy, ConfirmBeforeQuit {
  templates: PrinterTemplate[];
  form: FormGroup;
  @Input()
  operation: Operation;
  private _subscriptions: Subscription[] = [];
  private saved = false;

  constructor(private printerTemplateService: PrinterTemplateService,
              private formBuilder: FormBuilder,
              private operationService: OperationService,
              private router: Router,
              private route: ActivatedRoute,
              private authorizationService: AuthorizationService,
              private readOnlyService: ReadOnlyService,
              private operationManagementService: OperationManagementService) {
  }

  private _readOnly = false;

  get readOnly(): boolean {
    return this._readOnly;
  }

  get ready() {
    return !!this.templates;
  }

  ngOnDestroy(): void {
    this._subscriptions.forEach(s => s.unsubscribe());
  }

  isFormModified(): boolean {
    return isFormModifiedAndNotSaved(this.form, this.saved);
  }

  ngOnInit() {
    this.printerTemplateService.getAllTemplates().subscribe(templates => this.templates = templates);
    this.form = this.formBuilder.group({
      printerTemplate: [this.operation.printerTemplate, Validators.required]
    });
    this._subscriptions.push(
      this.readOnlyService.isPrinterTemplateInReadOnly().subscribe(readOnly => {
        this._readOnly = readOnly;
        this.updateForReadonly()
      }));
  }

  save() {
    this.form.updateValueAndValidity();
    if (this.form.valid) {
      let printerTemplate = this.form.getRawValue().printerTemplate;
      this.operationService.updatePrinterTemplate(this.operation, printerTemplate)
        .subscribe(() => {
          this.operation.printerTemplate = printerTemplate;
          this.operationManagementService.updateOperation(this.operation);
          this.saved = true;
          this.router.navigate(['..'], {relativeTo: this.route})
        });
    }
  }

  getTemplateForName(name: string) {
    return this.templates.filter(t => t.templateName == name)[0];
  }

  private updateForReadonly() {
    if (this.form) {
      enableFormComponent(this.form.controls.printerTemplate,
        !this.readOnly && this.authorizationService.hasAtLeastOneRole(['SELECT_PRINTER_TEMPLATE']));
    }
  }
}
