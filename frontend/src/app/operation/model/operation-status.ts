/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

export const TEST_SITE_IN_DEPLOYMENT = "TEST_SITE_IN_DEPLOYMENT";
export const IN_ERROR = "IN_ERROR";
export const IN_VALIDATION = "IN_VALIDATION";
export const VALIDATED = "VALIDATED";
export const INVALIDATED = "INVALIDATED";
export const DEPLOYMENT_REQUESTED = "DEPLOYMENT_REQUESTED";
export const DEPLOYMENT_REFUSED = "DEPLOYMENT_REFUSED";
export const DEPLOYED = "DEPLOYED";
export const DEPLOYMENT_TARGET_NOT_DEFINED = "NOT_DEFINED";
export const DEPLOYMENT_TARGET_SIMULATION = "SIMULATION";
export const DEPLOYMENT_TARGET_PRODUCTION = "PRODUCTION";


export const INCOMPLETE = "INCOMPLETE";
export const COMPLETE = "COMPLETE";
export const AVAILABLE_FOR_CREATION = "AVAILABLE_FOR_CREATION";
export const CREATION_REQUESTED = "CREATION_REQUESTED";
export const CREATION_REJECTED = "CREATION_REJECTED";
export const CREATION_FAILED = "CREATION_FAILED";
export const CREATION_IN_PROGRESS = "CREATION_IN_PROGRESS";
export const CREATED = "CREATED";
export const NOT_ENOUGH_VOTES_CAST = "NOT_ENOUGH_VOTES_CAST";
export const INVALIDATION_REQUESTED = "INVALIDATION_REQUESTED";
export const INVALIDATION_REJECTED = "INVALIDATION_REJECTED";
export const AVAILABLE_FOR_INITIALIZATION = "AVAILABLE_FOR_INITIALIZATION";
export const INITIALIZATION_REQUESTED = "INITIALIZATION_REQUESTED";
export const INITIALIZATION_REJECTED = "INITIALIZATION_REJECTED";
export const INITIALIZATION_IN_PROGRESS = "INITIALIZATION_IN_PROGRESS";
export const INITIALIZATION_FAILED = "INITIALIZATION_FAILED";
export const INITIALIZED = "INITIALIZED";
export const NOT_REQUESTED = "NOT_REQUESTED";

interface ConsistencyErrorVo {
  errorKey: string,
  errorsParameters: string[]
}


export interface OperationStatus {
  configurationStatus: ConfigurationStatus
  votingMaterialStatus: VotingMaterialStatus
  votingPeriodStatus: VotingPeriodStatus
  tallyArchiveStatus: TallyArchiveStatus
  deploymentTarget: string
  inTestStatusMessage?: MessageWithParameters
  inProductionStatusMessage?: MessageWithParameters
  deploymentStatusMessage?: MessageWithParameters;
  consistencyComputationInProgress: boolean
  configurationConsistencyErrors: ConsistencyErrorVo[]
  votingMaterialConsistencyErrors: ConsistencyErrorVo[]
  configurationCompleteAndConsistent: boolean
  votingMaterialCompleteAndConsistent: boolean
}

export interface TallyArchiveStatus {
  state: string
  tallyArchiveDownloadToken?: string
}


export interface VotingMaterialStatus {
  state: string
  pactUrl: string
  readOnly: boolean
  completedSections: { [K in string]: boolean }
  sectionsInError: { [K in string]: boolean }
  progress?: VotingMaterialCreationProgress
  nonPrintableCardsAvailable: boolean
}

export interface VotingMaterialCreationProgress {
  steps: VotingMaterialCreationProgressStep[]
  stuck: boolean
}

export interface VotingMaterialCreationProgressStep {
  stepId: string
  componentControlStepProgressions: ComponentControlStepProgression[]
  stepStarted: boolean
  stuck: boolean
  percent: number
}

export interface ComponentControlStepProgression {
  stuck: boolean
  percent: number
  index: number
}


export type ReadOnlyConfigurationSection =
  "base-parameter"
  | "milestone"
  | "document"
  | "doi"
  | "management-entity"
  | "repository"
  | "testing-card"
  | "ballot_documentation"
  | "election_page_properties"
  | "voting_site_configuration";

export interface ConfigurationStatus {
  state: string
  pactUrl?: string
  readOnlySections: ReadOnlyConfigurationSection[]
  completedSections: { [K in string]: boolean }
  sectionsInError: { [K in string]: boolean }
  modificationMode: "IN_PARTIAL_MODIFICATION" | "IN_FULL_MODIFICATION" | "NOT_MODIFIABLE" | "PARTIALLY_MODIFIABLE" | "FULLY_MODIFIABLE"
  allSectionsInReadOnly: boolean
  voteReceiverUrl?: string
}


export interface MessageWithParameters {
  message: string
  parameters: any
}


export interface VotingPeriodStatus {
  state: string
  pactUrl?: string
  readOnly: boolean
  completedSections: { [K in string]: boolean }
}
