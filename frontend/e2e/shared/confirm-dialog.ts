/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { browser, by, ElementFinder, ProtractorBrowser } from "protractor";
import { promise } from 'selenium-webdriver';
import { waitForVisibility, waitForInvisibility } from './e2e.utils';
import { Button } from './button';

/**
 * Object used to manipulate a confirmation dialog box.
 */
export class ConfirmDialog {

  private dialog: ElementFinder;

  constructor(private b: ProtractorBrowser = browser) {
    this.dialog = b.element(by.tagName('td-confirm-dialog'));
  }

  /**
   * @returns {ElementFinder} this dialog's main element
   */
  get element(): ElementFinder {
    return this.dialog;
  }

  /**
   * @returns the dialog's content element
   */
  get content(): ElementFinder {
    return this.dialog.element(by.tagName('td-dialog-content'));
  }

  /**
   * @returns {promise.Promise<boolean>} whether the dialog is displayed or not
   */
  waitDisplayed() {
    waitForVisibility(this.element);
  }

  /**
   * Click on the [OK] button
   */
  accept(): void {
    this.waitDisplayed();
    new Button(this.dialog.all(by.tagName('button')).get(1)).click();
    waitForInvisibility(this.element);
  }

  /**
   * Click on the [CANCEL] button
   */
  cancel(): void {
    this.waitDisplayed();
    new Button(this.dialog.all(by.tagName('button')).get(0)).click();
    waitForInvisibility(this.element);
  }
}
