/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { by, ElementFinder } from "protractor";
import { promise } from 'selenium-webdriver';

/**
 * Object used to manipulate an expansion panel.
 */
export class ExpansionPanel {

  constructor(private container: ElementFinder) {
  }

  /**
   * Perform a click on the panel.
   */
  click(): void {
    this.container.click();
  }

  /**
   * @returns {ElementFinder} the panel's header element
   */
  get header(): ElementFinder {
    return this.container.element(by.className('td-expansion-panel-header'));
  }

  /**
   * @returns {promise.Promise<string>} the panel's header label
   */
  get headerLabel(): promise.Promise<string> {
    return this.header.element(by.className('td-expansion-label')).getText();
  }

  /**
   * @returns {promise.Promise<string>} the panel's header sub-label
   */
  get headerSubLabel(): promise.Promise<string> {
    return this.header.element(by.className('td-expansion-sublabel')).getText();
  }

  /**
   * @returns {ElementFinder} the panel's content element
   */
  get content(): ElementFinder {
    return this.container.element(by.className('td-expansion-content'));
  }

  /**
   * @returns {ElementFinder} the panel's summary element
   */
  get summary(): ElementFinder {
    return this.container.element(by.className('td-expansion-summary'));
  }
}
