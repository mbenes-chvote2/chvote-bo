/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { browser, by, ElementFinder, ProtractorBrowser } from "protractor";
import { waitForInvisibility, waitForVisibility } from './e2e.utils';

/**
 * Object used to manipulate a confirmation dialog box.
 */
export class BusinessErrorDialog {

  private dialog: ElementFinder;

  constructor(private b: ProtractorBrowser = browser) {
    this.dialog = b.element(by.id("business-error-dialog"));
  }

  close() {
    waitForVisibility(this.dialog.$("button"), 10000, this.b);
    this.dialog.$("button").click();
    waitForInvisibility(this.dialog, 10000, this.b);
  }

  waitContentTextToBe(text: string) {
    this.b.wait(() => this.dialog.$('mat-dialog-content').getText().then(contentText => contentText == text), 10000,
      "dialog text content should be " + text)

  }
}
