/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { browser, by, ElementFinder, ProtractorBrowser } from 'protractor';
import { waitForInvisibility, waitForVisibility } from './e2e.utils';

/**
 * Page object to manipulate a slide details element.
 */
export class SlideDetails {
  private content: ElementFinder;
  private slideDetails: ElementFinder;
  private details: ElementFinder;
  private backButton: ElementFinder;

  constructor(from: ElementFinder,
              private b: ProtractorBrowser = browser) {
    this.slideDetails = from.element(by.tagName(`slide-details`));
    this.content = this.slideDetails.element(by.tagName(`slide-details-content`));
    this.details = this.slideDetails.element(by.tagName(`slide-details-details`));
    this.backButton = this.slideDetails.element(by.className('details__back'));
  }

  /**
   * Navigate back to content
   */
  back(): void {
    this.backButton.click();
  }

  /**
   * Wait fo the detail view to be displayed
   */
  waitForDetailsDisplayed(): void {
    waitForVisibility(this.details, 10000, this.b);
    waitForInvisibility(this.content, 10000, this.b);
  }

  /**
   * Wait fo the detail view to be hidden
   */
  waitForDetailsHidden(): void {
    waitForInvisibility(this.details, 10000, this.b);
    waitForVisibility(this.content, 10000, this.b);
  }

}
