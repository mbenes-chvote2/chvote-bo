/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { configuration, OperationOption, votingMaterial } from '../../../shared/mock-server';
import { browser, by } from 'protractor';
import {
  DeploymentSummarySection, OperationManagementPage, ParametersNotificationSection
} from '../../../page-object/operation/operation-management.po';
import { Main } from '../../../page-object/main.po';
import { ConsistencyCheck } from '../../../page-object/consistency-check.po';
import { waitForInvisibility } from '../../../shared/e2e.utils';

const FLOW = browser.controlFlow();

describe('Operation/Parameters/Workflow/Modification/Full Modification', () => {

  beforeAll(() => {
    Main.login();
    Main.createOperationAndNavigate(
      'operations/[OPERATION_ID]/voting-material',
      'test-parameter-modif-workflow',
      'TEST_e2e_parameter-modif',
      [
        OperationOption.ADD_ALL,
        OperationOption.FOR_SIMULATION
      ]
    );
  });

  it('should wait consistency to get computed', () => {
    ConsistencyCheck.waitConsistencyCheck();
  });

  it('should have a button do modify parameters', () => {
    expect(DeploymentSummarySection.modifyButton.displayed).toBeTruthy();
  });

  it('should display a confirm when clicking on the button', () => {
    DeploymentSummarySection.modifyButton.click();
    Main.confirmDialog.waitDisplayed();
    Main.confirmDialog.cancel();
    expect(DeploymentSummarySection.modifyButton.displayed).toBeTruthy();
  });

  it('after confirmation should switch to modification mode', () => {
    DeploymentSummarySection.modifyButton.click();
    Main.confirmDialog.waitDisplayed();
    Main.confirmDialog.accept();
    DeploymentSummarySection.waitForTestStatusMessageToMatch(/Paramétrage défini le .*/);
    expect(DeploymentSummarySection.modifyButton.displayed).toBeFalsy();
    expect(ParametersNotificationSection.isTestSectionDisplayed).toBeTruthy();
    expect(ParametersNotificationSection.isProductionSectionDisplayed).toBeTruthy();
  });

  it("should put every card in read write", () => {
    expect(OperationManagementPage.baseCard.readOnly).toBeFalsy();
    expect(OperationManagementPage.milestoneCard.readOnly).toBeFalsy();
    expect(OperationManagementPage.ballotDocumentationCard.readOnly).toBeFalsy();
    expect(OperationManagementPage.documentationCard.readOnly).toBeFalsy();
    expect(OperationManagementPage.domainInfluenceCard.readOnly).toBeFalsy();
    expect(OperationManagementPage.repositoryCard.readOnly).toBeFalsy();
    expect(OperationManagementPage.testingCardsLotCard.readOnly).toBeFalsy();
    expect(OperationManagementPage.electionPagePropertiesCard.readOnly).toBeFalsy();
    expect(OperationManagementPage.managementEntityCard.readOnly).toBeFalsy();
    expect(OperationManagementPage.votingSiteConfigurationCard.readOnly).toBeFalsy();
  });

  it("should allow to redeploy parameter til production get modified", () => {
    ParametersNotificationSection.uploadConfigurationButton.click();
    DeploymentSummarySection.waitForTestStatusMessageToMatch(/En cours de création/);

    FLOW.execute(() => configuration.finishTestDeploymentInPact(Main.operationId));
    DeploymentSummarySection.waitForTestStatusMessageToMatch(/Site en phase de test du paramétrage/);

    ParametersNotificationSection.validateParametersButton.click();
    DeploymentSummarySection.waitForTestStatusMessageToMatch(/Paramétrage validé le /);

    FLOW.execute(() => configuration.requestDeploymentInPact(Main.operationId));
    DeploymentSummarySection.waitForTestStatusMessageToMatch(/Déploiement demandé le/);

    FLOW.execute(() => configuration.validateDeploymentRequestInPact(Main.operationId));


    waitForInvisibility(browser.element(by.tagName("test-summary")));

    expect(ParametersNotificationSection.isProductionSectionDisplayed).toBeTruthy();
    expect(ParametersNotificationSection.isTestSectionDisplayed).toBeFalsy();
  })
});


describe('Operation/Parameters/Workflow/Modification/Partial Modification', () => {

  beforeAll(() => {
    Main.login();
    Main.createOperationAndNavigate(
      'operations/[OPERATION_ID]/voting-material',
      'test-parameter-modif-workflow',
      'TEST_e2e_parameter-modif',
      [
        OperationOption.ADD_ALL,
        OperationOption.FOR_SIMULATION
      ]
    );
  });

  it('should wait consistency to get computed', () => {
    ConsistencyCheck.waitConsistencyCheck();
    FLOW.execute(() => votingMaterial.setVotingMaterialToCreated(Main.operationId))
  });

  it('should have a button do modify parameters', () => {
    expect(DeploymentSummarySection.modifyButton.displayed).toBeTruthy();
    DeploymentSummarySection.modifyButton.click();
    Main.confirmDialog.accept();
  });

  it("should put only some cards in read write", () => {
    expect(OperationManagementPage.baseCard.readOnly).toBeTruthy();
    expect(OperationManagementPage.milestoneCard.readOnly).toBeTruthy();
    expect(OperationManagementPage.ballotDocumentationCard.readOnly).toBeFalsy();
    expect(OperationManagementPage.documentationCard.readOnly).toBeFalsy();
    expect(OperationManagementPage.domainInfluenceCard.readOnly).toBeTruthy();
    expect(OperationManagementPage.repositoryCard.readOnly).toBeFalsy();
    expect(OperationManagementPage.testingCardsLotCard.readOnly).toBeTruthy();
    expect(OperationManagementPage.electionPagePropertiesCard.readOnly).toBeFalsy();
    expect(OperationManagementPage.managementEntityCard.readOnly).toBeTruthy();
    expect(OperationManagementPage.votingSiteConfigurationCard.readOnly).toBeFalsy();
  });

  it("should allow to redeploy parameter til production get modified", () => {
    ParametersNotificationSection.uploadConfigurationButton.click();
    DeploymentSummarySection.waitForTestStatusMessageToMatch(/En cours de création/);

    FLOW.execute(() => configuration.finishTestDeploymentInPact(Main.operationId));
    DeploymentSummarySection.waitForTestStatusMessageToMatch(/Site en phase de test du paramétrage/);

    ParametersNotificationSection.validateParametersButton.click();
    DeploymentSummarySection.waitForTestStatusMessageToMatch(/Paramétrage validé le /);

    FLOW.execute(() => configuration.requestDeploymentInPact(Main.operationId));
    DeploymentSummarySection.waitForTestStatusMessageToMatch(/Déploiement demandé le/);

    FLOW.execute(() => configuration.validateDeploymentRequestInPact(Main.operationId));

    waitForInvisibility(browser.element(by.tagName("test-summary")));

    expect(ParametersNotificationSection.isTestSectionDisplayed).toBeFalsy();
    expect(ParametersNotificationSection.isProductionSectionDisplayed).toBeTruthy();
  })

});
