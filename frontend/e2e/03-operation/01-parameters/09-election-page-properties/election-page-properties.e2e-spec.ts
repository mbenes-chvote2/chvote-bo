/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { ParameterEditPage } from "../../../page-object/operation/parameters/parameter-edit.po";
import { OperationOption } from "../../../shared/mock-server";
import { Main } from '../../../page-object/main.po';
import { OperationManagementPage } from '../../../page-object/operation/operation-management.po';
import {
  BulkUpdatePopup, ElectionPropertiesEdit
} from '../../../page-object/operation/parameters/election-page-properties/election-page-properties-edit.po';
import {
  ElectionPropertiesModelEdit, VerificationCodeTableEdit
} from '../../../page-object/operation/parameters/election-page-properties/election-page-properties-model.po';
import { RepositoryEdit } from '../../../page-object/operation/parameters/repository-edit.po';
import { ConsistencyCheck } from '../../../page-object/consistency-check.po';
import { browser } from 'protractor';
import { waitForVisibility } from '../../../shared/e2e.utils';

let since = require('jasmine2-custom-message');

describe('Operation/Parameters/Election Page Properties', () => {

  beforeAll(() => {
    Main.login();
    Main.createOperationAndNavigate(
      'operations/[OPERATION_ID]/parameters',
      'test_election_page_properties',
      'TEST_e2e_election_page_properties',
      [
        OperationOption.ADD_MILESTONE,
        OperationOption.ADD_DOMAIN_INFLUENCE,
        OperationOption.ADD_REPOSITORY_VOTATION,
        OperationOption.ADD_REPOSITORY_ELECTION,
        OperationOption.ADD_DOCUMENT,
        OperationOption.ADD_TESTING_CARDS
      ]
    );
  });

  describe('no election defined', () => {

    beforeAll(() => {
      Main.updateOperation([OperationOption.REMOVE_REPOSITORY_ELECTION]);
      Main.refresh();
    });

    it('election page properties card should not be displayed in the dashboard', () => {
      expect(OperationManagementPage.electionPagePropertiesCard.button.present).toBeFalsy();
    });

    it('election page properties link should not be displayed in the side menu', () => {
      OperationManagementPage.baseCard.button.click();
      since('navigation list should display #{expected} items (#{actual} were found)')
        .expect(ParameterEditPage.navigationList.itemCount).toBe(10);
      since('operation link should be displayed and required (expected "#{expected}", was "#{actual}")')
        .expect(ParameterEditPage.navigationList.itemText(1)).toBe('Opération *');
      since('milestones link should be displayed and required (expected "#{expected}", was "#{actual}")')
        .expect(ParameterEditPage.navigationList.itemText(2)).toBe('Jalons opérationnels *');
      since('domain of influence link should be displayed and required (expected "#{expected}", was "#{actual}")')
        .expect(ParameterEditPage.navigationList.itemText(3)).toBe('Domaines d\'influence *');
      since('repository link should be displayed and required (expected "#{expected}", was "#{actual}")')
        .expect(ParameterEditPage.navigationList.itemText(4)).toBe('Référentiel(s) *');
      since('testing cards link should be displayed and required (expected "#{expected}", was "#{actual}")')
        .expect(ParameterEditPage.navigationList.itemText(5)).toBe('Électeurs de test *');
      since('documentation link should be displayed (expected "#{expected}", was "#{actual}")')
        .expect(ParameterEditPage.navigationList.itemText(6)).toBe('Documentation');
      since('ballot documentation link should be displayed (expected "#{expected}", was "#{actual}")')
        .expect(ParameterEditPage.navigationList.itemText(7)).toBe('Documentation scrutins');
      since('management entities link should be displayed (expected "#{expected}", was "#{actual}")')
        .expect(ParameterEditPage.navigationList.itemText(8)).toBe('Entités de gestion');
    });
  });

  describe('election defined', () => {

    beforeAll(() => {
      Main.updateOperation([OperationOption.ADD_REPOSITORY_ELECTION]);
      ParameterEditPage.clickBackLink();
      Main.refresh();
    });

    describe('election page properties card before edit', () => {
      it('election page properties should be displayed in the dashboard', () => {
        waitForVisibility(OperationManagementPage.electionPagePropertiesCard.element);
      });

      it('title should be "Paramètres d\'affichage d\'une élection *"', () => {
        expect(OperationManagementPage.electionPagePropertiesCard.title)
          .toBe('Paramètres d\'affichage d\'une élection *');
      });

      it('subtitle should be empty', () => {
        expect(OperationManagementPage.electionPagePropertiesCard.subtitle).toBe('');
      });

      it('card should indicate there is no ballot configured', () => {
        expect(OperationManagementPage.electionPagePropertiesCard.getContentLabel(0)).toBe('Scrutins configurés');
        expect(OperationManagementPage.electionPagePropertiesCard.getContentValue(0)).toBe('0');
      });

      it('card should indicate there is no model configured', () => {
        expect(OperationManagementPage.electionPagePropertiesCard.getContentLabel(1)).toBe('Modèles de paramètres');
        expect(OperationManagementPage.electionPagePropertiesCard.getContentValue(1)).toBe('0');
      });

      it('[CONFIGURE] button should be displayed', () => {
        expect(OperationManagementPage.electionPagePropertiesCard.buttonLabel).toBe('CONFIGURER');
      });

      it('completed badge should not be present', () => {
        expect(OperationManagementPage.electionPagePropertiesCard.completeBadgeDisplayed).toBeFalsy();
      });
    });

    describe('election page properties edit section', () => {
      it('should navigate to mapping page when clicking on the [CONFIGURATION] button', () => {
        OperationManagementPage.electionPagePropertiesCard.button.click();
        expect(ElectionPropertiesEdit.root.present).toBeTruthy();
      });

      it('card title should be "Paramètres d\'affichage d\'une élection"', () => {
        expect(ElectionPropertiesEdit.root.title).toBe('Paramètres d\'affichage d\'une élection');
      });

      it('card subtitle should be "Configurer l\'affichage des scrutins type élection sur le site de vote"', () => {
        expect(ElectionPropertiesEdit.root.subtitle)
          .toContain('Configurer l\'affichage des scrutins type élection sur le site de vote');
      });

      it('should have no mapping section while there is no model', () => {
        expect(ElectionPropertiesEdit.mappings.isPresent()).toBeFalsy();
      });
    });

    describe('model creation', () => {
      it('should open the model edit when clicking on [CREATE MODEL] button', () => {
        ElectionPropertiesEdit.createModelButton.click();
        expect(ElectionPropertiesModelEdit.root.present).toBeTruthy();
      });

      it('card title should be "Définir un modèle de paramètres d\'affichage d\'une élection"', () => {
        expect(ElectionPropertiesModelEdit.root.title)
          .toBe('Définir un modèle de paramètres d\'affichage d\'une élection');
      });

      it('should show an error when clicking on [SAVE] button with uncompleted form', () => {
        ElectionPropertiesModelEdit.saveButton.click();
        expect(ElectionPropertiesModelEdit.modelName.error).toBe('Champ obligatoire');
        expect(ElectionPropertiesModelEdit.candidateInformationDisplayModel.error).toBe('Champ obligatoire');
      });

      it('should display a warning message if we try to quit the model edition page', () => {
        ParameterEditPage.clickBackLink(false);
        waitForVisibility(Main.confirmDialog.element);
        Main.confirmDialog.waitDisplayed();
        expect(Main.confirmDialog.content.getText()).toBe('Voulez vous vraiment interrompre l\'action en cours ?');
        Main.confirmDialog.cancel();
        expect(ElectionPropertiesModelEdit.root.present).toBeTruthy();
      });

      describe('configure verification page model', () => {
        it('should have all columns selected for display', () => {
          expect(VerificationCodeTableEdit.numberOfColumnToDisplay).toBe(4);
          expect(VerificationCodeTableEdit.getColumnToDisplay(0).value).toBe('Numéro du candidat');
          expect(VerificationCodeTableEdit.getColumnToDisplay(1).value).toBe('Identité');
          expect(VerificationCodeTableEdit.getColumnToDisplay(2).value).toBe('Position sur le bulletin');
          expect(VerificationCodeTableEdit.getColumnToDisplay(3).value).toBe('Code de vérification');
          since('button [ADD A COLUMN TO DISPLAY] should be hidden')
            .expect(VerificationCodeTableEdit.addAColumnToDisplayButton.present).toBeFalsy();
        });

        it('should have no columns selected for sorting', () => {
          expect(VerificationCodeTableEdit.numberOfColumnToDisplay).toBe(4);
          expect(VerificationCodeTableEdit.getColumnToDisplay(0).value).toBe('Numéro du candidat');
          expect(VerificationCodeTableEdit.getColumnToDisplay(1).value).toBe('Identité');
          expect(VerificationCodeTableEdit.getColumnToDisplay(2).value).toBe('Position sur le bulletin');
          expect(VerificationCodeTableEdit.getColumnToDisplay(3).value).toBe('Code de vérification');
          since('button [ADD A COLUMN TO SORT] should be displayed')
            .expect(VerificationCodeTableEdit.addAColumnToSortButton.present).toBeTruthy();
        });

        it('should allow user to change order of te columns to display', () => {
          VerificationCodeTableEdit.getColumnToDisplay(0).select('Identité');
          expect(VerificationCodeTableEdit.getColumnToDisplay(0).value).toBe('Identité');
          expect(VerificationCodeTableEdit.getColumnToDisplay(1).value).toBe('Numéro du candidat');
        });

        it('should allow user to remove a columns to display', () => {
          VerificationCodeTableEdit.getColumnToDisplay(0).select('Ne pas afficher la colonne');
          expect(VerificationCodeTableEdit.numberOfColumnToDisplay).toBe(3);
          since('button [ADD A COLUMN TO DISPLAY] should be displayed')
            .expect(VerificationCodeTableEdit.addAColumnToDisplayButton.present).toBeTruthy();
        });

        it('should prevent user to remove all columns to display', () => {
          VerificationCodeTableEdit.getColumnToDisplay(2).select('Ne pas afficher la colonne');
          VerificationCodeTableEdit.getColumnToDisplay(1).select('Ne pas afficher la colonne');
          expect(VerificationCodeTableEdit.numberOfColumnToDisplay).toBe(1);
          since('option "Ne pas afficher la colonne" should not be available')
            .expect(VerificationCodeTableEdit.getColumnToDisplay(0).hasOption('Ne pas afficher la colonne'))
            .toBeFalsy();
        });

        it('should allow user to add a column to sort', () => {
          VerificationCodeTableEdit.addAColumnToSortButton.click();
          expect(VerificationCodeTableEdit.numberOfColumnToSort).toBe(1);
          expect(VerificationCodeTableEdit.getColumnToSort(0).value).toBe('Numéro du candidat');
        });

        it('should allow user to remove a column to sort', () => {
          VerificationCodeTableEdit.getColumnToSort(0).select('Supprimer');
          expect(VerificationCodeTableEdit.numberOfColumnToSort).toBe(0);
        });

        it('should prevent user to have twice the same column to sort', () => {
          VerificationCodeTableEdit.addAColumnToSortButton.click();
          VerificationCodeTableEdit.addAColumnToSortButton.click();
          expect(VerificationCodeTableEdit.numberOfColumnToSort).toBe(2);
          expect(VerificationCodeTableEdit.getColumnToSort(0).value).toBe('Numéro du candidat');
          expect(VerificationCodeTableEdit.getColumnToSort(1).hasOption('Numéro du candidat')).toBeFalsy();
        });

        it('should prevent user to add more column to sort that it exists', () => {
          VerificationCodeTableEdit.addAColumnToSortButton.click();
          VerificationCodeTableEdit.addAColumnToSortButton.click();
          expect(VerificationCodeTableEdit.addAColumnToSortButton.present).toBeFalsy();
        });

      });

      it('should allow to save if the user fill in the form correctly', () => {
        ElectionPropertiesModelEdit.modelName.value = "test1";
        ElectionPropertiesModelEdit.candidateInformationDisplayModel.select("Grand Conseil");
        ElectionPropertiesModelEdit.displayVoidOnEmptyBallotPaper.setValue(true);
        ElectionPropertiesModelEdit.allowMultipleMandates.setValue(true);
        ElectionPropertiesModelEdit.displaySuffrageCount.setValue(true);
        ElectionPropertiesModelEdit.displayCandidatePositionOnAModifiedBallotPaper.setValue(true);
        ElectionPropertiesModelEdit.displayCandidateSearchForm.setValue(true);
        ElectionPropertiesModelEdit.allowChangeOfElectoralRoll.setValue(true);
        ElectionPropertiesModelEdit.displayEmptyPosition.setValue(true);
        ElectionPropertiesModelEdit.displayCandidatePositionOnACompactBallotPaper.setValue(true);
        ElectionPropertiesModelEdit.displayListVerificationCode.setValue(true);
        ElectionPropertiesModelEdit.allowOpenCandidature.setValue(true);
        ElectionPropertiesModelEdit.saveButton.click(true);
        expect(ElectionPropertiesEdit.root.present).toBeTruthy();
      });

      it('should display the model in the mapping page', () => {
        expect(ElectionPropertiesEdit.getModel('test1').present).toBeTruthy();
      });

      it('should display a [DELETE] button on the model', () => {
        expect(ElectionPropertiesEdit.getModel('test1').hasDelete).toBeTruthy();
      });
    });

    describe('model edition', () => {
      it('should switch to edition mode if we click on the model button', () => {
        ElectionPropertiesEdit.getModel("test1").click();
        expect(ElectionPropertiesModelEdit.root.present).toBeTruthy();
        expect(ElectionPropertiesModelEdit.modelName.inputValue).toBe('test1');
        expect(ElectionPropertiesModelEdit.candidateInformationDisplayModel.value).toBe('Grand Conseil');
        expect(ElectionPropertiesModelEdit.displayVoidOnEmptyBallotPaper.checked).toBeTruthy();
        expect(ElectionPropertiesModelEdit.allowMultipleMandates.checked).toBeTruthy();
        expect(ElectionPropertiesModelEdit.displaySuffrageCount.checked).toBeTruthy();
        expect(ElectionPropertiesModelEdit.displayCandidatePositionOnAModifiedBallotPaper.checked).toBeTruthy();
        expect(ElectionPropertiesModelEdit.displayCandidateSearchForm.checked).toBeTruthy();
        expect(ElectionPropertiesModelEdit.allowChangeOfElectoralRoll.checked).toBeTruthy();
        expect(ElectionPropertiesModelEdit.displayEmptyPosition.checked).toBeTruthy();
        expect(ElectionPropertiesModelEdit.displayCandidatePositionOnACompactBallotPaper.checked).toBeTruthy();
        expect(ElectionPropertiesModelEdit.displayListVerificationCode.checked).toBeTruthy();
        expect(ElectionPropertiesModelEdit.allowOpenCandidature.checked).toBeTruthy();
      });

      it('should allow changes', () => {
        ElectionPropertiesModelEdit.modelName.value = 'test2';
        ElectionPropertiesModelEdit.displayVoidOnEmptyBallotPaper.setValue(true);
        ElectionPropertiesModelEdit.saveButton.click(true);
        expect(ElectionPropertiesEdit.root.present).toBeTruthy();
        since('"test1" model should not be present').expect(ElectionPropertiesEdit.getModel('test1').present)
          .toBeFalsy();
        since('"test2" model should be displayed').expect(ElectionPropertiesEdit.getModel('test2').present)
          .toBeTruthy();
      });
    });

    describe('model mapping', () => {
      it('should allow user to define a mapping', () => {
        ElectionPropertiesEdit.getMapping('Conseil d\'Etat 2018').selectField.select('test2');
        ElectionPropertiesEdit.getMapping('Grand Conseil 2018').selectField.select('test2');
        ElectionPropertiesEdit.getMapping('Test majoritaire sans liste 2018').selectField.select('test2');
      });

      it('should not allow to do bulk update if there is less than 4 ballot', () => {
        expect(ElectionPropertiesEdit.bulkUpdateButton.present).toBeFalsy();
      });

      it('should allow user to remove a mapping', () => {
        ElectionPropertiesEdit.getMapping('Grand Conseil 2018').selectField.select('Retirer');
      });

      it('should prevent user to remove a model if it is used in a mapping', () => {
        expect(ElectionPropertiesEdit.getModel('test2').hasDelete).toBeFalsy();
      });

      it('should update summary card accordingly', () => {
        ParameterEditPage.clickBackLink();
        expect(OperationManagementPage.electionPagePropertiesCard.getContentValue(0)).toBe('2');
        expect(OperationManagementPage.electionPagePropertiesCard.getContentValue(1)).toBe('1');
      });
    });

    describe('model mapping deletion', () => {
      it('should display delete button on mapping if the ballot doesn\'t exist anymore in the operation', () => {
        OperationManagementPage.repositoryCard.button.click();
        RepositoryEdit.getDeleteButton(1).click();
        Main.confirmDialog.accept();
        ParameterEditPage.clickBackLink();
        ConsistencyCheck.waitConsistencyCheck();
        OperationManagementPage.electionPagePropertiesCard.button.click();
        expect(ElectionPropertiesEdit.root.present).toBeTruthy();
        expect(ElectionPropertiesEdit.getMapping('Conseil d\'Etat 2018').deleteButton.present).toBeTruthy();
      });

      it('should display a confirm dialog if clicking on delete', () => {
        ElectionPropertiesEdit.getMapping('Conseil d\'Etat 2018').deleteButton.click();
        waitForVisibility(Main.confirmDialog.element);
        expect(Main.confirmDialog.content.getText()).toBe(
          'Le scrutin Conseil d\'Etat 2018 n\'est plus associé à l\'opération. Souhaitez-vous supprimer cette association ?');
      });

      it('should delete the mapping and prevent user to select it anymore after validating', () => {
        Main.confirmDialog.accept();
        expect(ElectionPropertiesEdit.getMapping('Conseil d\'Etat 2018').present).toBeFalsy();
      });
    });

    describe('bulk update', () => {
      it('should display a [BULK UPDATE] button if there is more that 4 elections', () => {
        ParameterEditPage.clickBackLink();
        OperationManagementPage.repositoryCard.button.click();
        RepositoryEdit.uploadInput.file = '../resources/parameters/big-election.xml';
        RepositoryEdit.uploadInput.uploadButton.click();
        ParameterEditPage.clickBackLink();
        OperationManagementPage.electionPagePropertiesCard.button.click();
        expect(ElectionPropertiesEdit.root.present).toBeTruthy();
        expect(ElectionPropertiesEdit.bulkUpdateButton.present).toBeTruthy();
      });

      it('should open a bulk update popup if user click on the button', () => {
        ElectionPropertiesEdit.bulkUpdateButton.click();
        expect(BulkUpdatePopup.present).toBeTruthy();
      });

      it('should not allow to validate if nothing has been selected', () => {
        expect(BulkUpdatePopup.acceptButton.enabled).toBeFalsy();
      });

      it('should allow to filter ballots', () => {
        BulkUpdatePopup.filterInput.value = 'test-test';
        expect(BulkUpdatePopup.allBallots.then(ab => ab.length)).toBe(3);
      });

      it('should prevent user to validate if there is only ballots to be selected', () => {
        BulkUpdatePopup.toggleAll();
        expect(BulkUpdatePopup.acceptButton.enabled).toBeFalsy();
      });

      it('should allow user to validate after selection of the model', () => {
        BulkUpdatePopup.modelSelect.select('test2');
        expect(BulkUpdatePopup.acceptButton.enabled).toBeTruthy();
        BulkUpdatePopup.acceptButton.click();
        BulkUpdatePopup.waitForInvisibility();
      });


      it('should update mapping accordingly', () => {
        browser.wait(
          ElectionPropertiesEdit.getMapping('test-test-4').selectField.value.then(value => value == "test2"),
          5000,
          "expected test-test-4 ballot to have test2 as model"
        );
      });
    });

    describe('configuration not ready for deployment', () => {
      it('should have parameter\'s tab in error state', () => {
        ParameterEditPage.clickBackLink();
        ConsistencyCheck.waitConsistencyCheck();
        expect(OperationManagementPage.getNavigationTabIcon(0)).toBe('warning');
      });

      it('should have corresponding cards in error state', () => {
        expect(OperationManagementPage.repositoryCard.inErrorBadgeDisplayed).toBeTruthy();
        expect(OperationManagementPage.electionPagePropertiesCard.inErrorBadgeDisplayed).toBeTruthy();
      });

      it('should have corresponding navigation items in error state', () => {
        OperationManagementPage.electionPagePropertiesCard.button.click();
        expect(ParameterEditPage.navigationList.inErrorBadgeDisplayed(4)).toBeTruthy();
        expect(ParameterEditPage.navigationList.inErrorBadgeDisplayed(6)).toBeTruthy();
      });

      it('should delete the orphan ballot mapping', () => {
        ElectionPropertiesEdit.getMapping('Test majoritaire sans liste 2018').deleteButton.click();
        Main.confirmDialog.accept();
        expect(ElectionPropertiesEdit.getMapping('Test majoritaire sans liste 2018').present).toBeFalsy();
      });
    });

    describe('right management', () => {
      it('should not display the [DELETE] button if user rights are limited', () => {
        Main.login('ge4');
        Main.navigateTo('/operations/[OPERATION_ID]/parameters/edit/election-page-properties');
        expect(ElectionPropertiesEdit.getMapping('Grand Conseil 2018').deleteButton.present).toBeFalsy();
      });

      it('should not display [CREATE MODEL] button if user rights are limited', () => {
        expect(ElectionPropertiesEdit.createModelButton.present).toBeFalsy();
      });

      it('should display model content even if user rights are limited', () => {
        ElectionPropertiesEdit.getModel('test2').click();
        expect(ElectionPropertiesModelEdit.root.present).toBeTruthy();
      });

      it('should disable the model form if user rights are limited', () => {
        expect(ElectionPropertiesModelEdit.modelName.enabled).toBeFalsy();
        expect(ElectionPropertiesModelEdit.candidateInformationDisplayModel.enabled).toBeFalsy();
        expect(ElectionPropertiesModelEdit.displayVoidOnEmptyBallotPaper.disabled).toBeTruthy();
        expect(ElectionPropertiesModelEdit.allowMultipleMandates.disabled).toBeTruthy();
        expect(ElectionPropertiesModelEdit.displaySuffrageCount.disabled).toBeTruthy();
        expect(ElectionPropertiesModelEdit.displayCandidatePositionOnAModifiedBallotPaper.disabled).toBeTruthy();
        expect(ElectionPropertiesModelEdit.displayCandidateSearchForm.disabled).toBeTruthy();
        expect(ElectionPropertiesModelEdit.allowChangeOfElectoralRoll.disabled).toBeTruthy();
        expect(ElectionPropertiesModelEdit.displayEmptyPosition.disabled).toBeTruthy();
        expect(ElectionPropertiesModelEdit.displayCandidatePositionOnACompactBallotPaper.disabled).toBeTruthy();
        expect(ElectionPropertiesModelEdit.displayListVerificationCode.disabled).toBeTruthy();
        expect(ElectionPropertiesModelEdit.allowOpenCandidature.disabled).toBeTruthy();
        expect(ElectionPropertiesModelEdit.saveButton.present).toBeFalsy();

        // end suit: put the operation in a suitable state before continuing
        Main.updateOperation([
          OperationOption.REMOVE_REPOSITORY_ELECTION,
          OperationOption.ADD_REPOSITORY_ELECTION,
          OperationOption.REMOVE_ELECTION_PAGE_PROPERTIES,
          OperationOption.ADD_ELECTION_PAGE_PROPERTIES
        ]);
        Main.login('ge1');
        Main.navigateTo('/operations/[OPERATION_ID]/parameters');
      });
    });
  });

});
