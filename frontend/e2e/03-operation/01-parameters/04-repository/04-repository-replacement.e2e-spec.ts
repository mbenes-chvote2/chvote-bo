/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { OperationOption, votingMaterial } from "../../../shared/mock-server";
import {
  DeploymentSummarySection, OperationManagementPage
} from '../../../page-object/operation/operation-management.po';
import { RepositoryEdit } from '../../../page-object/operation/parameters/repository-edit.po';
import { Main } from '../../../page-object/main.po';
import { ConsistencyCheck } from '../../../page-object/consistency-check.po';
import { browser } from 'protractor';

const FLOW = browser.controlFlow();
describe('Operation/Parameters/Repository/replacement', () => {

  beforeAll(() => {
    Main.login();
    Main.createOperationAndNavigate(
      'operations/[OPERATION_ID]/parameters',
      'test-parameter-modif-workflow',
      'TEST_e2e_parameter-modif',
      [
        OperationOption.ADD_ALL,
        OperationOption.FOR_SIMULATION
      ]
    );
    FLOW.execute(() => {
      votingMaterial.setVotingMaterialToCreated(Main.operationId);
    });

    ConsistencyCheck.waitConsistencyCheck();
    DeploymentSummarySection.modifyButton.click();
    Main.confirmDialog.accept();
  });

  it('should display the repository section in modification mode', () => {
    OperationManagementPage.repositoryCard.button.click();
    expect(RepositoryEdit.root.displayed).toBeTruthy();
    expect(RepositoryEdit.getDeleteButton(0).present).toBeFalsy();
    expect(RepositoryEdit.uploadInput.uploadButton.present).toBeFalsy();
    expect(RepositoryEdit.getReplaceButton(0).present).toBeTruthy();

  });

  it('should allow modifying a file', () => {
    RepositoryEdit.getReplaceButton(0).click();
    expect(RepositoryEdit.replacementFormIsPresent).toBeTruthy();
    expect(RepositoryEdit.replaceCancelButton).toBeTruthy();

    RepositoryEdit.uploadInput.file = '../resources/parameters/valid-repository-votation.xml';
    RepositoryEdit.uploadInput.uploadButton.click();
    expect(RepositoryEdit.table.displayed).toBeTruthy();
    expect(RepositoryEdit.replacementFormIsPresent).toBeFalsy();
    expect(RepositoryEdit.replaceCancelButton.present).toBeFalsy();
  });

  it('should allow user to cancel a modification', () => {
    RepositoryEdit.getReplaceButton(0).click();
    RepositoryEdit.replaceCancelButton.click();
    expect(RepositoryEdit.table.displayed).toBeTruthy();
    expect(RepositoryEdit.replacementFormIsPresent).toBeFalsy();
    expect(RepositoryEdit.replaceCancelButton.present).toBeFalsy();
  });


});
