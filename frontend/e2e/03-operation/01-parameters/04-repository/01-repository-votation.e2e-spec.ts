/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { OperationOption } from "../../../shared/mock-server";
import { OperationManagementPage } from '../../../page-object/operation/operation-management.po';
import { RepositoryEdit, VotationDetails } from '../../../page-object/operation/parameters/repository-edit.po';
import { BaseEdit } from '../../../page-object/operation/parameters/base-edit.po';
import { ParameterEditPage } from '../../../page-object/operation/parameters/parameter-edit.po';
import { Main } from '../../../page-object/main.po';
import * as moment from "moment";
import { browser, protractor } from 'protractor';
import { waitForVisibility } from '../../../shared/e2e.utils';

let since = require('jasmine2-custom-message');

describe('Operation/Parameters/Repository/Votation', () => {

  beforeAll(() => {
    Main.login();
    Main.createOperationAndNavigate(
      'operations/[OPERATION_ID]/parameters',
      'test_repository_vot',
      'TEST_e2e_repository_votation',
      [
        OperationOption.ADD_MILESTONE,
        OperationOption.ADD_DOMAIN_INFLUENCE
      ]
    );
  });

  describe('repository edit section', () => {

    it('should display the repository section when clicking on the [CONFIGURE] button', () => {
      OperationManagementPage.repositoryCard.button.click();
      expect(RepositoryEdit.root.displayed).toBeTruthy();
    });

    it('card title should be "Référentiel(s) de l\'opération"', () => {
      expect(RepositoryEdit.root.title).toBe('Référentiel(s) de l\'opération');
    });

    it('card subtitle should be "Liste des référentiels de l\'opération"', () => {
      expect(RepositoryEdit.root.subtitle).toContain('Liste des référentiels de l\'opération');
    });

    it('file input should have placeholder "Sélectionner un fichier référentiel"', () => {
      expect(RepositoryEdit.uploadInput.fileInput.placeholder).toBe('Sélectionner un fichier référentiel');
      since('input should be required').expect(RepositoryEdit.uploadInput.fileInput.required).toBeTruthy();
    });

    it('[BROWSE] button should be present', () => {
      since('button should be displayed').expect(RepositoryEdit.uploadInput.browseButton.displayed).toBeTruthy();
      expect(RepositoryEdit.uploadInput.browseButton.text).toBe('attach_file PARCOURIR');
    });

    it('[UPLOAD] button should be present and disabled', () => {
      since('button should be displayed').expect(RepositoryEdit.uploadInput.uploadButton.displayed).toBeTruthy();
      since('button should be disable').expect(RepositoryEdit.uploadInput.uploadButton.enabled).toBeFalsy();
      expect(RepositoryEdit.uploadInput.uploadButton.text).toBe('file_upload IMPORTER');
    });
  });

  describe('import a repository file', () => {
    it('should display an error if the file is not an XML file', () => {
      // given
      RepositoryEdit.uploadInput.file = '../resources/parameters/test-file-1.pdf';
      // when
      RepositoryEdit.uploadInput.uploadButton.click();
      // then
      expect(RepositoryEdit.uploadInput.fileInput.error)
        .toBe('Le fichier sélectionné n\'a pas été reconnu comme un fichier XML');
    });

    it('should display an error if the file is invalid', () => {
      // given
      RepositoryEdit.uploadInput.file = '../resources/parameters/invalid-repository-votation.xml';
      // when
      RepositoryEdit.uploadInput.uploadButton.click();
      // then
      expect(RepositoryEdit.validationErrors.isDisplayed()).toBeTruthy();
    });

    it('should display the result grid if the file is valid', () => {
      // given
      RepositoryEdit.uploadInput.file = '../resources/parameters/valid-repository-votation.xml';
      // when
      RepositoryEdit.uploadInput.uploadButton.click();
      // then
      expect(RepositoryEdit.table.displayed).toBeTruthy();
    });


    it('should display 6 columns...', () => {
      expect(RepositoryEdit.table.headerValue('managementEntity')).toBe('Entité de gestion');
      expect(RepositoryEdit.table.headerValue('type')).toBe('Type');
      expect(RepositoryEdit.table.headerValue('file')).toBe('Fichier');
      expect(RepositoryEdit.table.headerValue('author')).toBe('Auteur');
      expect(RepositoryEdit.table.headerValue('importedDate')).toBe('Importé le');
      expect(RepositoryEdit.table.headerValue('actions')).toBe('Actions');
    });

    it('... And one row', () => {
      since('table should contain one row').expect(RepositoryEdit.table.rowCount).toBe(1);
      expect(RepositoryEdit.table.cellValue(0, 'file')).toContain('VOTATION POPULAIRE - 202012SGA - 12.06.2057');
      expect(RepositoryEdit.table.cellValue(0, 'type')).toBe('Votation');
      expect(RepositoryEdit.table.cellValue(0, 'author')).toBe('ge1');
      expect(RepositoryEdit.table.cellValue(0, 'importedDate')).toBe(moment().format('D.MM.Y'));
    });

    it('should show details when clicking on details button', () => {
      // when
      RepositoryEdit.getDetailButton(0).click();
      // then
      RepositoryEdit.slideDetails.waitForDetailsDisplayed();
    });

    it('should display a summary grouped grid...', () => {
      expect(VotationDetails.groupedGrid.headerValue(0)).toBe('Numéro d\'affichage de la question');
      expect(VotationDetails.groupedGrid.headerValue(1)).toBe('Domaine d\'influence');
      expect(VotationDetails.groupedGrid.headerValue(2)).toBe('Types de réponse acceptés');
    });

    it('... With one group', () => {
      expect(VotationDetails.groupedGrid.groupCount).toBe(1);
      expect(VotationDetails.groupedGrid.groupName(0)).toBe('202012SGA-COM-6624');
    });

    it('should expand the rows when clicking on the group', () => {
      // when
      VotationDetails.groupedGrid.group(0).click();
      // then
      expect(VotationDetails.groupedGrid.rowCount(0)).toBe(2);
    });

    it('should display the repository question information', () => {
      expect(VotationDetails.groupedGrid.cellValue(0, 1, 0)).toBe('1');
      expect(VotationDetails.groupedGrid.cellValue(0, 1, 1)).toBe('6624');
      expect(VotationDetails.groupedGrid.cellValue(0, 1, 2)).toBe('Oui / Non / Vide');
    });
  });

  describe('delete a repository file', () => {

    it('should open a confirmation dialog when clicking on the [DELETE] button', () => {
      RepositoryEdit.slideDetails.back();
      RepositoryEdit.slideDetails.waitForDetailsHidden();
      // when
      RepositoryEdit.getDeleteButton(0).click();
      // then
      since('the confirmation dialog should be displayed').expect(Main.confirmDialog.content.isDisplayed())
        .toBeTruthy();
      expect(Main.confirmDialog.content.getText())
        .toBe('Souhaitez-vous supprimer le référentiel valid-repository-votation.xml ?');
    });

    it('cancelling action should close the dialog', () => {
      // when
      Main.confirmDialog.cancel();
      // then
      expect(Main.confirmDialog.content.isPresent()).toBeFalsy();
    });

    it('accepting action should remove the repository file', () => {
      // when
      RepositoryEdit.getDeleteButton(0).click();
      Main.confirmDialog.accept();
      // then
      expect(Main.confirmDialog.content.isPresent()).toBeFalsy();
      since('file grid should be hidden').expect(RepositoryEdit.table.displayed).toBeFalsy();
    });
  });

  describe('repository summary card', () => {
    it('repository card title should be "Référentiel(s) *"', () => {
      Main.updateOperation([OperationOption.ADD_REPOSITORY_VOTATION]);
      ParameterEditPage.clickBackLink();
      Main.refresh();
      waitForVisibility(OperationManagementPage.repositoryCard.element);
      expect(OperationManagementPage.repositoryCard.title).toBe('Référentiel(s) *');
    });

    it('subtitle should be empty', () => {
      expect(OperationManagementPage.repositoryCard.subtitle).toBe('');
    });

    it('content should display the number of repositories by type', () => {
      since('first type label should be "#{expected}" (was "#{actual}")')
        .expect(OperationManagementPage.repositoryCard.getContentLabel(0)).toBe('Votation');
      since('first type value should be "#{expected}" (was "#{actual}")')
        .expect(OperationManagementPage.repositoryCard.getContentValue(0)).toBe('1 référentiel');
      since('second type label should be "#{expected}" (was "#{actual}")')
        .expect(OperationManagementPage.repositoryCard.getContentLabel(1)).toBe('Élection');
      since('second type value should be "#{expected}" (was "#{actual}")')
        .expect(OperationManagementPage.repositoryCard.getContentValue(1)).toBe('aucun référentiel');
    });

    it('[CONFIGURE] button should be present', () => {
      expect(OperationManagementPage.repositoryCard.buttonLabel).toBe('CONFIGURER');
    });

    it('completed badge should be present', () => {
      expect(OperationManagementPage.repositoryCard.completeBadgeDisplayed).toBeTruthy();
    });

    it('operation\'s date input should be disabled', () => {
      // when
      OperationManagementPage.baseCard.button.click();
      // then
      since('date input should be disabled').expect(BaseEdit.dateInput.enabled).toBeFalsy();
      ParameterEditPage.clickBackLink();
      OperationManagementPage.repositoryCard.button.click();
    });
  });

});
