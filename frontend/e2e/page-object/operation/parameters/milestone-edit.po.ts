/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Card } from '../../../shared/card';
import { DatePicker } from '../../../shared/date-picker';
import { InputField } from '../../../shared/input-field';
import { Button } from '../../../shared/button';
import { Cache } from '../../cache.po';

/**
 * Page object for the milestone edit section.
 */
export class MilestoneEdit {

  static get root(): Card {
    return Cache.card('milestoneEdit');
  }

  static get gracePeriodInput(): InputField {
    return Cache.inputField(this.root.content, 'milestoneEdit', 'gracePeriod');
  }

  static get clearButton(): Button {
    return Cache.button('clearButton');
  }

  static get saveButton(): Button {
    return Cache.button('saveButton');
  }

  /**
   * Get the date input field corresponding to the given milestone type.
   *
   * @param milestoneType the seeked milestone type
   * @returns the corresponding date input field
   */
  static milestoneInput(milestoneType: string): DatePicker {
    return Cache.datePickerField(this.root.content, 'milestoneEdit', `${milestoneType}_Input`, false);
  }
}

