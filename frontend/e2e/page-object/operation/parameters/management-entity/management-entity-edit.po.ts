/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { by, element } from 'protractor';
import { Button } from '../../../../shared/button';
import { Cache } from '../../../cache.po';
import { promise } from 'selenium-webdriver';
import { Card } from '../../../../shared/card';

/**
 * Page object for the management entity edit section.
 */
export class ManagementEntityEdit {

  /**
   * @returns {Card} the edit section element itself
   */
  static get root(): Card {
    return Cache.card('managementEntityEdit');
  }

  /**
   * @returns {promise.Promise<boolean>} whether the "no management entity" message is displayed or not
   */
  static get hasNoManagementEntity(): promise.Promise<boolean> {
    return Cache.element('no-management-entity').isPresent();
  }

  /**
   * @returns {promise.Promise<boolean>} whether the "all management entities" message is displayed or not
   */
  static get hasAllManagementEntities() {
    return Cache.element('all-management-entities').isPresent();
  }

  /**
   * @returns {Button} the [INVITE] button
   */
  static get inviteButton(): Button {
    return Cache.button('inviteButton');
  }

  /**
   * @returns {Button} the [REVOKE] button
   */
  static get revokeButton(): Button {
    return Cache.button('revokeButton');
  }

  /**
   * @returns {promise.Promise<string[]>} the list of invited management entities
   */
  static get invitedManagementEntities(): promise.Promise<string[]> {
    return element
      .all(by.className("management-entity"))
      .map(elementFinder => elementFinder.getText());
  }

}
