/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.repository.entity;

import ch.ge.ve.bo.repository.ConnectedUser;
import java.time.LocalDateTime;
import java.util.Objects;
import javax.persistence.AttributeOverride;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Entity representing an electoral authority key
 */
@Entity
@Table(name = "BO_T_ELECTORAL_AUTHORITY_KEY")
@SequenceGenerator(name = EntityConstants.SEQUENCE_GENERATOR, allocationSize = 1,
                   sequenceName = "BO_S_ELECTORAL_AUTHORITY_KEYS")
@AttributeOverride(name = EntityConstants.PROPERTY_ID, column = @Column(name = "EAK_N_ID"))
@AttributeOverride(name = EntityConstants.PROPERTY_LOGIN, column = @Column(name = "EAK_C_LOGIN"))
@AttributeOverride(name = EntityConstants.PROPERTY_SAVE, column = @Column(name = "EAK_D_SAVE"))

public class ElectoralAuthorityKey extends BaseEntity {

  @Column(name = "EAK_X_CONTENT")
  @Lob
  @Basic(fetch = FetchType.LAZY)
  private byte[] keyContent;

  @Column(name = "EAK_C_NAME")
  private String keyName;

  @Column(name = "EAK_C_MANAGEMENT_ENTITY")
  private String managementEntity;

  @Column(name = "EAK_D_IMPORT_DATE")
  private LocalDateTime importDate;

  public byte[] getKeyContent() {
    return keyContent;
  }

  public void setKeyContent(byte[] keyContent) {
    this.keyContent = keyContent;
  }

  public String getKeyName() {
    return keyName;
  }

  public void setKeyName(String keyName) {
    this.keyName = keyName;
  }

  public String getManagementEntity() {
    return managementEntity;
  }

  public void setManagementEntity(String managementEntity) {
    this.managementEntity = managementEntity;
  }

  @PrePersist
  void setup() {
    if (managementEntity == null) {
      setManagementEntity(ConnectedUser.get().managementEntity);
    }
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof ElectoralAuthorityKey)) {
      return false;
    }
    ElectoralAuthorityKey that = (ElectoralAuthorityKey) o;
    return Objects.equals(getId(), that.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }


  public LocalDateTime getImportDate() {
    return importDate;
  }

  public void setImportDate(LocalDateTime importDate) {
    this.importDate = importDate;
  }
}
