/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.repository.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * A dictionnary entry define by its {@link DictionaryDataType} it content and a optional scope (management entity or
 * realm)
 */
@Entity
@Table(name = "BO_T_DICTIONARY")
@SequenceGenerator(name = EntityConstants.SEQUENCE_GENERATOR, allocationSize = 1, sequenceName = "BO_S_DICTIONARY")
public class Dictionary {

  @Id
  @Column(name = "DIC_N_ID")
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = EntityConstants.SEQUENCE_GENERATOR)
  private Long id;

  @Enumerated(EnumType.STRING)
  @Column(name = "DIC_S_DATA_TYPE")
  private DictionaryDataType dataType;

  @Lob
  @Column(name = "DIC_X_CONTENT")
  private String content;

  @Column(name = "DIC_S_SCOPE_VALUE")
  private String scope;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public DictionaryDataType getDataType() {
    return dataType;
  }

  public void setDataType(DictionaryDataType dictionaryDataType) {
    this.dataType = dictionaryDataType;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getScope() {
    return scope;
  }

  public void setScope(String scope) {
    this.scope = scope;
  }
}
