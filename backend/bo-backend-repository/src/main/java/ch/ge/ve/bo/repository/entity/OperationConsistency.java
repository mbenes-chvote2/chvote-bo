/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.repository.entity;

import ch.ge.ve.bo.repository.security.NoCheckForOperationHolder;
import java.time.LocalDateTime;
import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Consistency report result
 */
@NoCheckForOperationHolder
@Entity
@Table(name = "BO_T_OPE_CONSISTENCY")
@SequenceGenerator(
    name = EntityConstants.SEQUENCE_GENERATOR, allocationSize = 1, sequenceName = "BO_S_OPE_CONSISTENCY")
@AttributeOverride(name = EntityConstants.PROPERTY_ID, column = @Column(name = "CON_N_ID"))
@AttributeOverride(name = EntityConstants.PROPERTY_LOGIN, column = @Column(name = "CON_C_LOGIN"))
@AttributeOverride(name = EntityConstants.PROPERTY_SAVE, column = @Column(name = "CON_D_SAVE"))

public class OperationConsistency extends BaseEntity {


  @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
  @JoinColumn(name = "CON_N_OPE_ID", nullable = false)
  private Operation operation;

  /**
   * Represent the date of the operation (which is the maximum of lastVotingMaterialUpdateDate and
   * lastConfigurationUpdateDate) when the consistency check has been taken into account
   */
  @Column(name = "CON_D_COMPUTATION_DATE", nullable = false)
  @SuppressWarnings("squid:S3437")
  private LocalDateTime computationDate;

  @Lob
  @Column(name = "CON_X_REPORT")
  private String report;

  public Operation getOperation() {
    return operation;
  }

  public void setOperation(Operation operation) {
    this.operation = operation;
  }

  public LocalDateTime getComputationDate() {
    return computationDate;
  }

  public void setComputationDate(LocalDateTime computationDate) {
    this.computationDate = computationDate;
  }

  public String getReport() {
    return report;
  }

  public void setReport(String report) {
    this.report = report;
  }
}
