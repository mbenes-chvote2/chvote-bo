/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.repository.entity;

import ch.ge.ve.bo.repository.security.OperationHolder;
import java.time.LocalDateTime;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Configuration to used for voting site opening closing and grace delay in the case of a simulation
 */
@Entity
@Table(name = "BO_T_SIMUL_PERIOD_CONFIG")
@SequenceGenerator(
    name = EntityConstants.SEQUENCE_GENERATOR, allocationSize = 1, sequenceName = "BO_S_SIMUL_PERIOD_CONFIG")
@AttributeOverride(name = EntityConstants.PROPERTY_ID, column = @Column(name = "SPC_N_ID"))
@AttributeOverride(name = EntityConstants.PROPERTY_LOGIN, column = @Column(name = "SPC_C_LOGIN"))
@AttributeOverride(name = EntityConstants.PROPERTY_SAVE, column = @Column(name = "SPC_D_SAVE"))

public class SimulationPeriodConfiguration extends BaseEntity implements OperationHolder {

  @OneToOne
  @JoinColumn(name = "SPC_N_OPE_ID", nullable = false)
  private Operation operation;

  @Column(name = "SPC_D_DATE_OPEN", nullable = false)
  @SuppressWarnings("squid:S3437") // suppress Sonar check on serialized value-based objects
  private LocalDateTime dateOpen;

  @Column(name = "SPC_D_DATE_CLOSE", nullable = false)
  @SuppressWarnings("squid:S3437") // suppress Sonar check on serialized value-based objects
  private LocalDateTime dateClose;

  @Column(name = "SPC_N_GRACE_PERIOD", nullable = false)
  private Integer gracePeriod;

  @Override
  public Operation getOperation() {
    return operation;
  }

  public void setOperation(Operation operation) {
    this.operation = operation;
  }

  @Override
  public boolean onlyManagementEntityInChargeCanCreate() {
    return true;
  }

  @Override
  public OperationParameterType getOperationParameterType() {
    return OperationParameterType.VOTING_PERIOD;
  }

  public LocalDateTime getDateOpen() {
    return dateOpen;
  }

  public void setDateOpen(LocalDateTime dateOpen) {
    this.dateOpen = dateOpen;
  }

  public LocalDateTime getDateClose() {
    return dateClose;
  }

  public void setDateClose(LocalDateTime dateClose) {
    this.dateClose = dateClose;
  }

  public Integer getGracePeriod() {
    return gracePeriod;
  }

  public void setGracePeriod(Integer gracePeriod) {
    this.gracePeriod = gracePeriod;
  }
}
