/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.repository.exception;

import ch.ge.ve.bo.repository.ConnectedUser;

/**
 * Exception thrown when user cannot access to a given operation
 */
public class AccessToProhibitedOperationException extends RuntimeException {

  /**
   * Exception thrown when user is not connected
   */
  public AccessToProhibitedOperationException(Long operationId) {
    super(String.format("Try to access operation %s without being connected", operationId));
  }

  /**
   * Exception thrown when user belongs to an uninvited management entity
   */
  public AccessToProhibitedOperationException(ConnectedUser connectedUser, Long operationId) {
    super(String.format("Try to access operation %s by user %s from management entity %s",
                        operationId, connectedUser.login, connectedUser.managementEntity));
  }

}
