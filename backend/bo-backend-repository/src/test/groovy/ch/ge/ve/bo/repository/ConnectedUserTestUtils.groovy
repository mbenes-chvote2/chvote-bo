/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.repository

class ConnectedUserTestUtils {
  def static ALL_ROLES = [
          "ROLE_USER", "ROLE_CREATE_OPERATION", "ROLE_CREATE_TEST_VOTING_CARD_DEFINITION", "ROLE_DELETE_DOI",
          "ROLE_DELETE_OPERATION_DOCUMENTATION", "ROLE_DELETE_REGISTER_FILE", "ROLE_DELETE_REPOSITORY",
          "ROLE_DELETE_TEST_VOTING_CARD_DEFINITION", "ROLE_DEPLOY_TEST_SITE", "ROLE_DEPLOY_VOTING_MATERIAL",
          "ROLE_EDIT_TEST_VOTING_CARD_DEFINITION", "ROLE_GET_REGISTER_REPORT", "ROLE_INVALIDATE_TEST_SITE",
          "ROLE_DEPLOYMENT_TARGET_SIMULATION", "ROLE_DEPLOYMENT_TARGET_REAL", "ROLE_SELECT_PRINTER_TEMPLATE",
          "ROLE_SET_OPERATION_DOCUMENTATION", "ROLE_UPDATE_BASE_PARAMETER", "ROLE_UPDATE_MILESTONE",
          "ROLE_UPDATE_VOTING_CARD_TITLE", "ROLE_UPLOAD_DOI", "ROLE_UPLOAD_REGISTER_FILE", "ROLE_UPLOAD_REPOSITORY",
          "ROLE_INVITE_MANAGEMENT_ENTITY", "ROLE_REVOKE_MANAGEMENT_ENTITY", "ROLE_VALIDATE_TEST_SITE",
          "ROLE_DOWNLOAD_GENERATED_CARDS_FOR_TEST_SITE", "ROLE_ADD_HIGHLIGHTED_QUESTION",
          "ROLE_ADD_LOCALIZED_HIGHLIGHTED_QUESTION", "ROLE_DELETE_LOCALIZED_HIGHLIGHTED_QUESTION",
          "ROLE_DELETE_HIGHLIGHTED_QUESTION", "ROLE_ADD_BALLOT_DOCUMENTATION", "ROLE_DELETE_BALLOT_DOCUMENTATION",
          "ROLE_CREATE_ELECTION_PAGE_PROPERTIES_MODEL", "ROLE_DELETE_ELECTION_PAGE_PROPERTIES_MODEL",
          "ROLE_MAP_ELECTION_PAGE_PROPERTIES_MODEL_TO_BALLOT", "ROLE_ADD_ELECTORAL_AUTHORITY_KEY",
          "ROLE_MODIFY_CONFIGURATION", "ROLE_DOWNLOAD_NOT_PRINTABLE_CARDS_FOR_PRODUCTION",
          "ROLE_SELECT_ELECTORAL_AUTHORITY_KEY", "ROLE_SELECT_SIMULATION_PERIOD", "ROLE_DEPLOY_VOTING_PERIOD",
          "ROLE_VALIDATE_VOTING_MATERIAL", "ROLE_CLOSE_VOTING_PERIOD_IN_SIMULATION", "ROLE_DOWNLOAD_TALLY_ARCHIVE",
          "ROLE_UPDATE_VOTING_SITE_CONFIG"
  ]


  static connectPrinter() {
    connectUser("printer", ConnectedUser.REALM_PRINTER, "printer inc.")

  }

  static disconnectUser() {
    ConnectedUser.unset()
  }

  static connectUser(String user = "test", String realm = "GE", String managementEntity = "Canton de Genève") {
    ConnectedUser.set(user, Arrays.asList("BELONGS_TO_REALM:" + realm, "BELONGS_TO_MANAGEMENT_ENTITY:" + managementEntity, "USER"))
  }

  static connectUserWithRole(String role) {
    ConnectedUser.set( "test", Arrays.asList("BELONGS_TO_REALM:GE", "BELONGS_TO_MANAGEMENT_ENTITY:Canton de Genève", "USER", role))
  }

  static connectUserWithAllRoles() {
    ConnectedUser.set("test", ["BELONGS_TO_REALM:GE", "BELONGS_TO_MANAGEMENT_ENTITY:Canton de Genève"] + ALL_ROLES)
  }


}
