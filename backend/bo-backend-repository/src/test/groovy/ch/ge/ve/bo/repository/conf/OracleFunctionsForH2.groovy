/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.repository.conf

import java.sql.Timestamp
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

/**
 * Oracle missing function implementation for H2 database.
 */
class OracleFunctionsForH2 {

    static Timestamp toTimestamp(String value, String pattern) throws Exception {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(convertPattern(pattern))
        return Timestamp.valueOf(LocalDateTime.parse(value, dateTimeFormatter))
    }

    private static String convertPattern(String pattern) {
        pattern = pattern.replaceAll("YY", "yy")
        pattern = pattern.replaceAll("DD", "dd")
        pattern = pattern.replaceAll("HH24|hh24", "HH")
        pattern = pattern.replaceAll("HH?!24|hh?!24", "KK")
        pattern = pattern.replaceAll("MONTH|month", "MMM")
        pattern = pattern.replaceAll("MI|mi", "mm")
        pattern = pattern.replaceAll("SS|ss", "ss")
        pattern = pattern.replaceAll("AM|PM", "aa")
        return pattern
    }
}
