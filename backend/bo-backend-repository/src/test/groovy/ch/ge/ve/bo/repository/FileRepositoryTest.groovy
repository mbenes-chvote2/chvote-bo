/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.repository

import static ch.ge.ve.bo.FileType.VOTATION_REPOSITORY
import static ch.ge.ve.bo.repository.dataset.FileDataset.electionOperationRepository
import static ch.ge.ve.bo.repository.dataset.FileDataset.file

import ch.ge.ve.bo.repository.conf.RepositoryConfigurationTest
import ch.ge.ve.bo.repository.security.SecuredContext
import org.flywaydb.test.FlywayTestExecutionListener
import org.flywaydb.test.annotation.FlywayTest
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.TestExecutionListeners
import org.springframework.test.context.support.AnnotationConfigContextLoader
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener
import spock.lang.Unroll

/**
 * Unit tests for the application files repository (@link FileRepository).
 */
@ContextConfiguration(classes = RepositoryConfigurationTest.class, loader = AnnotationConfigContextLoader.class)
@TestExecutionListeners([DependencyInjectionTestExecutionListener.class, FlywayTestExecutionListener.class])
@FlywayTest(locationsForMigrate = ['/dataset'])
class FileRepositoryTest {

  @Autowired
  FileRepository fileRepository

  @Autowired
  SecuredContext securedContext


  @Unroll
  def 'countByOperationIdAndType(#operationId, #fileType) should return #expectedCount'() {
    expect:
    securedContext.doInSecureContext({
      fileRepository.countByOperationIdAndType(operationId, fileType)
    }) == expectedCount

    where:
    operationId | fileType            || expectedCount
    1           | VOTATION_REPOSITORY || 1
    2           | VOTATION_REPOSITORY || 1
  }

  @Unroll
  def 'findByOperationIdAndType(#operationId, #fileType) should return the corresponding files'() {
    expect:
    securedContext.doInSecureContext({ fileRepository.findByOperationIdAndType(operationId, fileType) }) == expected

    where:
    operationId | fileType            || expected
    1           | VOTATION_REPOSITORY || [electionOperationRepository()]
    2           | VOTATION_REPOSITORY || [file()]
  }

  @Unroll
  def 'findByBusinessKey(#businessKey) should return the corresponding file'() {
    expect:
    securedContext.doInSecureContext({ fileRepository.findByBusinessKey(businessKey) }).orElse(null) == expected

    where:
    businessKey                                            || expected
    '202015SGAvota3://all6c6f4b14b6d349ef97703f2af84fabcd' || electionOperationRepository()
    '202012SGAvota3://all6c6f4b14b6d349ef97703f2af84ffbc6' || file()
    'unknown'                                              || null
  }
}
