/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.mock.server.pact;

import ch.ge.ve.bo.mock.server.NotFound;
import ch.ge.ve.bo.mock.server.services.PactSimulator;
import ch.ge.ve.chvote.pactback.contract.operation.status.VotingPeriodStatusVo;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Profile({"development", "integration"})
public class PactVotingPeriodStatusController {

  private final PactSimulator pactSimulator;

  public PactVotingPeriodStatusController(PactSimulator pactSimulator) {
    this.pactSimulator = pactSimulator;
  }

  @RequestMapping(path = "pact/operation/{operationId}/voting-period/status", method = RequestMethod.GET)
  @ResponseBody
  public VotingPeriodStatusVo getVotingPeriodStatus(@PathVariable("operationId") long operationId) {
    VotingPeriodStatusVo status = pactSimulator.getVotingPeriodStatus(operationId);
    if (status == null) {
      throw new NotFound();
    }
    return status;
  }

  @RequestMapping(path = "pact/operation/{operationId}/reject-voting-period-initialization",
                  method = RequestMethod.GET)
  @ResponseBody
  public void rejectVotingPeriodInitialization(@PathVariable("operationId") long operationId) {
    pactSimulator.setVotingPeriodStatus(operationId, VotingPeriodStatusVo.State.INITIALIZATION_REJECTED,
                                        "Rejected as asked by mock server");
  }


  @RequestMapping(path = "pact/operation/{operationId}/request-voting-period-initialization",
                  method = RequestMethod.GET)
  @ResponseBody
  public void requestVotingPeriodInitialization(@PathVariable("operationId") long operationId) {
    pactSimulator.setVotingPeriodStatus(operationId, VotingPeriodStatusVo.State.INITIALIZATION_REQUESTED, null);
  }

  @RequestMapping(
      path = "pact/operation/{operationId}/set-voting-period-initialization-to-in-progress",
      method = RequestMethod.GET)
  @ResponseBody
  public void setVotingPeriodInitializationToInProgress(@PathVariable("operationId") long operationId) {
    pactSimulator.setVotingPeriodStatus(operationId, VotingPeriodStatusVo.State.INITIALIZATION_IN_PROGRESS, null);
  }


  @RequestMapping(path = "pact/operation/{operationId}/set-voting-period-initialization-to-failed",
                  method = RequestMethod.GET)
  @ResponseBody
  public void setVotingPeriodInitializationToFailed(@PathVariable("operationId") long operationId) {
    pactSimulator.setVotingPeriodStatus(operationId, VotingPeriodStatusVo.State.INITIALIZATION_FAILED, null);
  }

  @RequestMapping(path = "pact/operation/{operationId}/set-voting-period-to-initialized", method = RequestMethod.GET)
  @ResponseBody
  public void setVotingPeriodToInitialized(@PathVariable("operationId") long operationId) {
    pactSimulator.setVotingPeriodStatus(operationId, VotingPeriodStatusVo.State.INITIALIZED, null);
  }


}
