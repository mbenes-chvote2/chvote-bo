/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.mock.server.workflow;

import java.util.function.Function;

public enum BackOfficeUser {
  GE(WorkflowConfiguration::getBoGeUser, WorkflowConfiguration::getBoGePass),
  SG(WorkflowConfiguration::getBoSgUser, WorkflowConfiguration::getBoSgPass),
  BE(WorkflowConfiguration::getBoBeUser, WorkflowConfiguration::getBoBePass);


  private final Function<WorkflowConfiguration, String> userProvider;
  private final Function<WorkflowConfiguration, String> passwordProvider;

  BackOfficeUser(Function<WorkflowConfiguration,String> userProvider,
                 Function<WorkflowConfiguration,String> passwordProvider) {


    this.userProvider = userProvider;
    this.passwordProvider = passwordProvider;
  }

  String getUser(WorkflowConfiguration configuration){
    return userProvider.apply(configuration);
  }

  String getPassword(WorkflowConfiguration configuration){
    return passwordProvider.apply(configuration);
  }


}
