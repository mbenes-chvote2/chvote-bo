/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.mock.server.generator;

import ch.ge.ve.interfaces.ech.eCH0007.v6.CantonAbbreviationType;
import ch.ge.ve.interfaces.ech.eCH0007.v6.SwissMunicipalityType;
import ch.ge.ve.interfaces.ech.eCH0008.v3.CountryType;
import ch.ge.ve.interfaces.ech.eCH0010.v6.AddressInformationType;
import ch.ge.ve.interfaces.ech.eCH0010.v6.OrganisationMailAddressInfoType;
import ch.ge.ve.interfaces.ech.eCH0010.v6.OrganisationMailAddressType;
import ch.ge.ve.interfaces.ech.eCH0010.v6.PersonMailAddressInfoType;
import ch.ge.ve.interfaces.ech.eCH0010.v6.PersonMailAddressType;
import ch.ge.ve.interfaces.ech.eCH0011.v8.ForeignerNameType;
import ch.ge.ve.interfaces.ech.eCH0011.v8.PlaceOfOriginType;
import ch.ge.ve.interfaces.ech.eCH0011.v8.ResidencePermitDataType;
import ch.ge.ve.interfaces.ech.eCH0044.v4.DatePartiallyKnownType;
import ch.ge.ve.interfaces.ech.eCH0044.v4.NamedPersonIdType;
import ch.ge.ve.interfaces.ech.eCH0044.v4.PersonIdentificationType;
import ch.ge.ve.interfaces.ech.eCH0045.v4.AuthorityType;
import ch.ge.ve.interfaces.ech.eCH0045.v4.CantonalRegisterType;
import ch.ge.ve.interfaces.ech.eCH0045.v4.EmailType;
import ch.ge.ve.interfaces.ech.eCH0045.v4.ForeignerPersonType;
import ch.ge.ve.interfaces.ech.eCH0045.v4.ForeignerType;
import ch.ge.ve.interfaces.ech.eCH0045.v4.LanguageType;
import ch.ge.ve.interfaces.ech.eCH0045.v4.SwissAbroadType;
import ch.ge.ve.interfaces.ech.eCH0045.v4.SwissDomesticType;
import ch.ge.ve.interfaces.ech.eCH0045.v4.SwissPersonType;
import ch.ge.ve.interfaces.ech.eCH0045.v4.VotingPersonType;
import ch.ge.ve.interfaces.ech.eCH0045.v4.extension.PersonTypeExtensionType;
import ch.ge.ve.interfaces.ech.eCH0058.v5.HeaderType;
import ch.ge.ve.interfaces.ech.eCH0058.v5.SendingApplicationType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.CountingCircleType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.DomainOfInfluenceType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.DomainOfInfluenceTypeType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.ExtensionType;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.concurrent.ThreadLocalRandom;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RegisterController {

  private static final String DATA_LOCK     = "0";
  private static final String SWITZERLAND   = "CH";
  private static final String ALLIANCE_NAME = "SINGLE";

  private static final Randomizer RANDOMIZER = new Randomizer();

  private enum VoterType {
    SWISS,
    SWISS_ABROAD,
    FOREIGNER
  }

  @ResponseBody
  @GetMapping(value = "/generate/register/{swiss}/{swissAbroad}/{foreigner}",
              produces = MediaType.APPLICATION_XML_VALUE)
  public void generateRegisterFile(@PathVariable("swiss") int swiss, @PathVariable("swissAbroad") int swissAbroad,
                                   @PathVariable("foreigner") int foreigner, HttpServletResponse response)
      throws JAXBException, IOException, XMLStreamException, ClassNotFoundException, NoSuchMethodException,
      IllegalAccessException, InvocationTargetException, InstantiationException {

    response.setHeader("Content-Disposition", String
        .format("attachment; filename=register-eCH0045-%d-swiss-%d-swissAbroad-%d-foreigner.xml", swiss, swissAbroad,
                foreigner));
    response.setHeader("access-control-expose-headers", "content-disposition");
    response.setContentType("text/xml;charset=utf-8");

    Class indentingXMLWriterClass = Class.forName("com.sun.xml.internal.txw2.output.IndentingXMLStreamWriter");
    XMLStreamWriter xmlWriter = (XMLStreamWriter) indentingXMLWriterClass
        .getConstructor(XMLStreamWriter.class)
        .newInstance(XMLOutputFactory.newFactory().createXMLStreamWriter(response.getOutputStream()));

    xmlWriter.writeStartDocument("UTF-8", "1.0");
    xmlWriter.writeStartElement("voterDelivery");

    xmlWriter.writeDefaultNamespace("http://www.ech.ch/xmlns/eCH-0045/4");
    xmlWriter.writeNamespace("ech-0045", "http://www.ech.ch/xmlns/eCH-0045/4");
    xmlWriter.writeNamespace("ech-0008", "http://www.ech.ch/xmlns/eCH-0008/3");

    // delivery header
    QName deliveryHeader = new QName("http://www.ech.ch/xmlns/eCH-0045/4", "deliveryHeader", "ech-0045");

    JAXBContext context = JAXBContext.newInstance(HeaderType.class);
    Marshaller m = context.createMarshaller();
    m.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

    m.marshal(new JAXBElement<>(deliveryHeader, HeaderType.class, deliveryHeader()), xmlWriter);

    xmlWriter.writeStartElement("voterList");

    // reportingAuthority
    QName reportingAuthority = new QName("http://www.ech.ch/xmlns/eCH-0045/4", "reportingAuthority");
    context = JAXBContext.newInstance(AuthorityType.class);
    m = context.createMarshaller();
    m.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
    m.marshal(new JAXBElement<>(reportingAuthority, AuthorityType.class, autority()), xmlWriter);

    // number of voters
    xmlWriter.writeStartElement("numberOfVoters");
    xmlWriter.writeCharacters(String.valueOf(swiss + swissAbroad + foreigner));
    xmlWriter.writeEndElement(); // numberOfVoters

    // voters
    context = JAXBContext.newInstance(VotingPersonType.class, CountingCircleType.class, ExtensionType.class);
    m = context.createMarshaller();
    m.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
    QName voter = new QName("http://www.ech.ch/xmlns/eCH-0045/4", "voter");
    for (int i = 0; i < swiss; i++) {
      m.marshal(new JAXBElement<>(voter, VotingPersonType.class, votingPersonType(i, VoterType.SWISS)), xmlWriter);
    }
    for (int i = 0; i < swissAbroad; i++) {
      m.marshal(new JAXBElement<>(voter, VotingPersonType.class, votingPersonType(i, VoterType.SWISS_ABROAD)),
                xmlWriter);
    }
    for (int i = 0; i < foreigner; i++) {
      m.marshal(new JAXBElement<>(voter, VotingPersonType.class, votingPersonType(i, VoterType.FOREIGNER)), xmlWriter);
    }

    xmlWriter.writeEndElement(); // voterList

    xmlWriter.writeEndElement(); // voterDelivery
    xmlWriter.close();
  }

  private AuthorityType autority() {
    AuthorityType authority = new AuthorityType();
    CantonalRegisterType cantonalRegister = new CantonalRegisterType();
    cantonalRegister.setCantonAbbreviation(CantonAbbreviationType.GE);
    cantonalRegister.setRegisterIdentification("id");
    cantonalRegister.setRegisterName("register name");
    authority.setCantonalRegister(cantonalRegister);
    return authority;
  }

  private HeaderType deliveryHeader() {
    HeaderType header = new HeaderType();
    header.setSenderId("test");
    header.setMessageId("messageID");
    header.setMessageType("Registry");
    header.setAction("1");

    header.setMessageDate(LocalDateTime.now());

    SendingApplicationType application = new SendingApplicationType();
    application.setManufacturer("manufacturer");
    application.setProduct("product");
    application.setProductVersion("1");
    header.setSendingApplication(application);
    return header;
  }

  private VotingPersonType votingPersonType(int i, VoterType voterType) throws IOException {
    VotingPersonType votingPersonType = new VotingPersonType();
    votingPersonType.setDataLock(DATA_LOCK);

    int sex = ThreadLocalRandom.current().nextInt(1, 3);
    String firstName = RANDOMIZER.randomFirstname();
    String lastName = RANDOMIZER.randomLastname();
    String country = VoterType.SWISS_ABROAD.equals(voterType) ? RANDOMIZER.randomCountry() : SWITZERLAND;
    String[] countingCircle = RANDOMIZER.randomSwissTown().split(",");

    String[] canton = new String[]{"GE", "GE", "Geneva"};
    String[] town = new String[]{"6621", "GE"};

    final PersonMailAddressType address = personMailAddressType(sex, firstName, lastName, town[1], country);
    votingPersonType.setDeliveryAddress(address);
    votingPersonType.setElectoralAddress(address);
    votingPersonType.setEmail(email(firstName, lastName));
    votingPersonType.setPerson(person(i, voterType, sex, firstName, lastName, country, town));
    votingPersonType.setIsEvoter(true);
    if (VoterType.SWISS == voterType || VoterType.SWISS_ABROAD == voterType) {
      votingPersonType.getDomainOfInfluenceInfo().add(getDomainOfInfluenceInfoFederal(countingCircle));
      votingPersonType.getDomainOfInfluenceInfo().add(getDomainOfInfluenceInfoCantonal(countingCircle, canton));
    }
    if (VoterType.SWISS == voterType || VoterType.FOREIGNER == voterType) {
      votingPersonType.getDomainOfInfluenceInfo().add(getDomainOfInfluenceInfoTown(countingCircle, town));
    }
    return votingPersonType;
  }

  private VotingPersonType.DomainOfInfluenceInfo getDomainOfInfluenceInfoTown(String[] countingCircle, String[] town) {
    return getDomainOfInfluenceInfo(domainOfInfluenceMuninicipality(town), countingCircle);
  }

  private VotingPersonType.DomainOfInfluenceInfo getDomainOfInfluenceInfoCantonal(String[] countingCircle, String[]
      canton) {
    return getDomainOfInfluenceInfo(domainOfInfluenceCantonal(canton), countingCircle);
  }

  private VotingPersonType.DomainOfInfluenceInfo getDomainOfInfluenceInfoFederal(String[] countingCircle) {
    return getDomainOfInfluenceInfo(domainOfInfluenceFederal(), countingCircle);
  }

  private VotingPersonType.DomainOfInfluenceInfo getDomainOfInfluenceInfo(DomainOfInfluenceType doi, String[]
      countingCircle) {
    VotingPersonType.DomainOfInfluenceInfo domainOfInfluenceInfoFederal = new VotingPersonType.DomainOfInfluenceInfo();
    domainOfInfluenceInfoFederal.setDomainOfInfluence(doi);
    CountingCircleType countingCircleType = new CountingCircleType();
    countingCircleType.setCountingCircleId(countingCircle[0]);
    countingCircleType.setCountingCircleName(countingCircle[1]);
    domainOfInfluenceInfoFederal.setCountingCircle(countingCircleType);
    return domainOfInfluenceInfoFederal;
  }

  private DomainOfInfluenceType domainOfInfluenceFederal() {
    DomainOfInfluenceType domainOfInfluenceType = new DomainOfInfluenceType();
    domainOfInfluenceType.setDomainOfInfluenceType(DomainOfInfluenceTypeType.CH);
    domainOfInfluenceType.setLocalDomainOfInfluenceIdentification("CH");
    domainOfInfluenceType.setDomainOfInfluenceName("Confédération");
    domainOfInfluenceType.setDomainOfInfluenceShortname("CH");
    return domainOfInfluenceType;
  }

  private DomainOfInfluenceType domainOfInfluenceCantonal(String[] canton) {
    DomainOfInfluenceType domainOfInfluenceType = new DomainOfInfluenceType();
    domainOfInfluenceType.setDomainOfInfluenceType(DomainOfInfluenceTypeType.CT);
    domainOfInfluenceType.setLocalDomainOfInfluenceIdentification(canton[0]);
    domainOfInfluenceType.setDomainOfInfluenceName(String.format("Canton de %s", canton[2]));
    domainOfInfluenceType.setDomainOfInfluenceShortname(canton[1]);
    return domainOfInfluenceType;
  }

  private DomainOfInfluenceType domainOfInfluenceMuninicipality(String[] town) {
    DomainOfInfluenceType domainOfInfluenceType = new DomainOfInfluenceType();
    domainOfInfluenceType.setDomainOfInfluenceType(DomainOfInfluenceTypeType.MU);
    domainOfInfluenceType.setLocalDomainOfInfluenceIdentification(town[0]);
    domainOfInfluenceType.setDomainOfInfluenceName(town[1]);
    domainOfInfluenceType.setDomainOfInfluenceShortname(town[1].length() <= 5 ? town[1] : town[1].substring(0, 6));
    return domainOfInfluenceType;
  }

  private EmailType email(String firstName, String lastName) {
    EmailType emailType = new EmailType();
    emailType.setEmailAddress(String.format("%s.%s@test.com", firstName.toLowerCase(), lastName.toLowerCase()));
    return emailType;
  }

  private PersonMailAddressType personMailAddressType(int sex, String firstName, String lastName, String town,
                                                      String country) throws IOException {
    PersonMailAddressType personMailAddressType = new PersonMailAddressType();
    personMailAddressType.setAddressInformation(addressInformation(town, country));
    PersonMailAddressInfoType addressInfoType = new PersonMailAddressInfoType();
    addressInfoType.setFirstName(firstName);
    addressInfoType.setLastName(lastName);
    addressInfoType.setMrMrs(String.valueOf(sex));
    addressInfoType.setTitle(sex == 1 ? "Mr" : "Mme");
    personMailAddressType.setPerson(addressInfoType);
    return personMailAddressType;
  }

  private AddressInformationType addressInformation(String town, String country) throws IOException {
    AddressInformationType addressInformation = new AddressInformationType();
    addressInformation.setAddressLine1(RANDOMIZER.randomAddressLine());
    addressInformation.setAddressLine2(RANDOMIZER.randomAddressLine());
    ch.ge.ve.interfaces.ech.eCH0010.v6.CountryType countryType = new ch.ge.ve.interfaces.ech.eCH0010.v6.CountryType();
    countryType.setCountryNameShort(country.substring(0, 2).toUpperCase());
    countryType.setCountryIdISO2(countryType.getCountryNameShort());
    addressInformation.setCountry(countryType);
    addressInformation.setSwissZipCode(RANDOMIZER.randomZip());
    addressInformation.setLocality(town);
    addressInformation.setTown(town);
    addressInformation.setStreet(RANDOMIZER.randomStreet());
    return addressInformation;
  }

  private VotingPersonType.Person person(int i, VoterType voterType, int sex, String firstName, String lastName,
                                         String country, String[] town) {
    VotingPersonType.Person person = new VotingPersonType.Person();
    switch (voterType) {
      case SWISS:
        person.setSwiss(swiss(i, sex, firstName, lastName, town));
        break;
      case SWISS_ABROAD:
        person.setSwissAbroad(swissAbroad(i, sex, firstName, lastName, country, town));
        break;
      case FOREIGNER:
        person.setForeigner(foreigner(i, sex, firstName, lastName, town));
        break;
    }
    return person;
  }

  private SwissDomesticType swiss(int i, int sex, String firstName, String lastName, String[] town) {
    SwissDomesticType swissDomesticType = new SwissDomesticType();
    swissDomesticType.setSwissDomesticPerson(swissPersonType(i, sex, firstName, lastName));
    SwissMunicipalityType swissMunicipalityType = new SwissMunicipalityType();
    swissMunicipalityType.setMunicipalityName(town[1]);
    swissMunicipalityType.setMunicipalityId(Integer.valueOf(town[0]));
    swissDomesticType.setMunicipality(swissMunicipalityType);
    return swissDomesticType;
  }

  private SwissAbroadType swissAbroad(int i, int sex, String firstName, String lastName, String country,
                                      String[] town) {
    SwissAbroadType swissAbroadType = new SwissAbroadType();
    swissAbroadType.setSwissAbroadPerson(swissPersonType(i, sex, firstName, lastName));
    swissAbroadType.setDateOfRegistration(RANDOMIZER.randomDateOfBirth());

    // country
    CountryType countryType = new CountryType();
    countryType.setCountryNameShort(country);
    swissAbroadType.setResidenceCountry(countryType);


    // municipality or canton
    if (ThreadLocalRandom.current().nextBoolean()) {
      SwissMunicipalityType swissMunicipalityType = new SwissMunicipalityType();
      swissMunicipalityType.setMunicipalityName(town[1]);
      swissMunicipalityType.setMunicipalityId(Integer.valueOf(town[0]));
      swissAbroadType.setMunicipality(swissMunicipalityType);
    } else {
      swissAbroadType.setCanton(CantonAbbreviationType.GE);
    }
    return swissAbroadType;
  }

  private ForeignerType foreigner(int i, int sex, String firstName, String lastName, String[] town) {
    ForeignerType foreignerType = new ForeignerType();
    foreignerType.setForeignerPerson(foreignerPersonType(i, sex, firstName, lastName));
    SwissMunicipalityType swissMunicipalityType = new SwissMunicipalityType();
    swissMunicipalityType.setMunicipalityName(town[1]);
    swissMunicipalityType.setMunicipalityId(Integer.valueOf(town[0]));
    foreignerType.setMunicipality(swissMunicipalityType);
    return foreignerType;
  }

  private ForeignerPersonType foreignerPersonType(int i, int sex, String firstName, String lastName) {
    ForeignerPersonType foreignerPersonType = new ForeignerPersonType();

    PersonTypeExtensionType personTypeExtension = new PersonTypeExtensionType();
    personTypeExtension.setPostageCode(1 + i % 3);
    OrganisationMailAddressType votingPlace = new OrganisationMailAddressType();
    OrganisationMailAddressInfoType organisationForVotingPlace = new OrganisationMailAddressInfoType();
    organisationForVotingPlace.setOrganisationName("ASE");
    organisationForVotingPlace.setOrganisationNameAddOn1("votingPlace Code to display on voting card");
    votingPlace.setOrganisation(organisationForVotingPlace);
    votingPlace.setAddressInformation(addressInformationType("avenue du grand pré 55",1100));

    OrganisationMailAddressType votingCardReturnAddress = new OrganisationMailAddressType();
    votingCardReturnAddress.setAddressInformation(addressInformationType("Route des acacias 25",1211));
    OrganisationMailAddressInfoType organisationForVotingPlace1 = new OrganisationMailAddressInfoType();
    organisationForVotingPlace1.setOrganisationName("SVE");
    votingCardReturnAddress.setOrganisation(organisationForVotingPlace1);

    personTypeExtension.setVotingCardReturnAddress(votingCardReturnAddress);
    personTypeExtension.setVotingPlace(votingPlace);
    foreignerPersonType.setExtension(personTypeExtension);

    foreignerPersonType.setPersonIdentification(personIdentification(i, sex, firstName, lastName));
    foreignerPersonType.setCallName(lastName);
    foreignerPersonType.setLanguageOfCorrespondance(LanguageType.FR);
    ResidencePermitDataType permit = new ResidencePermitDataType();
    permit.setResidencePermit("01");
    foreignerPersonType.setResidencePermit(permit);
    ForeignerNameType foreignerNameType = new ForeignerNameType();
    foreignerNameType.setFirstName(firstName);
    foreignerNameType.setName(lastName);
    foreignerPersonType.setNameOnForeignPassport(foreignerNameType);
    ResidencePermitDataType residencePermitDataType = new ResidencePermitDataType();
    residencePermitDataType.setResidencePermit("01");
    foreignerPersonType.setResidencePermit(residencePermitDataType);
    return foreignerPersonType;
  }


  private SwissPersonType swissPersonType(int i, int sex, String firstName, String lastName) {
    SwissPersonType swissPersonType = new SwissPersonType();
    swissPersonType.setAllianceName(ALLIANCE_NAME);
    swissPersonType.setLanguageOfCorrespondance(LanguageType.FR);
    swissPersonType.setPersonIdentification(personIdentification(i, sex, firstName, lastName));


    PersonTypeExtensionType personTypeExtension = new PersonTypeExtensionType();
    personTypeExtension.setPostageCode(1 + i % 3);
    OrganisationMailAddressType votingPlace = new OrganisationMailAddressType();
    OrganisationMailAddressInfoType organisationForVotingPlace = new OrganisationMailAddressInfoType();
    organisationForVotingPlace.setOrganisationName("ASE");
    organisationForVotingPlace.setOrganisationNameAddOn1("votingPlace Code to display on voting card");
    votingPlace.setOrganisation(organisationForVotingPlace);
    votingPlace.setAddressInformation(addressInformationType("avenue du grand pré 55",1100));

    OrganisationMailAddressType votingCardReturnAddress = new OrganisationMailAddressType();
    votingCardReturnAddress.setAddressInformation(addressInformationType("Route des acacias 25",1211));
    OrganisationMailAddressInfoType organisationForVotingPlace1 = new OrganisationMailAddressInfoType();
    organisationForVotingPlace1.setOrganisationName("SVE");
    votingCardReturnAddress.setOrganisation(organisationForVotingPlace1);

    personTypeExtension.setVotingCardReturnAddress(votingCardReturnAddress);
    personTypeExtension.setVotingPlace(votingPlace);
    swissPersonType.setExtension(personTypeExtension);

    PlaceOfOriginType placeOfOrigin = new PlaceOfOriginType();
    placeOfOrigin.setPlaceOfOriginId(BigInteger.valueOf(6621));
    placeOfOrigin.setCanton(ch.ge.ve.interfaces.ech.eCH0007.v5.CantonAbbreviationType.GE);
    placeOfOrigin.setOriginName("geneva");
    swissPersonType.getPlaceOfOrigin().add(placeOfOrigin);


    return swissPersonType;
  }

  @SuppressWarnings("Duplicates")
  private AddressInformationType addressInformationType(String addressLine1, long swissZipCode) {
    AddressInformationType addressInformation = new AddressInformationType();
    ch.ge.ve.interfaces.ech.eCH0010.v6.CountryType country1 = new ch.ge.ve.interfaces.ech.eCH0010.v6.CountryType();
    country1.setCountryNameShort("Switzerland");
    addressInformation.setCountry(country1);
    addressInformation.setTown("Genève");
    addressInformation.setAddressLine1(addressLine1);
    addressInformation.setSwissZipCode(swissZipCode);

    return addressInformation;
  }
  private PersonIdentificationType personIdentification(int i, int sex, String firstName, String lastName) {
    PersonIdentificationType personIdentification = new PersonIdentificationType();
    NamedPersonIdType namedPersonId = new NamedPersonIdType();
    namedPersonId.setPersonId(RandomStringUtils.randomAlphanumeric(10));
    namedPersonId.setPersonIdCategory("id");
    personIdentification.setLocalPersonId(namedPersonId);
    personIdentification.setDateOfBirth(datePartiallyKnown());
    personIdentification.setFirstName(firstName);
    personIdentification.setOfficialName(lastName);
    personIdentification.setSex(String.valueOf(sex));
    return personIdentification;
  }

  private DatePartiallyKnownType datePartiallyKnown() {
    DatePartiallyKnownType datePartiallyKnown = new DatePartiallyKnownType();
    datePartiallyKnown.setYearMonthDay(RANDOMIZER.randomDateOfBirth());
    return datePartiallyKnown;
  }


}
