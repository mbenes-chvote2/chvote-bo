/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.mock.server.workflow;

import io.restassured.response.ValidatableResponse;
import org.springframework.stereotype.Service;

@Service
public class Pact {
  private static final String CONFIGURATION_DEPLOYMENT     = "/privileged-actions/configuration-deployment";
  private static final String VOTING_MATERIAL_CREATION     = "/privileged-actions/voting-materials-creation";
  private static final String VOTING_PERIOD_INITIALIZATION = "/privileged-actions/voting-period-initialization";

  private       PactUser   user = PactUser.REQUESTER;
  private final Connection connection;

  public Pact(Connection connection) {
    this.connection = connection;
  }

  public void switchToUser(PactUser user) {
    this.user = user;
  }


  public ValidatableResponse requestConfigurationDeployment(Long pactOperationId) {
    return request(pactOperationId, CONFIGURATION_DEPLOYMENT);
  }

  public ValidatableResponse requestVotingMaterialCreation(Long pactId) {
    return request(pactId, VOTING_MATERIAL_CREATION);
  }

  public ValidatableResponse requestVotingPeriodInitialization(long businessId) {
    return request(businessId, VOTING_PERIOD_INITIALIZATION);
  }


  private ValidatableResponse request(Long pactId, String path) {
    return connection.pact(user)
                     .param("businessId", pactId)
                     .put(path)
                     .then()
                     .statusCode(200);
  }


  public ValidatableResponse refuseConfigurationDeployment(Long deploymentId, String reason) {
    return refuse(deploymentId, reason, CONFIGURATION_DEPLOYMENT);
  }

  public ValidatableResponse refuseVotingMaterialDeployment(Long deploymentId, String reason) {
    return refuse(deploymentId, reason, VOTING_MATERIAL_CREATION);
  }

  private ValidatableResponse refuse(Long deploymentId, String reason, String path) {
    return connection.pact(user)
                     .param("status", "REJECTED")
                     .param("rejectionReason", reason)
                     .put(path + "/" + deploymentId)
                     .then()
                     .statusCode(200);
  }


  public long getConfigurationDeploymentIdByBusinessKey(long key) {
    return getRequestIdByBusinessKey(key, CONFIGURATION_DEPLOYMENT);
  }

  public long getVotingMaterialIdByBusinessKey(long key) {
    return getRequestIdByBusinessKey(key, VOTING_MATERIAL_CREATION);
  }


  public long getVotingPeriodIdByBusinessKey(long key) {
    return getRequestIdByBusinessKey(key, VOTING_PERIOD_INITIALIZATION);
  }

  private long getRequestIdByBusinessKey(long businessKey, String path) {
    return connection.pact(user)
                     .param("businessId", businessKey)
                     .get(path)
                     .then()
                     .statusCode(200)
                     .extract().body().jsonPath().getLong("id");
  }

  public ValidatableResponse validateConfigurationDeployment(Long deploymentId) {
    return validate(deploymentId, CONFIGURATION_DEPLOYMENT);
  }

  public ValidatableResponse validateVotingMaterialCreation(Long deploymentId) {
    return validate(deploymentId, VOTING_MATERIAL_CREATION);
  }


  private ValidatableResponse validate(Long deploymentId, String path) {
    return connection.pact(user)
                     .param("status", "APPROVED")
                     .put(path + "/" + deploymentId)
                     .then()
                     .statusCode(200);
  }

  public ValidatableResponse validateVotingPeriodInitialization(long deploymentId) {
    return validate(deploymentId, VOTING_PERIOD_INITIALIZATION);
  }
}
