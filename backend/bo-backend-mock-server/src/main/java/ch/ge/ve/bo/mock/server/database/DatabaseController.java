/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.mock.server.database;

import ch.ge.ve.bo.mock.server.services.PactSimulator;
import ch.ge.ve.bo.mock.server.workflow.ManagementEntity;
import ch.ge.ve.bo.repository.entity.DeploymentTarget;
import ch.ge.ve.bo.repository.entity.Operation;
import ch.ge.ve.bo.service.exception.BusinessException;
import ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialsStatusVo;
import com.google.common.collect.ImmutableSet;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Set;
import java.util.stream.Stream;
import org.awaitility.core.ThrowingRunnable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DatabaseController {
  private static final Logger logger = LoggerFactory.getLogger(DatabaseController.class);

  private final PactSimulator   pactSimulator;
  private final DatabaseService databaseService;


  public DatabaseController(PactSimulator pactSimulator,
                            DatabaseService databaseService) {
    this.pactSimulator = pactSimulator;
    this.databaseService = databaseService;
  }

  /**
   * Reset database to create a new test
   */
  @RequestMapping(path = "database/reset", method = RequestMethod.GET)
  public void reset() {
    databaseService.reset();
    logger.info("The database has been cleaned");
  }

  @RequestMapping(path = "database/add-electoral-authority-keys", method = RequestMethod.GET)
  public void addElectoralAuthorityKeys() throws IOException {
    databaseService.addElectoralAuthorityKeys();
    logger.info("Electoral authority keys has been created");
  }


  @RequestMapping(path = "database/operation/{id}/add-voting-material", method = RequestMethod.GET)
  public void addVotingMaterial(@PathVariable("id") long operationId) throws BusinessException {
    databaseService.addVotingMaterial(operationId);
    logger.info("voting material has been added");
  }


  @RequestMapping(path = "database/operation/{id}/add-guest-management-entity", method = RequestMethod.GET)
  public void addGuestManagementEntity(@PathVariable("id") long operationId,
                                       @RequestParam("managementEntity") String guestManagementEntity) {
    databaseService.addGuestManagementEntity(operationId, guestManagementEntity);
    logger.info("Management entity " + guestManagementEntity + " has been invited");
  }

  @RequestMapping(path = "database/operation/{id}/delete-guest-management-entity", method = RequestMethod.GET)
  public void deleteGuestManagementEntity(@PathVariable("id") long operationId,
                                          @RequestParam("managementEntity") String guestManagementEntity) {
    databaseService.deleteGuestManagementEntity(operationId, guestManagementEntity);
    logger.info("Management entity " + guestManagementEntity + " has been invited");
  }

  /**
   * create a new operation
   */
  @RequestMapping(path = "database/operation/create/{shortName}/{longName}",
                  method = RequestMethod.GET)
  public long createOperation(@PathVariable("shortName") String shortName,
                              @PathVariable("longName") String longName,
                              @RequestParam(value = "managementEntity",
                                            defaultValue = "GE_CANTON_DE_GENEVE") ManagementEntity managementEntity,
                              @RequestParam(value = "option", required = false,
                                            defaultValue = "ADD_ALL") OperationOption[] options) {

    Operation operation = databaseService.createOperation(shortName, longName, managementEntity);
    logger.info("Operation " + operation.getId() + " has been created");
    updateOperation(operation.getId(),
                    managementEntity.getManagementEntity(),
                    managementEntity.getDefaultUser().name(),
                    options);

    return operation.getId();
  }

  /**
   * update an existing operation
   */
  @RequestMapping(path = "database/operation/update/{operationId}",
                  method = RequestMethod.GET)
  public void updateOperation(@PathVariable("operationId") long operationId,
                              @RequestParam(value = "managementEntity",
                                            defaultValue = "GE_CANTON_DE_GENEVE") ManagementEntity managementEntity,
                              @RequestParam(value = "option", required = false) OperationOption[] options) {

    updateOperation(operationId,
                    managementEntity.getManagementEntity(),
                    managementEntity.getDefaultUser().name(), options);
  }

  private void updateOperation(long operationId,
                               String managementEntity,
                               String canton, OperationOption[] operationOptions) {


    onOption(operationOptions, OperationOption.ADD_MILESTONE, OperationOption.ADD_ALL)
        .then(() -> databaseService.addMilestones(operationId));

    onOption(operationOptions, OperationOption.ADD_DOMAIN_INFLUENCE, OperationOption.ADD_ALL)
        .then(() -> databaseService.addDomainOfInfluence(operationId, managementEntity));


    onOption(operationOptions, OperationOption.ADD_REPOSITORY_VOTATION, OperationOption.ADD_ALL)
        .then(() -> databaseService.addRepositoryVotation(operationId, managementEntity));

    onOption(operationOptions, OperationOption.ADD_REPOSITORY_VOTATION_INCPQS, OperationOption.ADD_ALL)
        .then(() -> databaseService.addRepositoryVotationIncpqs(operationId, managementEntity));


    onOption(operationOptions, OperationOption.ADD_REPOSITORY_ELECTION, OperationOption.ADD_ALL)
        .then(() -> databaseService.addRepositoryElection(operationId, managementEntity));


    onOption(operationOptions, OperationOption.ADD_DOCUMENT, OperationOption.ADD_ALL)
        .then(() -> databaseService.addDocuments(operationId, managementEntity));

    onOption(operationOptions, OperationOption.ADD_TESTING_CARDS, OperationOption.ADD_ALL)
        .then(() -> databaseService.addTestingCards(operationId));

    onOption(operationOptions, OperationOption.ADD_ELECTION_PAGE_PROPERTIES, OperationOption.ADD_ALL)
        .then(() -> databaseService.addElectionPageProperties(operationId));


    onOption(operationOptions, OperationOption.ADD_REGISTER_SE, OperationOption.ADD_ALL)
        .then(() -> databaseService.validateRegister(
            operationId, databaseService.addRegister(operationId, canton, "register-se.xml")));


    onOption(operationOptions, OperationOption.ADD_REGISTER_SR, OperationOption.ADD_ALL)
        .then(() -> databaseService.validateRegister(
            operationId, databaseService.addRegister(operationId, canton, "register-sr.xml")));


    onOption(operationOptions, OperationOption.ADD_CARD_TITLE, OperationOption.ADD_ALL)
        .then(() ->
                  onOption(operationOptions, OperationOption.DATE_ON_CARD_TITLE)
                      .then(
                          () -> databaseService.addVotingCardTitle(
                              operationId, "Auto Ope " + LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME))
                      ).orElse(() -> databaseService.addVotingCardTitle(operationId, "Test label")));


    onOption(operationOptions, OperationOption.ADD_PRINTER_TEMPLATE, OperationOption.ADD_ALL)
        .then(() -> databaseService.addPrinterTemplace(operationId, canton));

    onOption(operationOptions, OperationOption.ADD_SIMULATION_SITE_PERIOD, OperationOption.ADD_ALL)
        .then(() -> databaseService.addSimulationSitePeriod(operationId));

    onOption(operationOptions, OperationOption.ADD_ELECTORAL_AUTHORITY_KEYS, OperationOption.ADD_ALL)
        .then(() -> {
          databaseService.addElectoralAuthorityKeys();
          databaseService.addElectoralAuthorityKeyConfig(operationId);
        });

    onOption(operationOptions, OperationOption.ADD_VOTING_SITE_CONFIGURATION, OperationOption.ADD_ALL)
        .then(() -> databaseService.addVotingSiteConfiguration(operationId));


    // workflow
    onOption(operationOptions, OperationOption.FOR_REAL)
        .then(() -> databaseService.addDeploymentTarget(operationId, DeploymentTarget.REAL));

    onOption(operationOptions, OperationOption.FOR_SIMULATION)
        .then(() -> databaseService.addDeploymentTarget(operationId, DeploymentTarget.SIMULATION));

    onOption(operationOptions, OperationOption.FOR_SIMULATION, OperationOption.FOR_REAL)
        .then(() -> pactSimulator.deployConfiguration(operationId));

    onOption(operationOptions, OperationOption.WITH_VOTING_MATERIAL_FINALIZED)
        .then(() -> pactSimulator.setVotingMaterialStatus(operationId, VotingMaterialsStatusVo.State.VALIDATED));


    // remove
    onOption(operationOptions, OperationOption.REMOVE_BALLOT_DOCUMENTATION)
        .then(() -> databaseService.removeBallotDocumentation(operationId));

    onOption(operationOptions, OperationOption.REMOVE_ELECTION_PAGE_PROPERTIES)
        .then(databaseService::removeElectionPageProperties);

    onOption(operationOptions, OperationOption.REMOVE_REPOSITORY_ELECTION)
        .then(() -> databaseService.removeRepositoryElection(operationId));

    databaseService.ensureConsistencyCheck(operationId);

  }

  private PossiblyExecute onOption(OperationOption[] operationOptions, OperationOption... expectedOptions) {
    Set<OperationOption> options = ImmutableSet.copyOf(operationOptions);
    return new PossiblyExecute(Stream.of(expectedOptions).anyMatch(options::contains));
  }


  private class PossiblyExecute {
    private final boolean doExecute;

    private PossiblyExecute(boolean doExecute) {
      this.doExecute = doExecute;
    }

    PossiblyExecute then(ThrowingRunnable runnable) {
      if (doExecute) {
        try {
          runnable.run();
        } catch (Throwable throwable) {
          throw new RuntimeException(throwable);
        }
      }
      return this;
    }

    void orElse(ThrowingRunnable runnable) {
      if (!doExecute) {
        try {
          runnable.run();
        } catch (Throwable throwable) {
          throw new RuntimeException(throwable);
        }
      }

    }

  }
}
