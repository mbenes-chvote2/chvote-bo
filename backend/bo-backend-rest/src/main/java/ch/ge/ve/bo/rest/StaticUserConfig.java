/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.rest;

import static ch.ge.ve.bo.repository.ConnectedUser.BELONGS_TO_MANAGEMENT_ENTITY;
import static ch.ge.ve.bo.repository.ConnectedUser.BELONGS_TO_REALM;
import static ch.ge.ve.bo.repository.ConnectedUser.REALM_PRINTER;

import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.provisioning.InMemoryUserDetailsManagerConfigurer;
import org.springframework.security.config.annotation.authentication.configurers.provisioning.UserDetailsManagerConfigurer;

/**
 * Static configuration to create some user with there rights
 * TODO should be replaced by an iam solution
 */
@Configuration
public class StaticUserConfig {

  private static final String DEFAULT_PASS = "{noop}pass";

  private static final String CANTON_DE_GENEVE = "Canton de Genève";
  private static final String GY               = "Gy";

  private static final String USER_ROLE                                    = "ROLE_USER";
  private static final String PRINTER_ROLE                                 = "ROLE_PRINTER";
  private static final String CREATE_OPERATION                             = "ROLE_CREATE_OPERATION";
  private static final String CREATE_TEST_VOTING_CARD_DEFINITION           = "ROLE_CREATE_TEST_VOTING_CARD_DEFINITION";
  private static final String DELETE_DOI                                   = "ROLE_DELETE_DOI";
  private static final String DELETE_OPERATION_DOCUMENTATION               = "ROLE_DELETE_OPERATION_DOCUMENTATION";
  private static final String DELETE_REGISTER_FILE                         = "ROLE_DELETE_REGISTER_FILE";
  private static final String DELETE_REPOSITORY                            = "ROLE_DELETE_REPOSITORY";
  private static final String DELETE_TEST_VOTING_CARD_DEFINITION           = "ROLE_DELETE_TEST_VOTING_CARD_DEFINITION";
  private static final String DEPLOY_TEST_SITE                             = "ROLE_DEPLOY_TEST_SITE";
  private static final String DEPLOY_VOTING_MATERIAL                       = "ROLE_DEPLOY_VOTING_MATERIAL";
  private static final String EDIT_TEST_VOTING_CARD_DEFINITION             = "ROLE_EDIT_TEST_VOTING_CARD_DEFINITION";
  private static final String GET_REGISTER_REPORT                          = "ROLE_GET_REGISTER_REPORT";
  private static final String INVALIDATE_TEST_SITE                         = "ROLE_INVALIDATE_TEST_SITE";
  private static final String DEPLOYMENT_TARGET_SIMULATION                 = "ROLE_DEPLOYMENT_TARGET_SIMULATION";
  private static final String DEPLOYMENT_TARGET_REAL                       = "ROLE_DEPLOYMENT_TARGET_REAL";
  private static final String SELECT_PRINTER_TEMPLATE                      = "ROLE_SELECT_PRINTER_TEMPLATE";
  private static final String SET_OPERATION_DOCUMENTATION                  = "ROLE_SET_OPERATION_DOCUMENTATION";
  private static final String UPDATE_BASE_PARAMETER                        = "ROLE_UPDATE_BASE_PARAMETER";
  private static final String UPDATE_MILESTONE                             = "ROLE_UPDATE_MILESTONE";
  private static final String UPDATE_VOTING_CARD_TITLE                     = "ROLE_UPDATE_VOTING_CARD_TITLE";
  private static final String UPLOAD_DOI                                   = "ROLE_UPLOAD_DOI";
  private static final String UPLOAD_REGISTER_FILE                         = "ROLE_UPLOAD_REGISTER_FILE";
  private static final String UPLOAD_REPOSITORY                            = "ROLE_UPLOAD_REPOSITORY";
  private static final String VALIDATE_TEST_SITE                           = "ROLE_VALIDATE_TEST_SITE";
  private static final String INVITE_MANAGEMENT_ENTITY                     = "ROLE_INVITE_MANAGEMENT_ENTITY";
  private static final String REVOKE_MANAGEMENT_ENTITY                     = "ROLE_REVOKE_MANAGEMENT_ENTITY";
  private static final String DOWNLOAD_GENERATED_CARDS_FOR_TEST_SITE       =
      "ROLE_DOWNLOAD_GENERATED_CARDS_FOR_TEST_SITE";
  private static final String ADD_HIGHLIGHTED_QUESTION                     = "ROLE_ADD_HIGHLIGHTED_QUESTION";
  private static final String ADD_LOCALIZED_HIGHLIGHTED_QUESTION           = "ROLE_ADD_LOCALIZED_HIGHLIGHTED_QUESTION";
  private static final String DELETE_LOCALIZED_HIGHLIGHTED_QUESTION        =
      "ROLE_DELETE_LOCALIZED_HIGHLIGHTED_QUESTION";
  private static final String DELETE_HIGHLIGHTED_QUESTION                  = "ROLE_DELETE_HIGHLIGHTED_QUESTION";
  private static final String ADD_BALLOT_DOCUMENTATION                     = "ROLE_ADD_BALLOT_DOCUMENTATION";
  private static final String DELETE_BALLOT_DOCUMENTATION                  = "ROLE_DELETE_BALLOT_DOCUMENTATION";
  private static final String CREATE_ELECTION_PAGE_PROPERTIES_MODEL        =
      "ROLE_CREATE_ELECTION_PAGE_PROPERTIES_MODEL";
  private static final String DELETE_ELECTION_PAGE_PROPERTIES_MODEL        =
      "ROLE_DELETE_ELECTION_PAGE_PROPERTIES_MODEL";
  private static final String MAP_ELECTION_PAGE_PROPERTIES_MODEL_TO_BALLOT =
      "ROLE_MAP_ELECTION_PAGE_PROPERTIES_MODEL_TO_BALLOT";
  private static final String DOWNLOAD_NOT_PRINTABLE_CARDS_FOR_PRODUCTION  =
      "ROLE_DOWNLOAD_NOT_PRINTABLE_CARDS_FOR_PRODUCTION";
  private static final String ADD_ELECTORAL_AUTHORITY_KEY                  = "ROLE_ADD_ELECTORAL_AUTHORITY_KEY";
  private static final String SELECT_ELECTORAL_AUTHORITY_KEY               = "ROLE_SELECT_ELECTORAL_AUTHORITY_KEY";
  private static final String SELECT_SIMULATION_PERIOD          = "ROLE_SELECT_SIMULATION_PERIOD";
  private static final String DEPLOY_VOTING_PERIOD              = "ROLE_DEPLOY_VOTING_PERIOD";
  private static final String VALIDATE_VOTING_MATERIAL          = "ROLE_VALIDATE_VOTING_MATERIAL";
  private static final String CLOSE_VOTING_PERIOD_IN_SIMULATION = "ROLE_CLOSE_VOTING_PERIOD_IN_SIMULATION";
  private static final String DOWNLOAD_TALLY_ARCHIVE            = "ROLE_DOWNLOAD_TALLY_ARCHIVE";
  private static final String MODIFY_CONFIGURATION              = "ROLE_MODIFY_CONFIGURATION";
  private static final String UPDATE_VOTING_SITE_CONFIG         = "ROLE_UPDATE_VOTING_SITE_CONFIG";
  private static final String GE                                = "GE";
  private static final String BE                                = "BE";
  private static final String SG                                = "SG";

  private static final String[] ALL_ROLES = {
      USER_ROLE, CREATE_OPERATION, CREATE_TEST_VOTING_CARD_DEFINITION, DELETE_DOI, DELETE_OPERATION_DOCUMENTATION,
      DELETE_REGISTER_FILE, DELETE_REPOSITORY, DELETE_TEST_VOTING_CARD_DEFINITION, DEPLOY_TEST_SITE,
      DEPLOY_VOTING_MATERIAL, EDIT_TEST_VOTING_CARD_DEFINITION, GET_REGISTER_REPORT, INVALIDATE_TEST_SITE,
      DEPLOYMENT_TARGET_SIMULATION, DEPLOYMENT_TARGET_REAL, SELECT_PRINTER_TEMPLATE, SET_OPERATION_DOCUMENTATION,
      UPDATE_BASE_PARAMETER, UPDATE_MILESTONE, UPDATE_VOTING_CARD_TITLE, UPLOAD_DOI, UPLOAD_REGISTER_FILE,
      UPLOAD_REPOSITORY, INVITE_MANAGEMENT_ENTITY, REVOKE_MANAGEMENT_ENTITY, VALIDATE_TEST_SITE,
      DOWNLOAD_GENERATED_CARDS_FOR_TEST_SITE, ADD_HIGHLIGHTED_QUESTION, ADD_LOCALIZED_HIGHLIGHTED_QUESTION,
      DELETE_LOCALIZED_HIGHLIGHTED_QUESTION, DELETE_HIGHLIGHTED_QUESTION, ADD_BALLOT_DOCUMENTATION,
      DELETE_BALLOT_DOCUMENTATION, CREATE_ELECTION_PAGE_PROPERTIES_MODEL, DELETE_ELECTION_PAGE_PROPERTIES_MODEL,
      MAP_ELECTION_PAGE_PROPERTIES_MODEL_TO_BALLOT, ADD_ELECTORAL_AUTHORITY_KEY, MODIFY_CONFIGURATION,
      DOWNLOAD_NOT_PRINTABLE_CARDS_FOR_PRODUCTION, SELECT_ELECTORAL_AUTHORITY_KEY, SELECT_SIMULATION_PERIOD,
      DEPLOY_VOTING_PERIOD, VALIDATE_VOTING_MATERIAL, CLOSE_VOTING_PERIOD_IN_SIMULATION, DOWNLOAD_TALLY_ARCHIVE,
      UPDATE_VOTING_SITE_CONFIG
  };

  /**
   * Create users
   */
  @Autowired
  public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
    InMemoryUserDetailsManagerConfigurer<AuthenticationManagerBuilder> userConfig = auth.inMemoryAuthentication();

    addUser(userConfig, "ge1").authorities(concat(realm(GE), mngEnt(CANTON_DE_GENEVE)));
    addUser(userConfig, "ge2").authorities(concat(realm(GE), mngEnt(CANTON_DE_GENEVE)));
    addUser(userConfig, "ge3").authorities(concat(realm(GE), mngEnt(GY)));
    addUser(userConfig, "ge4").authorities(realm(GE), mngEnt(CANTON_DE_GENEVE), USER_ROLE);

    addUser(userConfig, "be1").authorities(concat(realm(BE), mngEnt("Bern")));
    addUser(userConfig, "be2").authorities(concat(realm(BE), mngEnt("Biel")));
    addUser(userConfig, "be3").authorities(concat(realm(BE), mngEnt("Köniz")));

    addUser(userConfig, "sg1").authorities(concat(realm(SG), mngEnt("Saint-Gall")));

    addUser(userConfig, "prn1").authorities(USER_ROLE, PRINTER_ROLE, realm(REALM_PRINTER), mngEnt("Printer1"));
    addUser(userConfig, "prn2").authorities(USER_ROLE, PRINTER_ROLE, realm(REALM_PRINTER), mngEnt("Printer2"));
    addUser(userConfig, "prn3").authorities(USER_ROLE, PRINTER_ROLE, realm(REALM_PRINTER), mngEnt("Printer3"));
    addUser(userConfig, "prn4").authorities(USER_ROLE, PRINTER_ROLE, realm(REALM_PRINTER), mngEnt("Printer4"));
  }

  private String[] concat(String... others) {
    String[] toReturn = Arrays.copyOf(StaticUserConfig.ALL_ROLES, StaticUserConfig.ALL_ROLES.length + others.length);
    System.arraycopy(others, 0, toReturn, StaticUserConfig.ALL_ROLES.length, others.length);
    return toReturn;
  }

  private UserDetailsManagerConfigurer<AuthenticationManagerBuilder,
      InMemoryUserDetailsManagerConfigurer<AuthenticationManagerBuilder>>.UserDetailsBuilder addUser
      (InMemoryUserDetailsManagerConfigurer<AuthenticationManagerBuilder> userConfig, String userName) {
    return userConfig.withUser(userName).password(DEFAULT_PASS);
  }

  private String mngEnt(String managementEntity) {
    return BELONGS_TO_MANAGEMENT_ENTITY + managementEntity;
  }

  private String realm(String canton) {
    return BELONGS_TO_REALM + canton;
  }

}
