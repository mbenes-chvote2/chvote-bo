/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.rest.controller.operation.parameters;

import ch.ge.ve.bo.service.operation.model.VotingSiteConfigurationVo;
import ch.ge.ve.bo.service.operation.parameters.voting.site.configuration.VotingSiteConfigurationService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller used to handle voting site configuration linked to an operation.
 */
@RestController
@RequestMapping("/operation/{operationId}/voting-site-config")
public class VotingSiteConfigurationController {
  private final VotingSiteConfigurationService service;

  /**
   * Default constructor
   */
  public VotingSiteConfigurationController(VotingSiteConfigurationService service) {
    this.service = service;
  }

  /**
   * find voting site configuration for an operation
   */
  @GetMapping
  @PreAuthorize("hasRole('USER')")
  public VotingSiteConfigurationVo findByOperation(@PathVariable("operationId") Long operationId) {
    return service.findForOperation(operationId).orElse(null);
  }

  /**
   * save voting site configuration
   */
  @PostMapping
  @PreAuthorize("hasRole('UPDATE_VOTING_SITE_CONFIG')")
  public void saveOrUpdate(@RequestBody VotingSiteConfigurationVo configuration,
                           @PathVariable("operationId") Long operationId) {
    service.saveOrUpdate(configuration, operationId);
  }

}
