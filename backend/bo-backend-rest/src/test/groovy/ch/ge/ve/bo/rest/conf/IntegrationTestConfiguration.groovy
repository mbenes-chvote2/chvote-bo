/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.rest.conf

import ch.ge.ve.bo.service.conf.ServiceConfiguration
import ch.ge.ve.bo.service.file.FileService
import ch.ge.ve.bo.service.operation.OperationService
import ch.ge.ve.bo.service.operation.parameters.document.OperationDocumentService
import ch.ge.ve.bo.service.operation.parameters.doi.OperationDomainInfluenceService
import ch.ge.ve.bo.service.operation.parameters.repository.OperationRepositoryService
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Import
import spock.mock.DetachedMockFactory

/**
 * Configuration defining mocked services used to test the controllers.
 */
@TestConfiguration
@Import(ServiceConfiguration)
class IntegrationTestConfiguration {

    private DetachedMockFactory factory = new DetachedMockFactory()

    @Bean
    OperationService operationService() {
        factory.Mock(OperationService)
    }

    @Bean
    OperationRepositoryService operationRepositoryService() {
        factory.Mock(OperationRepositoryService)
    }

    @Bean
    OperationDomainInfluenceService operationDomainInfluenceService() {
        factory.Mock(OperationDomainInfluenceService)
    }

    @Bean
    OperationDocumentService operationDocumentService() {
        factory.Mock(OperationDocumentService)
    }

    @Bean
    FileService applicationFileService() {
        factory.Mock(FileService)
    }
}
