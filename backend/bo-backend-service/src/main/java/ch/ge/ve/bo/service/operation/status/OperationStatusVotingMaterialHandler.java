/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.status;

import ch.ge.ve.bo.repository.entity.DeploymentTarget;
import ch.ge.ve.bo.repository.entity.Operation;
import ch.ge.ve.bo.repository.entity.OperationParameterType;
import ch.ge.ve.bo.service.model.PactConfiguration;
import ch.ge.ve.bo.service.operation.exception.PactRequestException;
import ch.ge.ve.bo.service.operation.model.OperationStatusVo;
import ch.ge.ve.bo.service.operation.model.VotingMaterialStatusVo;
import ch.ge.ve.bo.service.printer.PrinterTemplateService;
import ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialsStatusVo;
import java.util.Map;
/**
 * Service responsible of computing voting material status
 */
public class OperationStatusVotingMaterialHandler {

  private final OperationStatusHandlerHelper helper;
  private final PactConfiguration            pactConfiguration;
  private final PrinterTemplateService       printerTemplateService;

  /**
   * Default constructor
   */
  public OperationStatusVotingMaterialHandler(
      OperationStatusHandlerHelper helper, PactConfiguration pactConfiguration,
      PrinterTemplateService printerTemplateService) {
    this.helper = helper;
    this.pactConfiguration = pactConfiguration;
    this.printerTemplateService = printerTemplateService;
  }
  /**
   * compute voting material status
   */
  public VotingMaterialStatusVo getVotingMaterialStatusVo(Operation operation) throws PactRequestException {
    Map<String, Boolean> completeBySection =
        helper.computeIsCompleteBySection(operation, OperationParameterType.VOTING_MATERIAL);

    String virtualPrinterId = printerTemplateService.getVirtualPrinterForTestCard().id;

    if (operation.getDeploymentTarget() == DeploymentTarget.NOT_DEFINED
        || !OperationStatusVo.isComplete(completeBySection)) {
      return new VotingMaterialStatusVo(
          VotingMaterialStatusVo.State.INCOMPLETE,
          operation.getLastVotingMaterialUpdateDate(),
          completeBySection,
          operation.getLogin());
    }

    VotingMaterialsStatusVo status = helper.doStatusRequest(
        VotingMaterialsStatusVo.class, operation.getId(), pactConfiguration.getUrl().getVotingMaterialStatus());

    if (status == null || operation.getLastVotingMaterialUpdateDate().isAfter(status.getLastChangeDate())) {
      return new VotingMaterialStatusVo(VotingMaterialStatusVo.State.COMPLETE,
                                        operation.getLastVotingMaterialUpdateDate(), completeBySection,
                                        operation.getLogin());
    }
    return new VotingMaterialStatusVo(status, completeBySection, virtualPrinterId);
  }
}
