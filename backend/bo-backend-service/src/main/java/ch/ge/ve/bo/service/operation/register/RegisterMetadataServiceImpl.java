/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.register;

import static ch.ge.ve.bo.service.utils.HashUtils.computeFilesHash;

import ch.ge.ve.bo.FileType;
import ch.ge.ve.bo.repository.ConnectedUser;
import ch.ge.ve.bo.repository.RegisterMetadataRepository;
import ch.ge.ve.bo.repository.SecuredOperationRepository;
import ch.ge.ve.bo.repository.entity.File;
import ch.ge.ve.bo.repository.entity.RegisterMetadata;
import ch.ge.ve.bo.service.operation.SecuredOperationHolderService;
import ch.ge.ve.bo.service.operation.register.exception.ConcurrentImportException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.EntityNotFoundException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.Transactional;

/**
 * Default Implementation of RegisterMetadataService
 */
public class RegisterMetadataServiceImpl implements RegisterMetadataService {

  private final RegisterMetadataRepository    registerMetadataRepository;
  private final SecuredOperationRepository    operationRepository;
  private final SecuredOperationHolderService securedOperationHolderService;
  private final long                          tempFileTTL;

  /**
   * default constructor
   */
  public RegisterMetadataServiceImpl(RegisterMetadataRepository registerMetadataRepository,
                                     SecuredOperationRepository operationRepository,
                                     SecuredOperationHolderService securedOperationHolderService,
                                     long tempFileTTL) {
    this.registerMetadataRepository = registerMetadataRepository;
    this.operationRepository = operationRepository;
    this.securedOperationHolderService = securedOperationHolderService;
    this.tempFileTTL = tempFileTTL;
  }


  private void updateOperationRegisterMap(Long operationId) {
    String newHash = computeFilesHash(getAllByFileOperationIdAndValidated(operationId, true)
                                          .stream()
                                          .map(RegisterMetadata::getHashTable));
    operationRepository.updateRegisterHash(operationId, newHash);

  }


  @Override
  @Transactional(readOnly = true)
  public List<RegisterMetadata> getAllByFileOperationIdAndValidated(Long operationId, boolean validated) {
    return securedOperationHolderService
        .safeReadAll(() -> registerMetadataRepository.findAllByFileOperationIdAndValidated(operationId, validated));
  }

  @Override
  @Transactional
  public void store(Long operationId,
                    String messageUniqueId,
                    String fileName,
                    byte[] zippedFile,
                    String hashTable,
                    String report,
                    String preRegisterHash,
                    String postRegisterHash) {
    securedOperationHolderService.doTechnicalNotOptionalUpdate(
        () -> {
          String businessKey = getBusinessKey(operationId, messageUniqueId);
          // Can only happen if the old file is temporary
          registerMetadataRepository.deleteByFileBusinessKey(businessKey);
          registerMetadataRepository.flush();
          File file = new File();
          file.setType(FileType.REGISTER);
          file.setBusinessKey(businessKey);
          file.setOperation(operationRepository.findOne(operationId, false));
          file.setFileName(fileName);
          file.setFileContent(zippedFile);
          file.setDate(LocalDateTime.now());

          RegisterMetadata registerMetadata = new RegisterMetadata();
          registerMetadata.setMessageUniqueId(messageUniqueId);
          registerMetadata.setFile(file);
          registerMetadata.setHashTable(hashTable);
          registerMetadata.setReport(report);
          registerMetadata.setValidated(false);
          registerMetadata.setPreRegisterHash(preRegisterHash);
          registerMetadata.setPostRegisterHash(postRegisterHash);
          return registerMetadata;
        },
        registerMetadataRepository::save
    );
  }

  @Override
  @Transactional
  public void validate(Long operationId, String messageUniqueId) throws ConcurrentImportException {

    String businessKey = getBusinessKey(operationId, messageUniqueId);
    RegisterMetadata metadata = securedOperationHolderService
        .safeRead(() -> registerMetadataRepository.findByFileBusinessKey(businessKey))
        .orElseThrow(() -> new EntityNotFoundException("file business key : " + businessKey));


    if (operationRepository.getRegisterFilesLock(
        operationId, metadata.getPreRegisterHash(), metadata.getPostRegisterHash()) == 0) {
      throw new ConcurrentImportException();
    }

    securedOperationHolderService.doNotOptionalUpdateAndForget(
        () -> metadata,
        entity -> {
          metadata.setValidated(true);
          registerMetadataRepository.save(entity);
        }
    );
  }

  @Override
  @Scheduled(fixedRateString = "${operation.register.temporaryFile.ttl}")
  @Transactional
  public void cleanNotValidated() {
    ConnectedUser.technical();
    try {
      securedOperationHolderService
          .safeReadAll(() -> registerMetadataRepository.findAllByValidated(false))
          .stream()
          .filter(md -> Duration.between(md.getFile().getSaveDate(), LocalDateTime.now()).toMillis() > tempFileTTL)
          .forEach(md -> securedOperationHolderService
              .doTechnicalNotOptionalUpdateAndForget(() -> md, registerMetadataRepository::delete));
    } finally {
      ConnectedUser.unset();
    }
  }

  @Override
  @Transactional
  public void delete(long operationId, String messageUniqueId) {
    securedOperationHolderService.doUpdateAndForget(
        () -> registerMetadataRepository.findByFileBusinessKey(getBusinessKey(operationId, messageUniqueId)),
        registerMetadataRepository::delete
    );

    updateOperationRegisterMap(operationId);
  }

  @Override
  public RegisterMetadata getByOperationIdAndFileMessageUniqueIdAndValidated(long operationId, String
      messageUniqueId, boolean validated) {
    return securedOperationHolderService
        .safeRead(() -> registerMetadataRepository
            .findByFileBusinessKeyAndValidated(getBusinessKey(operationId, messageUniqueId), validated))
        .orElseThrow(() -> new EntityNotFoundException(
            "File " + getBusinessKey(operationId, messageUniqueId) + " " + validated));
  }

  private String getBusinessKey(Long operationId, String messageUniqueId) {
    return String.format("%d|%s|%s", operationId, FileType.REGISTER, messageUniqueId);
  }

}
