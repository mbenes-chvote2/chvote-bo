/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.xml.sax.SAXParseException;

/**
 * Representation of a schema validtion error.
 */
public class SchemaValidationError {
  public final int    lineNumber;
  public final int    columnNumber;
  public final String message;

  /**
   * Default construcor using a {@link SAXParseException}
   */
  public SchemaValidationError(SAXParseException exception) {
    this.lineNumber = exception.getLineNumber();
    this.columnNumber = exception.getColumnNumber();
    this.message = exception.getMessage();
  }

  /**
   * Default constructor
   */
  @JsonCreator
  public SchemaValidationError(@JsonProperty("lineNumber") int lineNumber,
                               @JsonProperty("columnNumber") int columnNumber,
                               @JsonProperty("message") String message) {
    this.lineNumber = lineNumber;
    this.columnNumber = columnNumber;
    this.message = message;
  }
}
