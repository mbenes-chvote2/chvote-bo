/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.printer;

import ch.ge.ve.bo.service.model.printer.PrinterConfiguration;
import ch.ge.ve.bo.service.model.printer.PrinterTemplateVo;

/**
 * Service responsible of providing printer templcate for the connected user realm
 */
public interface PrinterTemplateService {

  /**
   * Retrieve {@link PrinterConfiguration} used for test card
   */
  PrinterConfiguration getVirtualPrinterForTestCard();

  /**
   * @return All printer templates for connected user realm
   */
  PrinterTemplateVo[] getAllPrinterTemplates(String canton);

  /**
   * get all printer templates associate to a given printer id
   */
  PrinterTemplateVo[] getPrinterTemplatesByPrinterId(String printerId);

  /**
   * retieve a printer template given the canton and its name
   */
  PrinterTemplateVo getPrinterTemplate(String canton, String printerTemplate);
}
