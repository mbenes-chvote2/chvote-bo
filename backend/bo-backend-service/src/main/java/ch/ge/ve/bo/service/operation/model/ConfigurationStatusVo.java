/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.model;

import ch.ge.ve.bo.service.operation.status.ReadonlyStatusServiceImpl;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Value object representing configuration status
 */
public class ConfigurationStatusVo {

  public enum State {
    INCOMPLETE(false),
    COMPLETE(false),
    TEST_SITE_IN_DEPLOYMENT(true),
    IN_VALIDATION(true),
    IN_ERROR(false),
    VALIDATED(true),
    INVALIDATED(false),
    DEPLOYMENT_REQUESTED(true),
    DEPLOYMENT_REFUSED(false),
    DEPLOYED(true);

    private final boolean readOnly;

    State(boolean readOnly) {
      this.readOnly = readOnly;
    }

    public boolean isReadOnly() {
      return readOnly;
    }
  }

  private final Map<String, Boolean> completedSections;
  private final Map<String, Boolean> sectionsInError = new HashMap<>();
  private final List<String>         readOnlySections;
  private final State                state;
  private final String               pactUrl;
  private final ModificationMode     modificationMode;
  private final boolean              allSectionsInReadOnly;
  private final String               voteReceiverUrl;

  private final String        generatedVotingCardLocation;
  private final LocalDateTime stateDate;
  private final String        stateUser;
  private final String        comment;

  /**
   * Default constructor
   */
  public ConfigurationStatusVo(
      Map<String, Boolean> completedSections, State state, String comment, LocalDateTime stateDate,
      String stateUser, String pactUrl, String generatedVotingCardLocation, ModificationMode modificationMode,
      List<String> readOnlySections, String voteReceiverUrl) {
    this.completedSections = completedSections;
    this.state = state;
    this.comment = comment;
    this.stateDate = stateDate;
    this.stateUser = stateUser;
    this.pactUrl = pactUrl;
    this.generatedVotingCardLocation = generatedVotingCardLocation;
    this.modificationMode = modificationMode;
    this.readOnlySections = readOnlySections;
    this.allSectionsInReadOnly =
        readOnlySections.size() == ReadonlyStatusServiceImpl.ConfigurationSection.values().length;
    this.voteReceiverUrl = voteReceiverUrl;
  }


  void markSectionInError(String section) {
    sectionsInError.put(section, true);
  }


  public Map<String, Boolean> getCompletedSections() {
    return completedSections;
  }

  public Map<String, Boolean> getSectionsInError() {
    return sectionsInError;
  }

  public List<String> getReadOnlySections() {
    return readOnlySections;
  }

  public State getState() {
    return state;
  }

  public String getPactUrl() {
    return pactUrl;
  }

  public ModificationMode getModificationMode() {
    return modificationMode;
  }

  public boolean getAllSectionsInReadOnly() {
    return allSectionsInReadOnly;
  }

  public String getVoteReceiverUrl() {
    return voteReceiverUrl;
  }

  @JsonIgnore
  public String getGeneratedVotingCardLocation() {
    return generatedVotingCardLocation;
  }

  @JsonIgnore
  public LocalDateTime getStateDate() {
    return stateDate;
  }

  @JsonIgnore
  public String getStateUser() {
    return stateUser;
  }

  @JsonIgnore
  public String getComment() {
    return comment;
  }


  /**
   * Builder used to simplify creation of {@link ConfigurationStatusVo}
   */
  public static class Builder {
    private Map<String, Boolean> completedSections;
    private State                state;
    private String               comment;
    private LocalDateTime        stateDate;
    private String               stateUser;
    private String               pactUrl;
    private String               generatedVotingCardLocation;
    private ModificationMode     modificationMode;
    private List<String>         readOnlySections = Collections.emptyList();
    private String               voteReceiverUrl;

    public void setReadOnlySections(List<String> readOnlySections) {
      this.readOnlySections = readOnlySections;
    }

    public Builder setCompletedSections(Map<String, Boolean> completedSections) {
      this.completedSections = completedSections;
      return this;
    }

    /**
     * update complete state related properties
     */
    public Builder setState(State state, LocalDateTime stateDate, String stateUser, String comment) {
      this.stateDate = stateDate;
      this.stateUser = stateUser;
      this.comment = comment;
      this.state = state;
      return this;
    }

    /**
     * update complete state related properties
     */
    public Builder setState(State state, LocalDateTime stateDate, String stateUser) {
      this.stateDate = stateDate;
      this.stateUser = stateUser;
      this.state = state;
      return this;
    }

    public Builder setPactUrl(String pactUrl) {
      this.pactUrl = pactUrl;
      return this;
    }

    public Builder setVoteReceiverUrl(String voteReceiverUrl) {
      this.voteReceiverUrl = voteReceiverUrl;
      return this;
    }

    public Builder setGeneratedVotingCardLocation(String generatedVotingCardLocation) {
      this.generatedVotingCardLocation = generatedVotingCardLocation;
      return this;
    }


    public Builder setModificationMode(ModificationMode modificationMode) {
      this.modificationMode = modificationMode;
      return this;
    }

    /**
     * Build {@link ConfigurationStatusVo} with current builder state
     */
    public ConfigurationStatusVo build() {
      return new ConfigurationStatusVo(completedSections, state, comment, stateDate, stateUser, pactUrl,
                                       generatedVotingCardLocation, modificationMode, readOnlySections,
                                       voteReceiverUrl);
    }
  }


}
