/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.register;

import ch.ge.ve.bo.service.exception.BusinessException;
import ch.ge.ve.bo.service.exception.TechnicalException;
import ch.ge.ve.bo.service.operation.exception.ImportFailure;
import ch.ge.ve.bo.service.operation.register.exception.UnexpectedEndOfImportException;
import ch.ge.ve.bo.service.operation.register.processor.BusinessProcessor;
import ch.ge.ve.bo.service.operation.register.processor.SchemaValidationProcessor;
import ch.ge.ve.bo.service.operation.register.processor.ZipProcessor;
import java.util.concurrent.CountDownLatch;
import java.util.stream.Stream;

/**
 * Hypervisor responsible of managing multiple parallel input stream
 */
public class RegisterImportHypervisor {
  private CountDownLatch runningProcessor = new CountDownLatch(3);
  private final BusinessProcessor         businessProcessor;
  private final SchemaValidationProcessor schemaValidationProcessor;
  private final ZipProcessor              zipProcessor;
  private       Throwable                 stopCause;


  /**
   * Constructor for each stream processor
   */
  RegisterImportHypervisor(BusinessProcessor businessProcessor, SchemaValidationProcessor
    schemaValidationProcessor, ZipProcessor zipProcessor) {
    this.businessProcessor = businessProcessor;
    this.schemaValidationProcessor = schemaValidationProcessor;
    this.zipProcessor = zipProcessor;

    businessProcessor.setHypervisor(this);
    schemaValidationProcessor.setHypervisor(this);
    zipProcessor.setHypervisor(this);
  }


  void processFinish() {
    runningProcessor.countDown();
  }

  void stopOthersFrom(RegisterImportProcessor processor, Throwable cause) {
    if (stopCause == null) {
      this.stopCause = cause;
      Stream.of(businessProcessor, schemaValidationProcessor, zipProcessor)
            .filter(otherProcessor -> !processor.equals(otherProcessor))
            .forEach(RegisterImportProcessor::stopProcessing);
    }
  }

  void waitEndResult() throws BusinessException {
    try {
      runningProcessor.await();
      handleThrownException();
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
      throw new UnexpectedEndOfImportException(e);
    }
  }

  private void handleThrownException() throws BusinessException {
    if (stopCause != null) {
      if (stopCause instanceof BusinessException) {
        throw (BusinessException) stopCause;
      }
      if (stopCause instanceof TechnicalException) {
        throw (TechnicalException) stopCause;
      } else {
        throw new ImportFailure(stopCause);
      }
    }
  }

  /**
   * Start the superviser and stream processing
   */
  public void run() {
    this.businessProcessor.run();
    this.schemaValidationProcessor.run();
    this.zipProcessor.run();
  }

  BusinessProcessor getBusinessProcessor() {
    return businessProcessor;
  }

  SchemaValidationProcessor getSchemaValidationProcessor() {
    return schemaValidationProcessor;
  }

  ZipProcessor getZipProcessor() {
    return zipProcessor;
  }

  public Throwable getStopCause() {
    return stopCause;
  }

}
