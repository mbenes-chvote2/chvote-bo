/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.consistency;

import ch.ge.ve.bo.repository.entity.Operation;
import ch.ge.ve.bo.service.operation.model.ConsistencyResultVo;
import ch.ge.ve.bo.service.operation.model.OperationStatusVo;

/**
 * Abstract class to check consistency
 */
public abstract class AbstractConsistencyChecker implements ConsistencyChecker {
  private static final ConsistencyResultVo EMPTY_RESULT = new ConsistencyResultVo();

  public enum CheckType {
    CONFIGURATION, VOTING_MATERIAL
  }

  @Override
  public ConsistencyResultVo checkForOperation(OperationStatusVo operationStatus, Operation operation) {
    if (!operationStatus
        .getConfigurationStatus().getState().isReadOnly() && getCheckType().equals(CheckType.CONFIGURATION) ||
        operationStatus.getVotingMaterialStatus() != null && !operationStatus.getVotingMaterialStatus().isReadOnly() &&
        getCheckType().equals(CheckType.VOTING_MATERIAL)) {
      return checkForOperation(operation);
    }
    return EMPTY_RESULT;
  }

  /**
   * @return Return consistency check type which can be configuration or voting material configuration
   */
  public abstract CheckType getCheckType();

  /**
   * Shortcut to checkForOperation(OperationStatusVo, Operation) to make sure that prevent implementation id this
   * check should be proceed depending on status
   *
   */
  public abstract ConsistencyResultVo checkForOperation(Operation operation);
}
