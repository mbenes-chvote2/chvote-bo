/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.management.entity;

import ch.ge.ve.bo.service.operation.exception.CannotRevokeManagementEntityException;
import java.util.Collection;

/**
 * Service managing the list of invited management entities.
 */
public interface ManagementEntityService {

  /**
   * @return all the available management entities.
   */
  String[] getAllManagementEntities();

  /**
   * Invite management entities.
   *
   * @param operationId        the concerned operation ID
   * @param managementEntities the list of management entities to invite
   */
  void invite(long operationId, Collection<String> managementEntities);

  /**
   * Revoke management entities.
   *
   * @param operationId        the concerned operation ID
   * @param managementEntities the list of management entities to revoke
   *
   * @throws CannotRevokeManagementEntityException if some revoked management entities already have imported files in the operation
   */
  void revoke(long operationId, Collection<String> managementEntities) throws CannotRevokeManagementEntityException;
}
