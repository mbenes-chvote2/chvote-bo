/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.consistency.checks;

import static ch.ge.ve.bo.service.operation.model.ConsistencyErrorVo.ConsistencyErrorType.BALLOT_IN_ELECTION_PAGE_PROPERTIES_NOT_IN_OPERATION_REPOSITORY;
import static ch.ge.ve.bo.service.operation.model.ConsistencyResultVo.param;

import ch.ge.ve.bo.repository.entity.Operation;
import ch.ge.ve.bo.service.operation.consistency.AbstractConsistencyChecker;
import ch.ge.ve.bo.service.operation.model.ConsistencyResultVo;
import ch.ge.ve.bo.service.operation.parameters.election.ElectionPagePropertiesService;
import ch.ge.ve.bo.service.operation.parameters.repository.OperationRepositoryService;
import java.util.Set;

/**
 * Check to verify all ballots in election page properties are in operation repository
 */
public class AllBallotsInElectionPagePropertiesShouldBeInOperationRepository extends AbstractConsistencyChecker {
  private final OperationRepositoryService    repositoryService;
  private final ElectionPagePropertiesService electionPagePropertiesService;

  /**
   * Default constructor
   */
  public AllBallotsInElectionPagePropertiesShouldBeInOperationRepository(
      OperationRepositoryService repositoryService,
      ElectionPagePropertiesService electionPagePropertiesService) {
    this.repositoryService = repositoryService;
    this.electionPagePropertiesService = electionPagePropertiesService;
  }

  @Override
  public CheckType getCheckType() {
    return CheckType.CONFIGURATION;
  }

  @Override
  public ConsistencyResultVo checkForOperation(Operation operation) {
    Set<String> operationBallots = repositoryService.getAllBallotsForOperation(operation.getId(), true, false);
    ConsistencyResultVo results = new ConsistencyResultVo();
    electionPagePropertiesService.getBallotToModelIdMapping(operation.getId())
                                 .keySet()
                                 .stream()
                                 .filter(ballot -> !operationBallots.contains(ballot))
                                 .forEach(ballot -> results.addError(
                                     BALLOT_IN_ELECTION_PAGE_PROPERTIES_NOT_IN_OPERATION_REPOSITORY,
                                     param("ballot", ballot)));
    return results;
  }


}
