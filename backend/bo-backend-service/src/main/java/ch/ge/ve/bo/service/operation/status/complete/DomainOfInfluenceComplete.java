/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.status.complete;

import ch.ge.ve.bo.FileType;
import ch.ge.ve.bo.repository.entity.Operation;
import ch.ge.ve.bo.service.file.FileService;
import java.util.EnumSet;
import org.springframework.stereotype.Service;

/**
 * Identify if domain of influence is complete for an operation
 */
@Service
public class DomainOfInfluenceComplete implements ConfigurationIsCompleteSupplier {
  private final FileService fileService;

  /**
   * Default constructor
   */
  public DomainOfInfluenceComplete(FileService fileService) {
    this.fileService = fileService;
  }

  @Override
  public boolean isSectionComplete(Operation operation) {
    return !fileService.getFiles(operation.getId(), false, EnumSet.of(FileType.DOMAIN_OF_INFLUENCE)).isEmpty();
  }
}
