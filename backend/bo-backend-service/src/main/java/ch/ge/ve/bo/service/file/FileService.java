/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.file;

import ch.ge.ve.bo.FileType;
import ch.ge.ve.bo.Language;
import ch.ge.ve.bo.service.exception.BusinessException;
import ch.ge.ve.bo.service.file.exception.InvalidFileException;
import ch.ge.ve.bo.service.file.model.FileVo;
import java.util.List;

/**
 * Services handling operation files.
 */
public interface FileService {

  /**
   * Import a new file in the database, optionally after having deleted any file with the same business key.
   *
   * @param fileVo        the file to import
   * @param deleteIfExist specifies whether or not any same-business key file should be deleted
   *
   * @return the imported file updated with database information
   *
   * @throws BusinessException if a business exception occurred during the import
   */
  FileVo importFile(FileVo fileVo, boolean deleteIfExist) throws BusinessException;


  /**
   * Replace an existing file in the database.
   *
   * @param oldId        the old file id to delete
   * @param fileVo        the file to import
   *
   * @return the imported file updated with database information
   *
   */
  FileVo replaceFile(long oldId, FileVo fileVo);

  /**
   * Retrieves the files information (with or without the file's content) linked to the given operation ID.
   *
   * @param operationId the concerned operation's ID
   * @param withContent get content also
   * @param types       file types to get
   *
   * @return the list of files associated to this operation, or an empty list if no corresponding file was found
   */
  List<FileVo> getFiles(long operationId, boolean withContent, Iterable<FileType> types);

  /**
   * Retrieves the files information of the given type and language, and linked to the given operation ID.
   *
   * @param operationId the concerned operation's ID
   * @param type        the file type
   * @param language    the file's language, or {@code null} if it does not apply
   *
   * @return the list of files associated to this operation or an empty list if no corresponding file was found
   */
  List<FileVo> getFiles(long operationId, FileType type, Language language);

  /**
   * Retrieve the full file (with the file's content) identified by its ID.
   *
   * @param fileId the file's ID
   *
   * @return the corresponding file
   */
  FileVo getFile(long fileId);

  /**
   * Delete the file identified by its ID.
   *
   * @param fileId the file ID
   */
  void deleteFile(long fileId);

  /**
   * Builds a business key for an operation general documents
   *
   * @param operationId the concerned operation's ID
   * @param fileType    the file type
   * @param language    the file's language, or {@code null} if it does not apply
   *
   * @return the key
   */
  String buildDocumentBusinessKey(long operationId, FileType fileType, Language language);

  /**
   * Builds a business key for an operation XML file
   *
   * @param senderId  the sender ID
   * @param messageId the message ID
   *
   * @return the key
   */
  String buildApplicationBusinessKey(String senderId, String messageId) throws InvalidFileException;

}