/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.consistency.checks;

import ch.ge.ve.bo.repository.entity.Operation;
import ch.ge.ve.bo.repository.entity.RegisterMetadata;
import ch.ge.ve.bo.service.exception.TechnicalException;
import ch.ge.ve.bo.service.operation.consistency.AbstractConsistencyChecker;
import ch.ge.ve.bo.service.operation.model.ConsistencyErrorVo;
import ch.ge.ve.bo.service.operation.model.ConsistencyResultVo;
import ch.ge.ve.bo.service.operation.register.RegisterMetadataService;
import ch.ge.ve.bo.service.operation.register.model.ImportedRegisterFile;
import ch.ge.ve.bo.service.printer.PrinterTemplateService;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * check to verify all municipalities have a printer
 */
public class AllMunicipalitiesShouldHaveAPrinter extends AbstractConsistencyChecker {

  private final RegisterMetadataService registerMetadataService;
  private final ObjectMapper            objectMapper;
  private final PrinterTemplateService  printerTemplateService;
  /**
   * Default constructor
   */
  public AllMunicipalitiesShouldHaveAPrinter(RegisterMetadataService registerMetadataService,
                                             ObjectMapper objectMapper,
                                             PrinterTemplateService printerTemplateService) {
    this.registerMetadataService = registerMetadataService;
    this.objectMapper = objectMapper;
    this.printerTemplateService = printerTemplateService;
  }

  @Override
  public AbstractConsistencyChecker.CheckType getCheckType() {
    return CheckType.VOTING_MATERIAL;
  }

  @Override
  public ConsistencyResultVo checkForOperation(Operation operation) {
    ConsistencyResultVo consistencyResultVo = new ConsistencyResultVo();

    if (operation.getPrinterTemplate() == null) {
      return consistencyResultVo;
    }


    Set<Integer> municipalitiesOfsIdHavingPrinter =
        Stream.of(printerTemplateService.getPrinterTemplate(operation.getCanton(), operation.getPrinterTemplate())
                      .printerMunicipalityMappings)
              .map(mapping -> mapping.municipalityOfsId).collect(Collectors.toSet());


    try {
      for (RegisterMetadata metaData : registerMetadataService
          .getAllByFileOperationIdAndValidated(operation.getId(), true)) {
        ImportedRegisterFile importedRegisterFile =
            objectMapper.readValue(metaData.getReport(), ImportedRegisterFile.class);
        importedRegisterFile.detailedReport.municipalities.forEach(
            municipality -> {
              if (!municipalitiesOfsIdHavingPrinter.contains(municipality.ofsId)) {
                consistencyResultVo.addError(
                    ConsistencyErrorVo.ConsistencyErrorType.VM_NO_PRINTER_ASSOCIATED_TO_MUNICIPALITY,
                    ConsistencyResultVo.param("filename", importedRegisterFile.fileName),
                    ConsistencyResultVo.param("ofsId", String.valueOf(municipality.ofsId)),
                    ConsistencyResultVo.param("municipality", String.valueOf(municipality.name)));
              }
            }
        );
      }

      return consistencyResultVo;
    } catch (Exception e) {
      throw new TechnicalException(e);
    }

  }
}
