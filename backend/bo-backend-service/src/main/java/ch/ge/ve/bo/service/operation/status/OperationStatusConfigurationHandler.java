/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.status;

import ch.ge.ve.bo.repository.entity.Operation;
import ch.ge.ve.bo.repository.entity.OperationParameterType;
import ch.ge.ve.bo.repository.entity.WorkflowStep;
import ch.ge.ve.bo.service.exception.TechnicalException;
import ch.ge.ve.bo.service.model.PactConfiguration;
import ch.ge.ve.bo.service.operation.exception.PactRequestException;
import ch.ge.ve.bo.service.operation.model.ConfigurationStatusVo;
import ch.ge.ve.bo.service.operation.model.OperationStatusVo;
import ch.ge.ve.chvote.pactback.contract.operation.status.DeployedConfigurationStatusVo;
import ch.ge.ve.chvote.pactback.contract.operation.status.InTestConfigurationStatusVo;
import ch.ge.ve.chvote.pactback.contract.operation.status.OperationConfigurationStatusVo;
import java.util.Map;

/**
 * Service responsible of computing configuration status
 */
public class OperationStatusConfigurationHandler {
  private final OperationStatusHandlerHelper operationStatusHandlerHelper;
  private final PactConfiguration            pactConfiguration;

  /**
   * Default constructor
   */
  public OperationStatusConfigurationHandler(OperationStatusHandlerHelper operationStatusHandlerHelper,
                                             PactConfiguration pactConfiguration) {
    this.operationStatusHandlerHelper = operationStatusHandlerHelper;
    this.pactConfiguration = pactConfiguration;
  }

  /**
   * compute configuration status
   */
  public ConfigurationStatusVo.Builder getConfigurationStatusVo(Operation operation) throws PactRequestException {
    Map<String, Boolean> completedSections =
        operationStatusHandlerHelper.computeIsCompleteBySection(operation, OperationParameterType.CONFIGURATION);


    ConfigurationStatusVo.Builder builder = new ConfigurationStatusVo.Builder();
    builder.setState(OperationStatusVo.isComplete(completedSections) ?
                         ConfigurationStatusVo.State.COMPLETE :
                         ConfigurationStatusVo.State.INCOMPLETE,
                     operation.getLastConfigurationUpdateDate(),
                     operation.getLogin());

    if (operation.getWorkflowStep() != WorkflowStep.CONFIGURATION_NOT_SENT) {
      fillStatusWithPact(operation, builder);
    }

    builder.setCompletedSections(completedSections);
    return builder;
  }

  private void fillStatusWithPact(Operation operation, ConfigurationStatusVo.Builder builder)
      throws PactRequestException {
    OperationConfigurationStatusVo configurationStatus = operationStatusHandlerHelper.doStatusRequest(
        OperationConfigurationStatusVo.class, operation.getId(),
        pactConfiguration.getUrl().getOperationConfigurationStatus());

    if (configurationStatus == null) {
      throw new TechnicalException("operation is in step " + operation
          .getWorkflowStep() + " but PACT returned 404 on configuration status request");
    }

    InTestConfigurationStatusVo inTest = configurationStatus.getInTest();
    if (inTest != null && inTest.getLastChangeDate().isAfter(operation.getLastConfigurationUpdateDate())) {
      builder.setState(ConfigurationStatusVo.State.valueOf(inTest.getState().name()),
                       inTest.getLastChangeDate(), inTest.getLastChangeUser(), inTest.getComment());
      builder.setPactUrl(inTest.getConfigurationPageUrl());
      builder.setGeneratedVotingCardLocation(inTest.getVotingCardsLocation());
      builder.setVoteReceiverUrl(inTest.getVoteReceiverUrl());
    }

    DeployedConfigurationStatusVo deployedConfig = configurationStatus.getDeployed();
    if (inTest == null && deployedConfig != null) {
      builder.setState(ConfigurationStatusVo.State.DEPLOYED, deployedConfig.getLastChangeDate(),
                       deployedConfig.getLastChangeUser());
      builder.setVoteReceiverUrl(deployedConfig.getVoteReceiverUrl());
    }
  }

}
