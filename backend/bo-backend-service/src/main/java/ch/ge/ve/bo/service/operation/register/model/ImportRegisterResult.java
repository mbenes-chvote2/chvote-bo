/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.register.model;

import ch.ge.ve.bo.service.model.SchemaValidationError;
import ch.ge.ve.bo.service.operation.register.processor.BusinessProcessorContext;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * value object for imported register result
 */
public class ImportRegisterResult {
  private final List<DuplicateVo>           duplicates;
  private final List<UndefinedDoiVo>        undefinedDois;
  private final List<SchemaValidationError> validationErrors = new ArrayList<>();
  private final String                      messageUniqueId;
  private final String                      fileName;
  private final String                      emitter;
  private final LocalDateTime               emissionDate;
  private final int                         voterCount;
  private final int                         doiCount;
  private final int                         countingCircleCount;
  private final List<InvalidDateOfBirthVo>  invalidDateOfBirths;
  private       byte[]                      detailedReport;

  /**
   * Default constructor
   */
  public ImportRegisterResult(BusinessProcessorContext context, List<SchemaValidationError> schemaValidationError) {
    this.duplicates = context.getDuplicates();
    this.undefinedDois = context.getUndefinedDois();
    this.invalidDateOfBirths = context.getInvalidDateOfBirths();
    this.messageUniqueId = context.getUniqueMessageIdContext().getMessageUniqueId();
    this.fileName = context.getFileInfo().getFileName();
    this.emitter = context.getFileInfo().getEmitter();
    this.emissionDate = context.getFileInfo().getEmissionDate();
    this.voterCount = context.getActualNumberOfVoter();
    this.doiCount = context.getDoiCount();
    this.countingCircleCount = context.getCountingCircleCount();

    validationErrors.addAll(context.getOtherSchemaValidationErrors());
    validationErrors.addAll(schemaValidationError);
  }


  public List<DuplicateVo> getDuplicates() {
    return duplicates;
  }

  public List<UndefinedDoiVo> getUndefinedDois() {
    return undefinedDois;
  }

  public List<InvalidDateOfBirthVo> getInvalidDateOfBirths() {
    return invalidDateOfBirths;
  }

  public String getMessageUniqueId() {
    return messageUniqueId;
  }

  public List<SchemaValidationError> getValidationErrors() {
    return validationErrors;
  }

  public String getFileName() {
    return fileName;
  }

  public String getEmitter() {
    return emitter;
  }

  public LocalDateTime getEmissionDate() {
    return emissionDate;
  }

  public int getVoterCount() {
    return voterCount;
  }

  public int getDoiCount() {
    return doiCount;
  }

  public int getCountingCircleCount() {
    return countingCircleCount;
  }

  /**
   * Add generated detailed report to the result
   */
  public void addDetailedReport(byte[] detailedReport) {
    this.detailedReport = detailedReport;
  }

  public byte[] getDetailedReport() {
    return detailedReport;
  }
}
