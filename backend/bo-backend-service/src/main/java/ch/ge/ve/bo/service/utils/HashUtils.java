/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.utils;

import static java.nio.charset.StandardCharsets.UTF_8;

import ch.ge.ve.bo.service.exception.TechnicalException;
import ch.ge.ve.protocol.core.support.Hash;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Base64;
import org.bouncycastle.util.encoders.Hex;

/**
 * Utils class to provide cryptographic hashcodes
 */
public class HashUtils {
  private static final String BOUNCYCASTLE_PROVIDER = "BC";
  private static final String BLAKE2B_ALGORITHM     = "BLAKE2B-256";
  private static final int    UPPER_L               = 32;

  static {
    Security.addProvider(new BouncyCastleProvider());
  }

  private HashUtils() {
  }

  /**
   * Convert a string to a cryptographic hash
   */
  public static String toHash(String hashBase) {
    MessageDigest messageDigest;
    try {
      messageDigest = MessageDigest.getInstance(BLAKE2B_ALGORITHM, BOUNCYCASTLE_PROVIDER);
    } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
      throw new TechnicalException(e);
    }
    return new String(Base64.encode(messageDigest.digest(hashBase.getBytes(UTF_8))), UTF_8);
  }

  /**
   * Convert a stream of strings to a cryptographic hash
   */
  public static String computeFilesHash(Stream<String> allVoterHashes) {
    return toHash(allVoterHashes.sorted().collect(Collectors.joining()));
  }

  /**
   * Convert a byte array to a cryptographic hash
   */
  public static String publicKeyToHexHash(byte[] content) {
    return new String(Hex.encode(getHash(content)), UTF_8);
  }

  private static byte[] getHash(byte[] content) {
    return new Hash(BLAKE2B_ALGORITHM, BOUNCYCASTLE_PROVIDER, UPPER_L).hash_L(content);
  }


}
