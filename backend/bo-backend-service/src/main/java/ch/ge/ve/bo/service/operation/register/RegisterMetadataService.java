/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.register;

import ch.ge.ve.bo.repository.entity.RegisterMetadata;
import ch.ge.ve.bo.service.operation.register.exception.ConcurrentImportException;
import java.util.List;

/**
 * Service responsible of managing metadata for register {@link ch.ge.ve.bo.repository.entity.File}
 */
public interface RegisterMetadataService {

  /**
   * Retrive all register metadata for a given operation
   * @param operationId the operation id
   * @param validated register should be validated
   */
  List<RegisterMetadata> getAllByFileOperationIdAndValidated(Long operationId, boolean validated);

  /**
   * Store a new Metadata report
   */
  @SuppressWarnings("squid:S00107")
  void store(Long operationId,
             String messageUniqueId,
             String fileName,
             byte[] zippedFile,
             String hashTable,
             String report,
             String preRegisterHash,
             String postRegisterHash);

  /**
   * Validate a register file after preview
   */
  void validate(Long operationId, String messageUniqueId) throws ConcurrentImportException;

  /**
   * clean up all not validated metadata
   */
  void cleanNotValidated();

  /**
   * Delete a given metadata and associate file
   */
  void delete(long operationId, String messageUniqueId);

  /**
   * retrieve a register metadata through its messageUniqueId
   */
  RegisterMetadata getByOperationIdAndFileMessageUniqueIdAndValidated(
      long operationId, String messageUniqueId, boolean validated);
}
