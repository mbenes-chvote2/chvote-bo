/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.model;

import static java.util.stream.Collectors.toMap;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * value object for representing all {@link ConsistencyErrorVo}
 */
public class ConsistencyResultVo {

  public final List<ConsistencyErrorVo> consistencyErrors;

  /**
   * empty constructor
   */
  public ConsistencyResultVo() {
    this.consistencyErrors = new ArrayList<>();
  }

  /**
   * Default constructor
   */
  @JsonCreator
  public ConsistencyResultVo(@JsonProperty("consistencyErrors") List<ConsistencyErrorVo> consistencyErrors) {
    this.consistencyErrors = consistencyErrors;
  }

  /**
   * a value object representing string key value
   */
  public static Param param(String key, String value) {
    return new Param(key, value);
  }

  /**
   * Add an error to this result
   */
  public ConsistencyResultVo addError(ConsistencyErrorVo.ConsistencyErrorType errorType, Param... errorsParameters) {
    //noinspection unchecked
    consistencyErrors.add(
        new ConsistencyErrorVo(errorType, Stream.of(errorsParameters).collect(toMap(Param::getKey, Param::getValue))));
    return this;
  }

  /**
   * Add all errors from another {@link ConsistencyResultVo}
   */
  public void concat(ConsistencyResultVo consistencyResult) {
    consistencyErrors.addAll(consistencyResult.consistencyErrors);
  }


  public static class Param {
    final String key;
    final String value;

    private Param(String key, String value) {
      this.key = key;
      this.value = value;
    }

    public String getKey() {
      return key;
    }

    public String getValue() {
      return value;
    }
  }
}
