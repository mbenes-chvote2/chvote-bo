/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.parameters.repository.model;

import ch.ge.ve.bo.service.model.SchemaValidationError;
import java.util.List;

/**
 * Result of a repository file import. Contains either the successfully imported file information or a list of
 * validation errors.
 */
public class RepositoryImportResult {

  public final RepositoryFileVo            repositoryFile;
  public final List<SchemaValidationError> validationErrors;

  /**
   * Default constructor
   */
  public RepositoryImportResult(RepositoryFileVo repositoryFile, List<SchemaValidationError> validationErrors) {
    this.repositoryFile = repositoryFile;
    this.validationErrors = validationErrors;
  }
}
