/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.consistency.checks

import ch.ge.ve.bo.Language
import ch.ge.ve.bo.repository.dataset.OperationDataset
import ch.ge.ve.bo.service.operation.model.BallotDocumentationVo
import ch.ge.ve.bo.service.operation.model.ConsistencyErrorVo
import ch.ge.ve.bo.service.operation.parameters.document.BallotDocumentationService
import ch.ge.ve.bo.service.operation.parameters.repository.OperationRepositoryService
import java.time.LocalDateTime
import spock.lang.Specification

class AllBallotsInDocumentationShouldBeInOperationRepositoryTest extends Specification {
    def repositoryService = Mock(OperationRepositoryService)
    def ballotDocumentationService = Mock(BallotDocumentationService)
    def check = new AllBallotsInDocumentationShouldBeInOperationRepository(
            repositoryService,
            ballotDocumentationService
    )

    def "CheckForOperation"() {
        given: "an operation"
        def operation = OperationDataset.electoralOperation()

        and: "A repository has been uploaded with one ballot"
        repositoryService.getAllBallotsForOperation(operation.id, true, true) >> ["known"].toSet()

        and: "A ballot documentation has been defined on absent ballot"

        BallotDocumentationVo known = new BallotDocumentationVo("known", "filename",
                "localizedLabel", "managementEntity", 1, Language.DE, LocalDateTime.now())
        BallotDocumentationVo unknown = new BallotDocumentationVo("unknown", "filename",
                "localizedLabel", "managementEntity", 1, Language.DE, LocalDateTime.now())
        ballotDocumentationService.findByOperation(operation.id) >> [known, unknown]

        when:
        def result = check.checkForOperation(operation)

        then:
        [ConsistencyErrorVo.ConsistencyErrorType.BALLOT_IN_DOCUMENTATION_NOT_IN_OPERATION_REPOSITORY] == result.consistencyErrors.errorType
        "[[ballot:unknown]]" == result.consistencyErrors.errorsParameters.toString()
    }
}
