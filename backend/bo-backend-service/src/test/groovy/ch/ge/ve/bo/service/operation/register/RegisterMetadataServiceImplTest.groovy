/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.register

import ch.ge.ve.bo.repository.ConnectedUserTestUtils
import ch.ge.ve.bo.repository.RegisterMetadataRepository
import ch.ge.ve.bo.repository.SecuredOperationRepository
import ch.ge.ve.bo.repository.entity.File
import ch.ge.ve.bo.repository.entity.Operation
import ch.ge.ve.bo.repository.entity.RegisterMetadata
import ch.ge.ve.bo.service.TestHelpers
import ch.ge.ve.bo.service.operation.register.exception.ConcurrentImportException
import java.time.LocalDateTime
import spock.lang.Specification

class RegisterMetadataServiceImplTest extends Specification {
    public static final LocalDateTime PAST = LocalDateTime.of(2000, 1, 1, 0, 0)
    public static final String NO_HASH = 'DldRwCblQ7Loqy6wYJnaodHl30d3j3eH+qtFzfEv46g='

    def mdRepository = Mock(RegisterMetadataRepository)
    def operationRepository = Mock(SecuredOperationRepository)
    def service = new RegisterMetadataServiceImpl(mdRepository, operationRepository, TestHelpers.passThroughSecuredOperationHolderService(), 5000)

    void cleanup() {
        ConnectedUserTestUtils.disconnectUser()
    }

    def "GetAllByFileOperationIdAndValidated"() {
    }

    def "Store"() {
        given: "a register file to save"

        when: "Store is called"
        service.store(1l, "message", "filename", null, "table", "report", "pre", "post")

        then:
        1 * mdRepository.deleteByFileBusinessKey("1|REGISTER|message")
        1 * mdRepository.save(_)
    }

    def "Validate normally"() {
        given: "a register file"
        def md = metadata()
        mdRepository.findByFileBusinessKey("1|REGISTER|message") >> Optional.of(md)
        operationRepository.getRegisterFilesLock(1, _, _) >> 1

        when: "Import is validated"
        service.validate(1l, "message")

        then:
        md.getValidated()
        1 * mdRepository.save(_)
    }


    def "Validate in concurrency"() {
        given: "a register file"
        def md = metadata()
        mdRepository.findByFileBusinessKey("1|REGISTER|message") >> Optional.of(md)
        operationRepository.getRegisterFilesLock(1, _, _) >> 0

        when: "Import is validated"
        service.validate(1l, "message")

        then:
        thrown(ConcurrentImportException)
        0 * mdRepository.save(_)
    }


    def "DeleteNotValidated"() {
        given: "There is one temp file in database"
        def md = metadata()
        md.getFile().setSaveDate(saveDate)
        md.getFile().setOperation(operation("pre"))
        mdRepository.findAllByValidated(false) >> [md]
        mdRepository.findAllByFileOperationIdAndValidated(1, true) >> []

        when: "file clean up is requested"
        service.cleanNotValidated()

        then:
        (deleted ? 1 : 0) * mdRepository.delete(md)

        where:
        id | saveDate            | deleted
        1  | PAST                | true
        2  | LocalDateTime.now() | false

    }

    def "Delete should call appropriate repository method and update register hash"() {
        given: "There is one temp file in database"
        def md = metadata()
        md.getFile().setFileName("fileName")
        mdRepository.findByFileBusinessKey("1|REGISTER|messageId") >> Optional.of(md)
        mdRepository.findAllByFileOperationIdAndValidated(1, true) >> []

        when: "file clean up is requested"
        service.delete(1, "messageId")

        then:
        1 * mdRepository.delete(md)
        1 * operationRepository.updateRegisterHash(1, NO_HASH)

    }


    RegisterMetadata metadata() {
        RegisterMetadata metadata = new RegisterMetadata()
        File file = new File()
        metadata.setFile(file)
        metadata.setPreRegisterHash("pre")
        metadata.setPostRegisterHash("post")
        return metadata
    }

    Operation operation(String registerHash, String managementEntity = "test") {
        def operation = new Operation()
        operation.registerHash = registerHash
        operation.managementEntity = managementEntity
        operation.id = 1
        return operation
    }

}
