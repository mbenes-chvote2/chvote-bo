/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.upload

import static ch.ge.ve.bo.repository.dataset.OperationDataset.Options.deploymentTarget
import static ch.ge.ve.bo.repository.entity.DeploymentTarget.REAL
import static ch.ge.ve.bo.service.TestHelpers.getResourceString
import static ch.ge.ve.bo.service.TestHelpers.parseMultiPartRequest

import ch.ge.ve.bo.repository.ConnectedUserTestUtils
import ch.ge.ve.bo.repository.SecuredOperationRepository
import ch.ge.ve.bo.repository.dataset.OperationDataset
import ch.ge.ve.bo.service.TestHelpers
import ch.ge.ve.bo.service.electoralAuthorityKey.model.ElectoralAuthorityKeyVo
import ch.ge.ve.bo.service.model.PactConfiguration
import ch.ge.ve.bo.service.operation.model.ConfigurationStatusVo
import ch.ge.ve.bo.service.operation.model.OperationStatusVo
import ch.ge.ve.bo.service.operation.model.TallyArchiveStatusVo
import ch.ge.ve.bo.service.operation.model.VotingMaterialStatusVo
import ch.ge.ve.bo.service.operation.model.VotingPeriodStatusVo
import ch.ge.ve.bo.service.operation.status.OperationStatusService
import ch.ge.ve.bo.service.operation.voting.period.ElectoralAuthorityKeyConfigurationService
import ch.ge.ve.bo.service.operation.voting.period.VotingSitePeriodService
import ch.ge.ve.bo.service.operation.voting.period.model.VotingSitePeriodVO
import java.time.LocalDateTime
import org.apache.http.client.HttpClient
import org.apache.http.client.methods.HttpPost
import org.skyscreamer.jsonassert.JSONAssert
import org.skyscreamer.jsonassert.JSONCompareMode
import spock.lang.Specification

class OperationUploadServiceVotingPeriodHandlerTest extends Specification {
  HttpPost httpPost = null
  def httpClient = Mock(HttpClient)
  def operationStatusService = Mock(OperationStatusService)

  static def pactConfig = new PactConfiguration()
  def keyService = Mock(ElectoralAuthorityKeyConfigurationService)
  def periodService = Mock(VotingSitePeriodService)
  def operationRepository = Mock(SecuredOperationRepository)
  def service = new OperationUploadServiceVotingPeriodHandler(
          pactConfig, TestHelpers.getObjectMapper(), httpClient, operationStatusService, keyService, periodService, operationRepository)

  def cleanup() {
    ConnectedUserTestUtils.disconnectUser()
  }

  def setup() {
    ConnectedUserTestUtils.connectUser()
  }

  def setupSpec() {
    pactConfig.setUsername("user")
    pactConfig.setPassword("pass")
    pactConfig.url.votingPeriodUpload = "votingPeriodUploadUrl/{0,number,#}"
  }

  def "Should throw an exception if voting period is not ready to be deployed"() {
    given:
    def vmStatus = new VotingMaterialStatusVo(VotingMaterialStatusVo.State.CREATED, LocalDateTime.now(), [:], "user")
    def vpStatus = (state == null) ? null : new VotingPeriodStatusVo(state, [:], LocalDateTime.now(), "user", "pactUrl", null)
    def confStatus = new ConfigurationStatusVo.Builder()
            .setState(ConfigurationStatusVo.State.DEPLOYED, LocalDateTime.now(), 'USER')
            .build()

    operationStatusService.getStatus(1) >> new OperationStatusVo(confStatus, vmStatus, vpStatus, TallyArchiveStatusVo.notRequested(), null, REAL)
    operationRepository.findOne(1, true) >>
            OperationDataset.electoralOperation(new OperationDataset.Options().with(deploymentTarget, REAL))


    when:
    service.upload(1)

    then:
    thrown(exceptionToThrow)

    where:
    _ | state                                                   | exceptionToThrow
    _ | VotingPeriodStatusVo.State.INCOMPLETE                   | TryToUploadUncompleteOrInconsitentOperation
    _ | VotingPeriodStatusVo.State.AVAILABLE_FOR_INITIALIZATION | TryToUploadAnOperationWithIllegalState
    _ | VotingPeriodStatusVo.State.INITIALIZATION_REQUESTED     | TryToUploadAnOperationWithIllegalState
    _ | VotingPeriodStatusVo.State.INITIALIZATION_IN_PROGRESS   | TryToUploadAnOperationWithIllegalState
    _ | VotingPeriodStatusVo.State.INITIALIZED                  | TryToUploadAnOperationWithIllegalState
  }

  def "Should send configuration if voting period is ready to be deployed"() {
    given:
    def vmStatus = new VotingMaterialStatusVo(VotingMaterialStatusVo.State.CREATED, LocalDateTime.now(), [:], "user")
    def vpStatus = new VotingPeriodStatusVo(VotingPeriodStatusVo.State.COMPLETE, [:], LocalDateTime.now(), "user", "pactUrl", null)
    def confStatus = new ConfigurationStatusVo.Builder()
            .setState(ConfigurationStatusVo.State.DEPLOYED, LocalDateTime.now(), 'USER')
            .build()

    operationRepository.findOne(1, true) >>
            OperationDataset.electoralOperation(new OperationDataset.Options().with(deploymentTarget, REAL))
    operationStatusService.getStatus(1) >> new OperationStatusVo(confStatus, vmStatus, vpStatus, TallyArchiveStatusVo.notRequested(), null, REAL)
    keyService.findForOperation(1) >> Optional.of(new ElectoralAuthorityKeyVo(1, "test", LocalDateTime.now(), "ge", "key1", LocalDateTime.parse("2007-11-03T09:00:00"), "sampleHash"))
    keyService.getContentForOperation(1) >> "test".bytes

    VotingSitePeriodVO period = new VotingSitePeriodVO(LocalDateTime.parse("2007-12-03T09:15:30"), LocalDateTime.parse("2007-12-03T09:30:30"), 10)
    periodService.findForOperation(1) >> Optional.of(period)


    when:
    service.upload(1)

    then:
    1 * httpClient.execute({ httpPost = it }, _)
    httpPost.method == "POST"
    httpPost.getURI().toString() == "votingPeriodUploadUrl/1"
    ByteArrayOutputStream stream = new ByteArrayOutputStream()
    httpPost.entity.writeTo(stream)
    def parts = parseMultiPartRequest(stream.toString("UTF-8"))
    JSONAssert.assertEquals(getResourceString(getClass(), "votingPeriodRequestJson.json"), parts[0].content, JSONCompareMode.NON_EXTENSIBLE)

  }

}
