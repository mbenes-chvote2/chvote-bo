/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.dictionnary

import ch.ge.ve.bo.repository.DictionaryRepository
import ch.ge.ve.bo.repository.SecuredOperationRepository
import ch.ge.ve.bo.repository.entity.Dictionary
import ch.ge.ve.bo.repository.entity.DictionaryDataType
import ch.ge.ve.bo.service.TestHelpers
import spock.lang.Specification

class DictionaryServiceImplTest extends Specification {
  DictionaryRepository repository = Mock(DictionaryRepository)
  def operationRepository = Mock(SecuredOperationRepository)
  DictionaryServiceImpl service = new DictionaryServiceImpl(TestHelpers.getObjectMapper(), repository, operationRepository)

  def "get should return String value if needed"() {
    given:
    Dictionary dictionaryEntry = new Dictionary()
    dictionaryEntry.setContent("a simple string")
    repository.findAllByDataTypeAndScope(DictionaryDataType.CANDIDATE_INFORMATION_DISPLAY_MODELS, "realm") >>
            [dictionaryEntry]

    when:
    def values = service.get(DictionaryDataType.CANDIDATE_INFORMATION_DISPLAY_MODELS, String.class, "realm", "me")


    then:
    1 == values.size()
    "a simple string" == values[0]
  }

  def "get should return json encode value if needed"() {
    given:
    Dictionary dictionaryEntry = new Dictionary()
    dictionaryEntry.setContent('["s1","s2"]')
    repository.findAllByDataType(DictionaryDataType.COLUMN_FOR_VERIFICATION_CODE) >>
            [dictionaryEntry]

    when:
    def values = service.get(DictionaryDataType.COLUMN_FOR_VERIFICATION_CODE, String[].class, "realm", "me")


    then:
    1 == values.size()
    2 == values[0].size()
    "s1" == values[0][0]
    "s2" == values[0][1]
  }


}
