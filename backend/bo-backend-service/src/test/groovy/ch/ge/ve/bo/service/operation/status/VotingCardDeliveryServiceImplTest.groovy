/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.status

import ch.ge.ve.bo.service.exception.BusinessException
import ch.ge.ve.bo.service.exception.TechnicalException
import ch.ge.ve.bo.service.model.PactConfiguration
import ch.ge.ve.bo.service.model.printer.PrinterConfiguration
import ch.ge.ve.bo.service.operation.model.ConfigurationStatusVo
import ch.ge.ve.bo.service.operation.model.OperationStatusVo
import ch.ge.ve.bo.service.operation.model.VotingMaterialStatusVo
import ch.ge.ve.bo.service.printer.PrinterTemplateService
import java.nio.charset.Charset
import java.nio.file.Files
import spock.lang.Specification

class VotingCardDeliveryServiceImplTest extends Specification {
  def operationStatusService = Mock(OperationStatusService)
  def pactConfiguration = new PactConfiguration()
  def printerTemplateService = Mock(PrinterTemplateService)
  def service = new VotingCardDeliveryServiceImpl(operationStatusService,
          pactConfiguration,
          printerTemplateService)
  def status = Mock(OperationStatusVo)
  def confStatus = Mock(ConfigurationStatusVo)
  def vmStatus = Mock(VotingMaterialStatusVo)
  def printer = new PrinterConfiguration("printerID", "printerName", BigInteger.ONE, 10)

  void setup() {
    def tempDir = Files.createTempDirectory("test")
    pactConfiguration.votingMaterialBaseLocation = tempDir.toFile().absolutePath
    def tempFile = tempDir.resolve("file")
    Files.write(tempFile, "everything is awesome".getBytes(Charset.forName("UTF-8")))

    status.configurationStatus >> confStatus
    status.votingMaterialStatus >> vmStatus
    operationStatusService.getStatus(1) >> status
  }


  def "getGeneratedCardsForTestSite"() {
    given:
    confStatus.state >> ConfigurationStatusVo.State.IN_VALIDATION
    confStatus.generatedVotingCardLocation >> "file"

    when:
    def response = service.getGeneratedCardsForTestSite(1)

    then:
    new String(response.fileContent) == 'everything is awesome'
  }

  def "getGeneratedCardsForTestSite should failed if the configuration has not been sent"() {
    given:
    confStatus.state >> ConfigurationStatusVo.State.INCOMPLETE
    confStatus.generatedVotingCardLocation >> "file"

    when:
    service.getGeneratedCardsForTestSite(1)

    then:
    def exception = thrown(BusinessException)
    exception.messageKey == "operationSummary.configuration.errors.noTestSiteAvailable"
  }


  def "getGeneratedCardsForTestSite should failed the voting card location has not been sent"() {
    given:
    confStatus.state >> ConfigurationStatusVo.State.IN_VALIDATION

    when:
    service.getGeneratedCardsForTestSite(1)

    then:
    def exception = thrown(BusinessException)
    exception.messageKey == "operationSummary.configuration.errors.noVotingMaterialAvailable"
  }

  def "getGeneratedCardsForTestSite should failed if the file is actually not present"() {
    given:
    confStatus.state >> ConfigurationStatusVo.State.IN_VALIDATION
    confStatus.generatedVotingCardLocation >> "file2"

    when:
    service.getGeneratedCardsForTestSite(1)

    then:
    def exception = thrown(TechnicalException)
    exception.message =~ /^File .* does not exist$/
  }


  def "getGeneratedNonPrintableCardsForProduction"() {
    given:
    vmStatus.state >> VotingMaterialStatusVo.State.CREATED
    vmStatus.votingCardLocations >> ["printerID": "file"]

    printerTemplateService.getVirtualPrinterForTestCard() >> printer

    when:
    def response = service.getGeneratedNonPrintableCardsForProduction(1)

    then:
    new String(response.fileContent) == 'everything is awesome'
  }

  def "getGeneratedNonPrintableCardsForProduction should failed if coting material configuration has not been sent"() {
    given:
    vmStatus.state >> VotingMaterialStatusVo.State.COMPLETE
    vmStatus.votingCardLocations >> ["printerID": "file"]
    printerTemplateService.getVirtualPrinterForTestCard() >> printer

    when:
    service.getGeneratedNonPrintableCardsForProduction(1)

    then:
    def exception = thrown(BusinessException)
    exception.messageKey == "operationSummary.votingMaterial.errors.notCreated"
  }


  def "getGeneratedNonPrintableCardsForProduction should failed if there is no cards for virtual printer"() {
    given:
    vmStatus.state >> VotingMaterialStatusVo.State.CREATED
    vmStatus.votingCardLocations >> ["OtherPrinterID": "file"]
    printerTemplateService.getVirtualPrinterForTestCard() >> printer

    when:
    service.getGeneratedNonPrintableCardsForProduction(1)

    then:
    def exception = thrown(BusinessException)
    exception.messageKey == "operationSummary.votingMaterial.errors.noPrinterFileForThisPrinter"
  }

}
