/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.testing.card.impl

import static ch.ge.ve.bo.service.TestHelpers.getResourceString
import static ch.ge.ve.bo.service.TestHelpers.passThroughSecuredOperationHolderService

import ch.ge.ve.bo.repository.ConnectedUserTestUtils
import ch.ge.ve.bo.repository.SecuredOperationRepository
import ch.ge.ve.bo.repository.VoterTestingCardsLotRepository
import ch.ge.ve.bo.repository.dataset.OperationDataset
import ch.ge.ve.bo.repository.dataset.VoterTestingCardsLotDataset
import ch.ge.ve.bo.repository.entity.CardType
import ch.ge.ve.bo.repository.entity.Operation
import ch.ge.ve.bo.repository.entity.VoterTestingCardsLot
import ch.ge.ve.bo.service.model.printer.PrinterConfiguration
import ch.ge.ve.bo.service.model.printer.PrinterTemplateVo
import ch.ge.ve.bo.service.operation.parameters.doi.model.DomainOfInfluenceVo
import ch.ge.ve.bo.service.operation.parameters.repository.OperationRepositoryService
import ch.ge.ve.bo.service.printer.PrinterTemplateService
import java.time.Clock
import java.time.Instant
import java.time.ZoneOffset
import org.xmlunit.builder.DiffBuilder
import org.xmlunit.builder.Input
import spock.lang.Specification

class TestingCardRegisterGeneratorServiceImplTest extends Specification {

  def PRINTER_1 = new PrinterConfiguration("prn1", "printer 1", null, 1)
  def PRINTER_2 = new PrinterConfiguration("prn2", "printer 2", null, 2)
  def PRINTER_FOR_TEST = new PrinterConfiguration("prntest", "printer for test card", null, 9999)
  def PRINTER_TEMPLATE = new PrinterTemplateVo("templateName", [PRINTER_1, PRINTER_2] as PrinterConfiguration[], null, PRINTER_1.id, null)


  def lotRepository = Mock(VoterTestingCardsLotRepository)
  def opeRepositoryService = Mock(OperationRepositoryService)
  def printerTemplateService = Mock(PrinterTemplateService)
  def operationRepository = Mock(SecuredOperationRepository)
  def service = new TestingCardRegisterGeneratorServiceImpl(
          opeRepositoryService,
          lotRepository,
          operationRepository,
          passThroughSecuredOperationHolderService(),
          printerTemplateService,
          Clock.fixed(Instant.EPOCH, ZoneOffset.UTC))
  Operation operation


  def cleanup() {
    ConnectedUserTestUtils.disconnectUser()
  }

  def setup() {
    ConnectedUserTestUtils.connectUser()
    printerTemplateService.getVirtualPrinterForTestCard() >> PRINTER_FOR_TEST
    printerTemplateService.getPrinterTemplate(_ as String, _ as String) >> PRINTER_TEMPLATE
    operation = OperationDataset.electoralOperation()
    operationRepository.findOne(operation.id, true) >> operation
    opeRepositoryService.getAllDomainOfInfluenceForOperation(operation.id) >> [doi("doi1"), doi("doi2")]

  }

  def "generateForOperation should create card for test site testing card"() {
    given: "2 lots to generate"
    VoterTestingCardsLot lot1 = VoterTestingCardsLotDataset.voterTestingCardsLot(CardType.TEST_SITE_TESTING_CARD)
    lot1.setDoi(["doi1", "doi2"])
    lot1.setLotName("lot 1")
    lot1.setId(1)
    lot1.setShouldPrint(false)

    VoterTestingCardsLot lot2 = VoterTestingCardsLotDataset.voterTestingCardsLot(CardType.TEST_SITE_TESTING_CARD)
    lot2.setDoi(["doi1"])
    lot2.setLotName("lot 2")
    lot2.setId(2)
    lot2.setShouldPrint(false)
    lotRepository.findAllByOperationIdAndCardTypeInOrderById(operation.id, CardType.forConfiguration()) >> [lot1, lot2]

    when:
    Map<VoterTestingCardsLot, Map<String, String>> xmls = service.generateForOperation(1, true)

    then:
    2 == xmls.size()

    !DiffBuilder.compare(Input.fromString(xmls.get(lot1).get('prntest')))
            .withTest(getResourceString(TestingCardRegisterGeneratorServiceImplTest, "TEST_SITE_TESTING_CARD_lot1_ech0045.xml"))
            .ignoreWhitespace()
            .normalizeWhitespace()
            .ignoreComments()
            .checkForSimilar()
            .build().hasDifferences()

    !DiffBuilder.compare(Input.fromString(xmls.get(lot2).get('prntest')))
            .withTest(getResourceString(TestingCardRegisterGeneratorServiceImplTest, "TEST_SITE_TESTING_CARD_lot2_ech0045.xml"))
            .ignoreWhitespace()
            .normalizeWhitespace()
            .ignoreComments()
            .checkForSimilar()
            .build().hasDifferences()
  }


  def "generateForOperation should create card for printer testing card"() {
    given:
    VoterTestingCardsLot lot1 = VoterTestingCardsLotDataset.voterTestingCardsLot(CardType.PRINTER_TESTING_CARD)
    lot1.setDoi(["doi1"])
    lot1.setLotName("lot 1")
    lot1.setId(1)
    lot1.setShouldPrint(true)
    lotRepository.findAllByOperationIdAndCardTypeInOrderById(operation.id, CardType.forVotingMaterial()) >> [lot1]

    when:
    Map<VoterTestingCardsLot, Map<String, String>> xmls = service.generateForOperation(1, false)

    then:
    1 == xmls.size()

    !DiffBuilder.compare(Input.fromString(xmls.get(lot1).get('prn1')))
            .withTest(getResourceString(TestingCardRegisterGeneratorServiceImplTest, "PRINTER_TESTING_CARD_lot1_ech0045.xml"))
            .ignoreWhitespace()
            .normalizeWhitespace()
            .ignoreComments()
            .checkForSimilar()
            .build().hasDifferences()

    !DiffBuilder.compare(Input.fromString(xmls.get(lot1).get('prn2')))
            .withTest(getResourceString(TestingCardRegisterGeneratorServiceImplTest, "PRINTER_TESTING_CARD_lot2_ech0045.xml"))
            .ignoreWhitespace()
            .normalizeWhitespace()
            .ignoreComments()
            .checkForSimilar()
            .build().hasDifferences()
  }

  def "generateForOperation should create card for controller testing card"() {
    given:
    VoterTestingCardsLot lot1 = VoterTestingCardsLotDataset.voterTestingCardsLot(CardType.CONTROLLER_TESTING_CARD)
    lot1.setDoi(["doi1"])
    lot1.setLotName("lot 1")
    lot1.setId(1)
    lot1.setShouldPrint(true)
    lotRepository.findAllByOperationIdAndCardTypeInOrderById(operation.id, CardType.forVotingMaterial()) >> [lot1]

    when:
    Map<VoterTestingCardsLot, Map<String, String>> xmls = service.generateForOperation(1, false)

    then:
    1 == xmls.size()

    !DiffBuilder.compare(Input.fromString(xmls.get(lot1).get('prn1')))
            .withTest(getResourceString(TestingCardRegisterGeneratorServiceImplTest, "CONTROLLER_TESTING_CARD_lot1_ech0045.xml"))
            .ignoreWhitespace()
            .normalizeWhitespace()
            .ignoreComments()
            .checkForSimilar()
            .build().hasDifferences()
  }


  def "generateForOperation should create card for printable production testing card"() {
    given:
    VoterTestingCardsLot lot1 = VoterTestingCardsLotDataset.voterTestingCardsLot(CardType.PRODUCTION_TESTING_CARD)
    lot1.setDoi(["doi1"])
    lot1.setLotName("lot 1")
    lot1.setId(1)
    lot1.setShouldPrint(true)
    lotRepository.findAllByOperationIdAndCardTypeInOrderById(operation.id, CardType.forVotingMaterial()) >> [lot1]

    when:
    Map<VoterTestingCardsLot, Map<String, String>> xmls = service.generateForOperation(1, false)

    then:
    1 == xmls.size()

    !DiffBuilder.compare(Input.fromString(xmls.get(lot1).get('prn1')))
            .withTest(getResourceString(TestingCardRegisterGeneratorServiceImplTest, "PRINTABLE_PRODUCTION_TESTING_CARD_lot1_ech0045.xml"))
            .ignoreWhitespace()
            .normalizeWhitespace()
            .ignoreComments()
            .checkForSimilar()
            .build().hasDifferences()
  }

  def "generateForOperation should create card for not printable production testing card"() {
    given:
    VoterTestingCardsLot lot1 = VoterTestingCardsLotDataset.voterTestingCardsLot(CardType.PRODUCTION_TESTING_CARD)
    lot1.setDoi(["doi1"])
    lot1.setLotName("lot 1")
    lot1.setId(1)
    lot1.setShouldPrint(false)
    lotRepository.findAllByOperationIdAndCardTypeInOrderById(operation.id, CardType.forVotingMaterial()) >> [lot1]

    when:
    Map<VoterTestingCardsLot, Map<String, String>> xmls = service.generateForOperation(1, false)

    then:
    1 == xmls.size()

    !DiffBuilder.compare(Input.fromString(xmls.get(lot1).get('prntest')))
            .withTest(getResourceString(TestingCardRegisterGeneratorServiceImplTest, "NOT_PRINTABLE_PRODUCTION_TESTING_CARD_lot1_ech0045.xml"))
            .ignoreWhitespace()
            .normalizeWhitespace()
            .ignoreComments()
            .checkForSimilar()
            .build().hasDifferences()
  }


  DomainOfInfluenceVo doi(String id) {
    return new DomainOfInfluenceVo("CH", id, "longName " + id, id)
  }
}
