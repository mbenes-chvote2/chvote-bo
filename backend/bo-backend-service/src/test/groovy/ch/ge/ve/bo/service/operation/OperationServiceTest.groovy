/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation

import static ch.ge.ve.bo.MilestoneType.BALLOT_BOX_DECRYPT
import static ch.ge.ve.bo.MilestoneType.BALLOT_BOX_INIT
import static ch.ge.ve.bo.MilestoneType.CERTIFICATION
import static ch.ge.ve.bo.MilestoneType.DATA_DESTRUCTION
import static ch.ge.ve.bo.MilestoneType.PRINTER_FILES
import static ch.ge.ve.bo.MilestoneType.RESULT_VALIDATION
import static ch.ge.ve.bo.MilestoneType.SITE_CLOSE
import static ch.ge.ve.bo.MilestoneType.SITE_OPEN
import static ch.ge.ve.bo.MilestoneType.SITE_VALIDATION
import static ch.ge.ve.bo.repository.dataset.DatasetTools.localDateTime
import static ch.ge.ve.bo.repository.dataset.OperationDataset.Options
import static ch.ge.ve.bo.repository.dataset.OperationDataset.electoralOperation
import static ch.ge.ve.bo.repository.dataset.OperationDataset.votingOperation
import static ch.ge.ve.bo.service.dataset.OperationVoDataset.electoralOperationVo
import static ch.ge.ve.bo.service.dataset.VotationRepositoryFileVoDataset.votationRepositoryFileVo

import ch.ge.ve.bo.FileType
import ch.ge.ve.bo.MilestoneType
import ch.ge.ve.bo.repository.ConnectedUserTestUtils
import ch.ge.ve.bo.repository.OperationConsistencyRepository
import ch.ge.ve.bo.repository.SecuredOperationRepository
import ch.ge.ve.bo.repository.entity.DeploymentTarget
import ch.ge.ve.bo.repository.entity.Operation
import ch.ge.ve.bo.service.file.FileService
import ch.ge.ve.bo.service.operation.consistency.ConsistencyScheduleService
import ch.ge.ve.bo.service.operation.exception.CannotTargetSimulation
import ch.ge.ve.bo.service.operation.exception.InvalidMilestoneDatesException
import ch.ge.ve.bo.service.operation.exception.LockedDateException
import ch.ge.ve.bo.service.operation.exception.PastDateException
import ch.ge.ve.bo.service.operation.exception.TryToModifyReadonlyPropertyException
import ch.ge.ve.bo.service.operation.model.BaseConfiguration
import ch.ge.ve.bo.service.operation.model.MilestoneConfiguration
import ch.ge.ve.bo.service.operation.model.MilestoneVo
import ch.ge.ve.bo.service.operation.status.OperationStatusService
import ch.ge.ve.bo.service.testing.card.VoterTestingCardsLotService
import com.fasterxml.jackson.databind.ObjectMapper
import java.time.LocalDateTime
import spock.lang.Specification
import spock.lang.Unroll

/**
 * Unit tests for the operation services ({@link ch.ge.ve.bo.service.operation.OperationService}).
 */
class OperationServiceTest extends Specification {


  def securedOperationRepositoryMock = Mock(SecuredOperationRepository)
  def operationConsistencyRepositoryMock = Mock(OperationConsistencyRepository)
  def fileServiceMock = Mock(FileService)
  def statusServiceMock = Mock(OperationStatusService)
  def voterTestingCardsLotService = Mock(VoterTestingCardsLotService)
  def consistencyScheduleService = Mock(ConsistencyScheduleService)
  def mapper = new ObjectMapper()

  void cleanup() {
    ConnectedUserTestUtils.disconnectUser()
  }


  OperationService operationService = new OperationServiceImpl(securedOperationRepositoryMock, operationConsistencyRepositoryMock, voterTestingCardsLotService,
          fileServiceMock, statusServiceMock, consistencyScheduleService, mapper)

  def 'findAll should return all the available operations for the given management entity'() {
    given: 'a repository returning two operations'
    def managementEntity = "managementEntity"
    ConnectedUserTestUtils.connectUser("test", "GE", managementEntity)

    securedOperationRepositoryMock.findAllByManagementEntity(managementEntity) >> [electoralOperation(), votingOperation()]

    when: 'calling the findAll service'
    def operations = operationService.findAll()

    then: 'two operations value objects should be returned'
    operations.shortLabel == ["EL042050", "VP052055"]
  }

  def 'create should create a new electoral operation'() {
    given: 'an operation to create'
    def baseConfiguration = new BaseConfiguration("short", "long", LocalDateTime.now().plusDays(1))

    when: 'calling the create service'
    def operationId = operationService.create(baseConfiguration)

    then: 'operation should be saved in the repository'
    1 * securedOperationRepositoryMock.save(_) >> electoralOperation()

    and: 'operation\'s ID should be returned'
    operationId == 1
  }

  def 'create an operation in the past should throw an exception'() {
    given: 'an electoral operation in the past'
    def baseConfiguration = new BaseConfiguration("short", "long", localDateTime('01.01.2010 00:00:00'))


    when: 'calling the create service'
    operationService.create(baseConfiguration)

    then: 'an exception is thrown'
    0 * securedOperationRepositoryMock.save(_)
    thrown(PastDateException)
  }

  def 'update without milestone should only update operation base parameters'() {
    given: 'an operation'
    def operationVo = electoralOperationVo(new Options()
            .with(Options.withMilestone, false))


    when: 'calling the update service'
    operationService.updateMilestones(operationVo.id, new MilestoneConfiguration(operationVo.gracePeriod, operationVo.milestones))


    then: 'operation should be modified'
    1 * securedOperationRepositoryMock.findOne(operationVo.id, true) >> electoralOperation()
    1 * securedOperationRepositoryMock.save(_) >> electoralOperation()
    1 * consistencyScheduleService.scheduleCheck(operationVo.id)
  }

  def 'update an operation with a date in the past should throw an exception'() {
    given: 'an operation'
    def baseConfiguration = new BaseConfiguration("short", "long", localDateTime('01.01.2010 00:00:00'))
    def operation = electoralOperation(
            new Options()
                    .with(Options.withMilestone, false)
                    .with(Options.operationDate, localDateTime('01.01.2056 00:00:00')))


    when: 'calling the update service'
    operationService.updateBaseConfiguration(operation.id, baseConfiguration)

    then: 'an exception is thrown'
    1 * securedOperationRepositoryMock.findOne(operation.id, true) >> operation
    0 * securedOperationRepositoryMock.save(_)
    0 * consistencyScheduleService.scheduleCheck(operation.id)
    thrown(PastDateException)
  }

  def 'update an operation date while having milestones defined should throw an exception'() {
    given: 'an operation with milestones'
    def operation = electoralOperation(
            new Options()
                    .with(Options.withMilestone, true)
                    .with(Options.operationDate, localDateTime('01.01.2056 00:00:00')))

    def baseConfiguration = new BaseConfiguration("short", "long", LocalDateTime.now().plusDays(1))

    when: 'calling the update service'
    operationService.updateBaseConfiguration(operation.id, baseConfiguration)

    then: 'an exception is thrown'
    1 * securedOperationRepositoryMock.findOne(operation.id, true) >> operation
    0 * securedOperationRepositoryMock.save(_)
    0 * consistencyScheduleService.scheduleCheck(operation.id)
    thrown(LockedDateException)
  }

  def 'update an operation date while having repository files defined should throw an exception'() {
    given: 'an operation with repository files'
    def operationVo = electoralOperationVo(new Options()
            .with(Options.withMilestone, false)
            .with(Options.operationDate, localDateTime('01.01.2056 00:00:00')))
    def baseConfiguration = new BaseConfiguration("short", "long", LocalDateTime.now().plusDays(1))

    when: 'calling the update service'
    operationService.updateBaseConfiguration(operationVo.id, baseConfiguration)

    then: 'an exception is thrown'
    1 * securedOperationRepositoryMock.findOne(operationVo.id, true) >>
            electoralOperation(new Options().with(Options.withMilestone, false))
    1 * fileServiceMock.getFiles(operationVo.id, false, EnumSet.of(FileType.VOTATION_REPOSITORY, FileType.ELECTION_REPOSITORY)) >>
            [votationRepositoryFileVo()]
    0 * securedOperationRepositoryMock.save(_)
    0 * consistencyScheduleService.scheduleCheck(operationVo.id)
    thrown(LockedDateException)
  }

  def 'update with new milestones should update operation with the milestones'() {
    given: 'a milestone config'
    MilestoneConfiguration milestoneConfig = new MilestoneConfiguration(9, [
            new MilestoneVo(0, null, null, SITE_VALIDATION, localDateTime('11.02.2050 00:00:00')),
            new MilestoneVo(0, null, null, CERTIFICATION, localDateTime('27.02.2050 00:00:00')),
            new MilestoneVo(0, null, null, PRINTER_FILES, localDateTime('04.03.2050 00:00:00')),
            new MilestoneVo(0, null, null, BALLOT_BOX_INIT, localDateTime('20.03.2050 00:00:00')),
            new MilestoneVo(0, null, null, SITE_OPEN, localDateTime('23.03.2050 12:00:00')),
            new MilestoneVo(0, null, null, SITE_CLOSE, localDateTime('18.04.2050 12:00:00')),
            new MilestoneVo(0, null, null, BALLOT_BOX_DECRYPT, localDateTime('19.04.2050 00:00:00')),
            new MilestoneVo(0, null, null, RESULT_VALIDATION, localDateTime('21.04.2050 00:00:00')),
            new MilestoneVo(0, null, null, DATA_DESTRUCTION, localDateTime('18.07.2050 00:00:00'))
    ])

    when: 'calling the update service'

    operationService.updateMilestones(1, milestoneConfig)

    then: 'operation should be modified'
    1 * securedOperationRepositoryMock.findOne(1, true) >> electoralOperation(new Options().with(Options.withMilestone, false))
    1 * securedOperationRepositoryMock.save(_) >> electoralOperation()
    1 * consistencyScheduleService.scheduleCheck(1)
  }

  def 'update with invalid milestone dates should throw an exception'() {
    given: 'an operation'
    MilestoneConfiguration milestoneConfig = new MilestoneConfiguration(9, [
            new MilestoneVo(0, null, null, SITE_VALIDATION, localDateTime('11.02.2015 00:00:00')),
            new MilestoneVo(0, null, null, CERTIFICATION, localDateTime('27.02.2015 00:00:00')),
            new MilestoneVo(0, null, null, PRINTER_FILES, localDateTime('04.03.2015 00:00:00')),
            new MilestoneVo(0, null, null, BALLOT_BOX_INIT, localDateTime('20.03.2015 00:00:00')),
            new MilestoneVo(0, null, null, SITE_OPEN, localDateTime('23.03.2015 12:00:00')),
            new MilestoneVo(0, null, null, SITE_CLOSE, localDateTime('18.04.2015 12:00:00')),
            new MilestoneVo(0, null, null, BALLOT_BOX_DECRYPT, localDateTime('18.04.2015 00:00:00')),
            new MilestoneVo(0, null, null, RESULT_VALIDATION, localDateTime('21.04.2015 00:00:00')),
            new MilestoneVo(0, null, null, DATA_DESTRUCTION, localDateTime('18.07.2015 00:00:00'))
    ])

    when: 'calling the update service'
    operationService.updateMilestones(1, milestoneConfig)

    then: 'operation should be modified'
    1 * securedOperationRepositoryMock.findOne(1, true) >> electoralOperation(new Options().with(Options.withMilestone, false))
    0 * securedOperationRepositoryMock.save(_)
    0 * consistencyScheduleService.scheduleCheck(1)
    thrown(InvalidMilestoneDatesException)
  }

  def 'update with existing milestones should update operation with the milestones'() {
    given: 'an operation'
    def operationVo = electoralOperationVo()

    when: 'calling the update service'
    operationService.updateMilestones(operationVo.id, new MilestoneConfiguration(operationVo.gracePeriod, operationVo.milestones))

    then: 'operation should be modified'
    1 * securedOperationRepositoryMock.findOne(operationVo.id, true) >> electoralOperation()
    1 * securedOperationRepositoryMock.save(_) >> electoralOperation()
    1 * consistencyScheduleService.scheduleCheck(operationVo.id)
  }


  def 'update while status is readonly should throw an exception'() {
    given: 'an operation'
    def operationVo = electoralOperationVo()
    statusServiceMock.isConfigurationInReadonly(_) >> true
    securedOperationRepositoryMock.findOne(operationVo.id, _) >> electoralOperation()

    when: 'calling the update service'
    operationService.updateMilestones(operationVo.id, new MilestoneConfiguration(operationVo.gracePeriod, operationVo.milestones))


    then: 'operation should be modified'
    0 * securedOperationRepositoryMock.save(_) >> electoralOperation()
    0 * consistencyScheduleService.scheduleCheck(operationVo.id)
    thrown TryToModifyReadonlyPropertyException
  }


  def 'findOne should return the operation corresponding to the given ID'() {
    when: 'calling the findOne(1L)'
    def operation = operationService.findOne(1L, true)

    then: 'securedOperationRepository.findOne(1L) is called once'
    1 * securedOperationRepositoryMock.findOne(1L, true) >> electoralOperation()

    and: 'the corresponding operation object is returned'
    operation.milestones == electoralOperationVo().milestones
    operation.shortLabel == "EL042050"
  }

  @Unroll
  def 'alignMilestoneTime(#milestoneType, #milestoneDate) should return #expectedDate'() {
    given:
    def milestone = new MilestoneVo(0, null, null, milestoneType, localDateTime(milestoneDate))

    when:
    def alignedMilestone = (operationService as OperationServiceImpl).alignMilestoneTime(milestone)

    then:
    alignedMilestone.date == localDateTime(expectedDate)

    where:
    milestoneType      | milestoneDate         || expectedDate
    SITE_VALIDATION    | '14.06.2010 15:23:58' || '14.06.2010 00:00:00'
    CERTIFICATION      | '14.06.2010 15:23:58' || '14.06.2010 00:00:00'
    PRINTER_FILES      | '14.06.2010 15:23:58' || '14.06.2010 00:00:00'
    BALLOT_BOX_INIT    | '14.06.2010 15:23:58' || '14.06.2010 00:00:00'
    SITE_OPEN          | '14.06.2010 15:23:58' || '14.06.2010 15:23:00'
    SITE_CLOSE         | '14.06.2010 15:23:58' || '14.06.2010 15:23:00'
    BALLOT_BOX_DECRYPT | '14.06.2010 15:23:58' || '14.06.2010 15:23:00'
    RESULT_VALIDATION  | '14.06.2010 15:23:58' || '14.06.2010 00:00:00'
    DATA_DESTRUCTION   | '14.06.2010 15:23:58' || '14.06.2010 00:00:00'
  }

  @Unroll
  def 'check site validation milestone dates (operation = #operationDate, siteValidation = #milestoneDate)'() {
    given:
    def milestone = new MilestoneVo(0, null, null, SITE_VALIDATION, localDateTime(milestoneDate))
    def operation = localDateTime(operationDate)

    when:
    def error = (operationService as OperationServiceImpl).checkDateValidity(operation, 9, new HashMap<MilestoneType, LocalDateTime>(), milestone)

    then:
    error == expectedError

    where:
    operationDate         | milestoneDate         || expectedError
    '10.11.2055 00:00:00' | '01.11.2055 00:00:00' || null
    '10.11.2055 00:00:00' | '15.11.2055 00:00:00' || InvalidMilestoneDatesException.BEFORE_OPERATION
    '10.11.2055 00:00:00' | '10.11.2055 00:00:00' || InvalidMilestoneDatesException.BEFORE_OPERATION
    '10.11.2055 00:00:00' | '10.11.2015 00:00:00' || InvalidMilestoneDatesException.IN_FUTURE
  }

  @Unroll
  def 'check certification milestone dates (operation = #operationDate, siteValidation = #siteValidationDate, certification = #milestoneDate)'() {
    given:
    def milestone = new MilestoneVo(0, null, null, CERTIFICATION, localDateTime(milestoneDate))
    def operation = localDateTime(operationDate)

    def milestoneDates = new HashMap()
    milestoneDates.put(SITE_VALIDATION, localDateTime(siteValidationDate))

    when:
    def error = (operationService as OperationServiceImpl).checkDateValidity(operation, 9, milestoneDates, milestone)

    then:
    error == expectedError

    where:
    operationDate         | siteValidationDate    | milestoneDate         || expectedError
    '01.01.2057 00:00:00' | '10.12.2056 00:00:00' | '15.12.2056 00:00:00' || null
    '01.01.2057 00:00:00' | '10.12.2056 00:00:00' | '10.01.2057 00:00:00' || InvalidMilestoneDatesException.BEFORE_OPERATION
    '01.01.2057 00:00:00' | '10.12.2056 00:00:00' | '01.01.2057 00:00:00' || InvalidMilestoneDatesException.BEFORE_OPERATION
    '01.01.2057 00:00:00' | '10.12.2056 00:00:00' | '05.12.2056 00:00:00' || InvalidMilestoneDatesException.CERTIFICATION_DATE
    '01.01.2057 00:00:00' | '10.12.2056 00:00:00' | '10.12.2056 00:00:00' || null
    '10.11.2055 00:00:00' | '10.11.2015 00:00:00' | '10.11.2015 00:00:00' || InvalidMilestoneDatesException.IN_FUTURE
  }

  @Unroll
  def 'check printer files milestone dates (operation = #operationDate, certification = #certificationDate, certification = #milestoneDate)'() {
    given:
    def milestone = new MilestoneVo(0, null, null, PRINTER_FILES, localDateTime(milestoneDate))
    def operation = localDateTime(operationDate)

    def milestoneDates = new HashMap()
    milestoneDates.put(CERTIFICATION, localDateTime(certificationDate))

    when:
    def error = (operationService as OperationServiceImpl).checkDateValidity(operation, 9, milestoneDates, milestone)

    then:
    error == expectedError

    where:
    operationDate         | certificationDate     | milestoneDate         || expectedError
    '31.03.2057 00:00:00' | '10.02.2057 00:00:00' | '20.03.2057 00:00:00' || null
    '31.03.2057 00:00:00' | '10.02.2057 00:00:00' | '01.04.2057 00:00:00' || InvalidMilestoneDatesException.BEFORE_OPERATION
    '31.03.2057 00:00:00' | '10.02.2057 00:00:00' | '31.03.2057 00:00:00' || InvalidMilestoneDatesException.BEFORE_OPERATION
    '31.03.2057 00:00:00' | '10.02.2057 00:00:00' | '01.02.2057 00:00:00' || InvalidMilestoneDatesException.PRINTER_FILE_DATE
    '31.03.2057 00:00:00' | '10.02.2057 00:00:00' | '10.02.2057 00:00:00' || null
    '10.11.2055 00:00:00' | '10.11.2015 00:00:00' | '10.11.2015 00:00:00' || InvalidMilestoneDatesException.IN_FUTURE
  }

  @Unroll
  def 'check ballot box initialisation milestone dates (operation = #operationDate, printerFiles = #printerFilesDate, certification = #milestoneDate)'() {
    given:
    def milestone = new MilestoneVo(0, null, null, BALLOT_BOX_INIT, localDateTime(milestoneDate))
    def operation = localDateTime(operationDate)

    def milestoneDates = new HashMap()
    milestoneDates.put(PRINTER_FILES, localDateTime(printerFilesDate))

    when:
    def error = (operationService as OperationServiceImpl).checkDateValidity(operation, 9, milestoneDates, milestone)

    then:
    error == expectedError

    where:
    operationDate         | printerFilesDate      | milestoneDate         || expectedError
    '30.09.2057 00:00:00' | '15.09.2057 00:00:00' | '20.09.2057 00:00:00' || null
    '30.09.2057 00:00:00' | '15.09.2057 00:00:00' | '30.09.2057 00:00:00' || InvalidMilestoneDatesException.BEFORE_OPERATION
    '30.09.2057 00:00:00' | '15.09.2057 00:00:00' | '10.09.2057 00:00:00' || InvalidMilestoneDatesException.BALLOT_BOX_INIT_DATE
    '30.09.2057 00:00:00' | '15.09.2057 00:00:00' | '15.09.2057 00:00:00' || null
    '10.11.2055 00:00:00' | '10.11.2015 00:00:00' | '10.11.2015 00:00:00' || InvalidMilestoneDatesException.IN_FUTURE
  }

  @Unroll
  def 'check site open milestone dates (operation = #operationDate, ballotBoxInit = #ballotBoxInitDate, certification = #milestoneDate)'() {
    given:
    def milestone = new MilestoneVo(0, null, null, SITE_OPEN, localDateTime(milestoneDate))
    def operation = localDateTime(operationDate)

    def milestoneDates = new HashMap()
    milestoneDates.put(BALLOT_BOX_INIT, localDateTime(ballotBoxInitDate))

    when:
    def error = (operationService as OperationServiceImpl).checkDateValidity(operation, 9, milestoneDates, milestone)

    then:
    error == expectedError

    where:
    operationDate         | ballotBoxInitDate     | milestoneDate         || expectedError
    '15.06.2057 00:00:00' | '01.06.2057 00:00:00' | '14.06.2057 14:00:00' || null
    '15.06.2057 00:00:00' | '01.06.2057 00:00:00' | '16.06.2057 14:00:00' || InvalidMilestoneDatesException.BEFORE_OPERATION
    '15.06.2057 00:00:00' | '01.06.2057 00:00:00' | '15.06.2057 12:00:00' || InvalidMilestoneDatesException.BEFORE_OPERATION
    '15.06.2057 00:00:00' | '01.06.2057 00:00:00' | '31.05.2057 00:00:00' || InvalidMilestoneDatesException.SITE_OPEN_DATE
    '15.06.2057 00:00:00' | '01.06.2057 00:00:00' | '01.06.2057 10:00:00' || null
    '10.11.2055 00:00:00' | '10.11.2015 00:00:00' | '10.11.2015 00:00:00' || InvalidMilestoneDatesException.IN_FUTURE
  }

  @Unroll
  def 'check site close milestone dates (operation = #operationDate, siteOpen = #siteOpenDate, certification = #milestoneDate)'() {
    given:
    def milestone = new MilestoneVo(0, null, null, SITE_CLOSE, localDateTime(milestoneDate))
    def operation = localDateTime(operationDate)

    def milestoneDates = new HashMap()
    milestoneDates.put(SITE_OPEN, localDateTime(siteOpenDate))

    when:
    def error = (operationService as OperationServiceImpl).checkDateValidity(operation, 9, milestoneDates, milestone)

    then:
    error == expectedError

    where:
    operationDate         | siteOpenDate          | milestoneDate         || expectedError
    '10.08.2057 00:00:00' | '31.07.2057 10:00:00' | '01.08.2057 14:00:00' || null
    '10.08.2057 00:00:00' | '31.07.2057 10:00:00' | '10.08.2057 12:00:00' || null
    '10.08.2057 00:00:00' | '31.07.2057 10:00:00' | '15.08.2057 12:00:00' || InvalidMilestoneDatesException.BEFORE_OR_EQUAL_OPERATION
    '10.08.2057 00:00:00' | '31.07.2057 10:00:00' | '31.07.2057 00:00:00' || InvalidMilestoneDatesException.SITE_CLOSE_DATE
    '10.08.2057 00:00:00' | '31.07.2057 10:00:00' | '31.07.2057 10:00:00' || InvalidMilestoneDatesException.SITE_CLOSE_DATE
    '10.11.2055 00:00:00' | '10.11.2015 00:00:00' | '10.11.2015 00:00:00' || InvalidMilestoneDatesException.IN_FUTURE
  }

  @Unroll
  def 'check ballot box decryption milestone dates (operation = #operationDate, siteClose = #siteCloseDate, certification = #milestoneDate)'() {
    given:
    def milestone = new MilestoneVo(0, null, null, BALLOT_BOX_DECRYPT, localDateTime(milestoneDate))
    def operation = localDateTime(operationDate)

    def milestoneDates = new HashMap()
    milestoneDates.put(SITE_CLOSE, localDateTime(siteCloseDate))

    when:
    def error = (operationService as OperationServiceImpl).checkDateValidity(operation, 9, milestoneDates, milestone)

    then:
    error == expectedError

    where:
    operationDate         | siteCloseDate         | milestoneDate         || expectedError
    '10.08.2057 00:00:00' | '31.07.2057 10:00:00' | '01.08.2057 00:00:00' || null
    '10.08.2057 00:00:00' | '31.07.2057 10:00:00' | '10.08.2057 00:00:00' || null
    '10.08.2057 00:00:00' | '31.07.2057 10:00:00' | '15.08.2057 00:00:00' || InvalidMilestoneDatesException.BEFORE_OR_EQUAL_OPERATION
    '10.08.2057 00:00:00' | '31.07.2057 10:00:00' | '31.07.2057 00:00:00' || InvalidMilestoneDatesException.BALLOT_BOX_DECRYPT_DATE
    '10.08.2057 00:00:00' | '31.07.2057 10:00:00' | '31.07.2057 10:09:00' || null
    '10.08.2057 00:00:00' | '31.07.2057 10:00:00' | '31.07.2057 10:10:00' || null
    '10.11.2055 00:00:00' | '10.11.2015 00:00:00' | '10.11.2015 00:00:00' || InvalidMilestoneDatesException.IN_FUTURE
  }

  @Unroll
  def 'check result validation milestone dates (operation = #operationDate, ballotBoxDecrypt = #ballotBoxDecryptDate, certification = #milestoneDate)'() {
    given:
    def milestone = new MilestoneVo(0, null, null, RESULT_VALIDATION, localDateTime(milestoneDate))
    def operation = localDateTime(operationDate)

    def milestoneDates = new HashMap()
    milestoneDates.put(BALLOT_BOX_DECRYPT, localDateTime(ballotBoxDecryptDate))

    when:
    def error = (operationService as OperationServiceImpl).checkDateValidity(operation, 9, milestoneDates, milestone)

    then:
    error == expectedError

    where:
    operationDate         | ballotBoxDecryptDate  | milestoneDate         || expectedError
    '01.12.2057 00:00:00' | '10.11.2057 00:00:00' | '05.12.2057 00:00:00' || null
    '01.12.2057 00:00:00' | '10.11.2057 00:00:00' | '01.12.2057 00:00:00' || null
    '01.12.2057 00:00:00' | '10.11.2057 00:00:00' | '30.11.2057 00:00:00' || InvalidMilestoneDatesException.AFTER_OR_EQUAL_OPERATION
    '01.12.2057 00:00:00' | '10.12.2057 00:00:00' | '05.12.2057 00:00:00' || InvalidMilestoneDatesException.RESULT_VALIDATION_DATE
    '01.12.2057 00:00:00' | '10.12.2057 00:00:00' | '10.12.2057 00:00:00' || null
    '10.11.2055 00:00:00' | '10.11.2015 00:00:00' | '10.11.2015 00:00:00' || InvalidMilestoneDatesException.IN_FUTURE
  }

  @Unroll
  def 'check data destruction milestone dates (operation = #operationDate, resultValidation = #resultValidationDate, certification = #milestoneDate)'() {
    given:
    def milestone = new MilestoneVo(0, null, null, DATA_DESTRUCTION, localDateTime(milestoneDate))
    def operation = localDateTime(operationDate)

    def milestoneDates = new HashMap()
    milestoneDates.put(RESULT_VALIDATION, localDateTime(resultValidationDate))

    when:
    def error = (operationService as OperationServiceImpl).checkDateValidity(operation, 9, milestoneDates, milestone)

    then:
    error == expectedError

    where:
    operationDate         | resultValidationDate  | milestoneDate         || expectedError
    '01.12.2057 00:00:00' | '10.11.2057 00:00:00' | '05.12.2057 00:00:00' || null
    '01.12.2057 00:00:00' | '10.11.2057 00:00:00' | '01.12.2057 00:00:00' || InvalidMilestoneDatesException.AFTER_OPERATION
    '01.12.2057 00:00:00' | '10.11.2057 00:00:00' | '30.11.2057 00:00:00' || InvalidMilestoneDatesException.AFTER_OPERATION
    '01.12.2057 00:00:00' | '10.12.2057 00:00:00' | '05.12.2057 00:00:00' || InvalidMilestoneDatesException.DATA_DESTRUCTION_DATE
    '01.12.2057 00:00:00' | '10.12.2057 00:00:00' | '10.12.2057 00:00:00' || InvalidMilestoneDatesException.DATA_DESTRUCTION_DATE
    '10.11.2055 00:00:00' | '10.11.2015 00:00:00' | '10.11.2015 00:00:00' || InvalidMilestoneDatesException.IN_FUTURE
  }

  @Unroll
  def 'can target production if previous status is NOT_DEFINED and copy testing cards'() {
    given:
    def operation = electoralOperation(new Options().with(Options.deploymentTarget, DeploymentTarget.NOT_DEFINED))
    securedOperationRepositoryMock.findOne(1, true) >> operation

    when:
    operationService.targetReal(1)

    then:
    1 * voterTestingCardsLotService.copyTestingCardFromTestSiteToProduction(1)
    1 * securedOperationRepositoryMock.save({ Operation ope -> ope.deploymentTarget == DeploymentTarget.REAL }) >> operation
  }

  @Unroll
  def "can target production if previous status is SIMULATION but don't copy testing cards"() {
    given:
    def operation = electoralOperation(new Options().with(Options.deploymentTarget, DeploymentTarget.SIMULATION))
    securedOperationRepositoryMock.findOne(1, true) >> operation

    when:
    operationService.targetReal(1)

    then:
    0 * voterTestingCardsLotService.copyTestingCardFromTestSiteToProduction(1)
    1 * securedOperationRepositoryMock.save({ Operation ope -> ope.deploymentTarget == DeploymentTarget.REAL }) >> operation
  }

  def "Can target simulation if previous target in not defined. Copy voting card "() {
    given:
    def operation = electoralOperation(new Options().with(Options.deploymentTarget, DeploymentTarget.NOT_DEFINED))
    securedOperationRepositoryMock.findOne(1, true) >> operation

    when:
    operationService.targetSimulation(1, "simulationName")

    then:
    1 * voterTestingCardsLotService.copyTestingCardFromTestSiteToProduction(1)
    1 * securedOperationRepositoryMock.save({ Operation ope ->
      ope.deploymentTarget == DeploymentTarget.SIMULATION
      ope.simulationName == "simulationName"
    }) >> operation
  }


  def "Cannot target simulation if previous target is already defined."() {
    given:
    def operation = electoralOperation(new Options().with(Options.deploymentTarget, deploymentTarget))
    securedOperationRepositoryMock.findOne(1, true) >> operation

    when:
    operationService.targetSimulation(1, "simulationName")

    then:
    0 * voterTestingCardsLotService.copyTestingCardFromTestSiteToProduction(1)
    0 * securedOperationRepositoryMock.save(_) >> operation
    thrown(CannotTargetSimulation)

    where:
    _ | deploymentTarget
    _ | DeploymentTarget.REAL
    _ | DeploymentTarget.SIMULATION

  }


}
