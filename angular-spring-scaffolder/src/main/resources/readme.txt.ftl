
++++++++++++++++++++++++++++++++

You've successfully done nothing

++++++++++++++++++++++++++++++++


Define your service in spring :
-------------------------------

  @Bean
  protected ${className}Service ${objectName}Service(){
    return new ${className}ServiceMockImpl();
  }

Define you routing in the parent component :
--------------------------------------------

export const routes: Routes = [
  //...
  {path: '${kebabCaseClassName}', loadChildren: 'app/${kebabCaseClassName}/${kebabCaseClassName}.module#${className}Module'},
  //...
];


merge i18n resources
--------------------

Define an access point to your component :
--------------------------------------------
router.navigate([ /*parent path,*/ "${kebabCaseClassName}-list"]);

