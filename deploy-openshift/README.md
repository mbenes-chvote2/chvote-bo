# How to deploy BackOffice in OpenShift

## Pre-requisites

* Install OpenShift client tool 
* Open a shell and login using oc login command (```oc login ```)

* Build chvote-bo-backend, chvote-bo-frontend, chvote-bo-mockserver docker image
* push those images into project chvote-images of OpenShift cluster

## How to deploy in OpenShift
```
cd deploy-openshift

# create all config maps
oc create -f conf/

# create deployments
oc process -f chvote-bo-backend.template.yml | oc create -f -
oc process -f chvote-bo-mockserver.template.yml | oc create -f -
oc process -f chvote-bo-frontend.template.yml | oc create -f -
oc process -f chvote-bo-reverse-proxy.template.yml DOMAIN=xxx | oc create -f -
```

Note: DOMAIN is the complete domain name to use, such as of <dubdomaine>.<cluster domain>.

## Clean up
To clean up all BO resources : 
```
* oc delete all --selector app=BO
* oc delete cm --selector app=BO
* oc delete sa --selector app=BO

```

To kill only the backend objects :
```
* oc delete all --selector app=BO,role=backend
```

Available roles are :
- frontend
- backend
- mock-server
- reverse-proxy

## How to reload NGINX reverse proxy configuration

If a frontend/backend/mock server service is destroyed and recreated, but not the reverse-proxy,
the reverse-proxy won't resolve new virtual IPs. 

So it must be either restarted (kill/deploy the pod), or configuration can be reloaded using 
the following command inside reverse-proxy container :

```
oc get pods --selector app=BO,role=reverse-proxy
oc rsh <pod id>
/opt/bitnami/nginx/sbin/nginx -s reload
```
