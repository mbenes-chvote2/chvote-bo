let http = require('http');
let requestEvery = 5000;
let globalTimeOut = 20 * 60 * 1000;
let startTime = new Date().getTime();

let serviceUp = (resp) => {
    try {
        return JSON.parse(resp).status === "UP"
    } catch (e) {
        return false;
    }
}
    
let servicesToTest = [
    {
        host: 'docker',
        path: '/health/bo',
        port: '80',
        isOk: serviceUp,
        name: "bo"
    },
    {
        host: 'docker',
        path: '/health/bo-mock',
        port: '80',
        isOk: serviceUp,
        name: "mock server"
    }
];

servicesToTest.forEach((service) => service.status = "nok");

let testServices = () => {
    servicesToTest.filter(s => s.status === "nok")
        .forEach(s => {
            s.status = "pending";
            let callback = function (response) {
                let str = '';
                response.on('data', function (chunk) {
                    str += chunk;
                });

                response.on('end', function () {
                    if (s.isOk(str)) {
                        s.status = "ok";
                        console.log("service " + s.name + " is up");
                    } else {
                        s.status = "nok";
                        console.log("service " + s.name + " is still down. Retry in : " + requestEvery / 1000 + "s");
                    }
                });
            };
            let req = http.request({host: s.host, path: s.path, port: s.port}, callback);
            req.on('error', function (error) {
                s.status = "nok";
                console.log("service " + s.name + " is still down. Retry in : " + requestEvery / 1000 + "s");
            });
            req.end();
        })

    if (startTime + globalTimeOut < new Date().getTime()) {
        throw new Error("Global Timeout. Service(s): " +
            servicesToTest.filter(s => s.status !== "ok").map(s => s.name).join(", ") + " is/are not started")

    } else if (servicesToTest.filter(s => s.status !== "ok").length > 0) {
        setTimeout(testServices, requestEvery);
    }
}
console.log("Testing services: " + servicesToTest.map(s => s.name).join(", "));
testServices();
